const findNodesQuery = {
  bool: {
    must: [
      { match: { '@type': 'Cycle' } },
      {
        nested: {
          path: 'inputs',
          query: { exists: { field: 'inputs.fate' } }
        }
      }
    ]
  }
};

const migrateNode = (node: any) => {
  node.inputs = (node.inputs || []).map(({ fate, ...input }) => ({
    ...input,
    isAnimalFeed: ['fed to animals', 'grazed'].includes(fate)
  }));
  return node;
};

module.exports = {
  findNodesQuery,
  migrateNode
};
