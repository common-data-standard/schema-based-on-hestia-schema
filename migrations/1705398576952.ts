const findNodesQuery = {
  bool: {
    must: [
      { match: { '@type': 'Cycle' } },
      {
        exists: {
          field: 'completeness.animalHerd'
        }
      }
    ]
  }
};

const migrateNode = ({ completeness: { animalHerd, ...completeness }, ...node }: any) => {
  node.completeness = {
    ...completeness,
    animalPopulation: animalHerd
  };
  node.schemaVersion = '27.0.0';
  return node;
};

module.exports = {
  findNodesQuery,
  migrateNode
};
