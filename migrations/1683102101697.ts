const findNodesQuery = {
  bool: {
    must: [{ match: { '@type': 'Source' } }]
  }
};

const migrateNode = (node: any) => {
  const { uploadDate, ...data } = node;
  return {
    ...data,
    updatedAt: uploadDate
  };
};

module.exports = {
  findNodesQuery,
  migrateNode
};
