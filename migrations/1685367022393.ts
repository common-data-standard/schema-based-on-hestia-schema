const findNodesQuery = {
  bool: {
    must: [
      { match: { '@type': 'Cycle' } },
      {
        nested: {
          path: 'transformations',
          query: { exists: { field: 'transformations.term.name' } }
        }
      }
    ]
  }
};

const transformationId = (index: number) => `transformation-${index + 1}`;

const migrateNode = (cycle: any) => {
  const transformations = (cycle.transformations || []);
  return {
    ...cycle,
    transformations: transformations.map(({ previousTransformationTerm, ...transformation }, index) => {
      if (previousTransformationTerm) {
        const idx = transformations.findIndex(tr => tr.term['@id'] === previousTransformationTerm['@id']);
        if (idx >= 0) {
          transformation.previousTransformationId = transformationId(idx);
        }
      }
      return {
        transformationId: transformationId(index),
        ...transformation
      };
    })
  };
};

module.exports = {
  findNodesQuery,
  migrateNode
};
