# Class Diagram

> This is a reprensentation of the Schema as a Class diagram.
It uses [mermaidjs](https://mermaid.js.org/syntax/classDiagram.html) for representation.

```mermaid
classDiagram

class Animal {
  +Number value
  +Number sd
  +Number min
  +Number max
  +Number observations
  +Number price
  +String description
  +String referencePeriod
  +String statsDefinition
  +String currency
  +String methodClassification
  +String methodClassificationDescription
  +List[Input] inputs
  +List[Practice] practices
  +List[Property] properties
  +Source source
  +Term term
}

class Bibliography {
  +List[String] websites
  +List[String] dateAccessed
  +Number year
  +Number volume
  +String documentDOI
  +String title
  +String arxivID
  +String scopus
  +String mendeleyID
  +String outlet
  +String issue
  +String chapter
  +String pages
  +String publisher
  +String city
  +String articlePdf
  +String abstract
  +List[Actor] authors
  +List[Actor] editors
  +List[Actor] institutionPub
}

class Completeness {
  +Bool animalFeed
  +Bool animalPopulation
  +Bool cropResidue
  +Bool electricityFuel
  +Bool excreta
  +Bool fertiliser
  +Bool grazedForage
  +Bool ingredient
  +Bool liveAnimalInput
  +Bool material
  +Bool operation
  +Bool otherChemical
  +Bool pesticideVeterinaryDrug
  +Bool product
  +Bool seed
  +Bool soilAmendment
  +Bool transport
  +Bool waste
  +Bool water
}

class Cycle {
  +Bool commercialPracticeTreatment
  +Bool dataPrivate
  +List[Number] otherSitesDuration
  +Number cycleDuration
  +Number siteDuration
  +Number numberOfCycles
  +Number numberOfReplications
  +Number sampleWeight
  +Number harvestedArea
  +String description
  +String treatment
  +String endDate
  +String startDate
  +String startDateDefinition
  +String functionalUnit
  +String functionalUnitDetails
  +String defaultMethodClassification
  +String defaultMethodClassificationDescription
  +Completeness completeness
  +List[Animal] animals
  +List[Emission] emissions
  +List[Input] inputs
  +List[Practice] practices
  +List[Product] products
  +List[Site] otherSites
  +List[Transformation] transformations
  +Site site
  +Source defaultSource
}

class Emission {
  +List[Number] value
  +List[Number] sd
  +List[Number] min
  +List[Number] max
  +List[Number] observations
  +List[String] dates
  +Number emissionDuration
  +Number depth
  +String description
  +String statsDefinition
  +String startDate
  +String endDate
  +String methodTier
  +String methodModelDescription
  +List[Property] properties
  +List[Term] inputs
  +List[Term] animals
  +List[Term] transport
  +Source source
  +Term term
  +Term methodModel
  +Term operation
  +Term transformation
}

class ImpactAssessment {
  +Bool dataPrivate
  +Number functionalUnitQuantity
  +String name
  +String version
  +String versionDetails
  +String allocationMethod
  +String endDate
  +String startDate
  +Cycle cycle
  +List[Indicator] emissionsResourceUse
  +List[Indicator] impacts
  +List[Indicator] endpoints
  +Organisation organisation
  +Product product
  +Site site
  +Source source
  +Term country
  +Term region
}

class Indicator {
  +List[Number] distribution
  +Number value
  +Number sd
  +Number min
  +Number max
  +Number observations
  +String statsDefinition
  +String methodModelDescription
  +List[Term] inputs
  +Term term
  +Term methodModel
  +Term operation
  +Term transformation
}

class Infrastructure {
  +Number lifespan
  +Number lifespanHours
  +Number mass
  +Number area
  +String name
  +String description
  +String startDate
  +String endDate
  +String ownershipStatus
  +String methodClassification
  +String methodClassificationDescription
  +ImpactAssessment impactAssessment
  +List[Input] inputs
  +List[Transport] transport
  +Source source
  +Term term
}

class Input {
  +Bool isAnimalFeed
  +Bool fromCycle
  +Bool producedInCycle
  +Bool impactAssessmentIsProxy
  +List[Number] value
  +List[Number] sd
  +List[Number] min
  +List[Number] max
  +List[Number] observations
  +List[String] dates
  +Number inputDuration
  +Number price
  +Number priceSd
  +Number priceMin
  +Number priceMax
  +Number cost
  +Number costSd
  +Number costMin
  +Number costMax
  +String description
  +String statsDefinition
  +String startDate
  +String endDate
  +String methodClassification
  +String methodClassificationDescription
  +String modelDescription
  +String priceStatsDefinition
  +String costStatsDefinition
  +String currency
  +ImpactAssessment impactAssessment
  +List[Property] properties
  +List[Transport] transport
  +Source source
  +Term term
  +Term model
  +Term operation
  +Term country
  +Term region
}

class Management {
  +Number value
  +String description
  +String startDate
  +String endDate
  +String methodClassification
  +String methodClassificationDescription
  +String modelDescription
  +List[Property] properties
  +Source source
  +Term term
  +Term model
}

class Measurement {
  +List[Number] value
  +List[Number] sd
  +List[Number] min
  +List[Number] max
  +List[Number] observations
  +List[String] dates
  +Number measurementDuration
  +Number depthUpper
  +Number depthLower
  +Number latitude
  +Number longitude
  +String description
  +String statsDefinition
  +String startDate
  +String endDate
  +String methodClassification
  +String methodClassificationDescription
  +String methodDescription
  +List[Property] properties
  +Source source
  +Term term
  +Term method
}

class Organisation {
  +Bool dataPrivate
  +Number area
  +Number latitude
  +Number longitude
  +object boundary
  +Object website
  +String name
  +String description
  +String streetAddress
  +String city
  +String postOfficeBoxNumber
  +String postalCode
  +String glnNumber
  +String startDate
  +String endDate
  +List[Infrastructure] infrastructure
  +Term region
  +Term country
}

class Practice {
  +List[Number] sd
  +List[Number] min
  +List[Number] max
  +List[Number] observations
  +List[String] value
  +List[String] dates
  +Number areaPercent
  +Number price
  +Number priceSd
  +Number priceMin
  +Number priceMax
  +Number cost
  +Number costSd
  +Number costMin
  +Number costMax
  +String description
  +String statsDefinition
  +String startDate
  +String endDate
  +String methodClassification
  +String methodClassificationDescription
  +String modelDescription
  +String priceStatsDefinition
  +String costStatsDefinition
  +String currency
  +String ownershipStatus
  +List[Property] properties
  +Source source
  +Term term
  +Term key
  +Term model
}

class Product {
  +Bool primary
  +List[Number] value
  +List[Number] sd
  +List[Number] min
  +List[Number] max
  +List[Number] observations
  +List[String] dates
  +Number price
  +Number priceSd
  +Number priceMin
  +Number priceMax
  +Number revenue
  +Number revenueSd
  +Number revenueMin
  +Number revenueMax
  +Number economicValueShare
  +String description
  +String variety
  +String statsDefinition
  +String startDate
  +String endDate
  +String methodClassification
  +String methodClassificationDescription
  +String modelDescription
  +String fate
  +String priceStatsDefinition
  +String revenueStatsDefinition
  +String currency
  +List[Property] properties
  +List[Transport] transport
  +Source source
  +Term term
  +Term model
}

class Property {
  +Number value
  +Number share
  +Number sd
  +Number min
  +Number max
  +Number observations
  +String description
  +String statsDefinition
  +String date
  +String startDate
  +String endDate
  +String methodModelDescription
  +String methodClassification
  +String methodClassificationDescription
  +String dataState
  +Source source
  +Term term
  +Term key
  +Term methodModel
}

class Site {
  +Bool dataPrivate
  +Number numberOfSites
  +Number area
  +Number areaSd
  +Number areaMin
  +Number areaMax
  +Number latitude
  +Number longitude
  +object boundary
  +String description
  +String siteType
  +String tenure
  +String glnNumber
  +String startDate
  +String endDate
  +String defaultMethodClassification
  +String defaultMethodClassificationDescription
  +List[Infrastructure] infrastructure
  +List[Management] management
  +List[Measurement] measurements
  +Organisation organisation
  +Source defaultSource
  +Term country
  +Term region
}

class Source {
  +Bool comparativeAssertions
  +Bool dataPrivate
  +String uploadNotes
  +String intendedApplication
  +String studyReasons
  +String intendedAudience
  +String weightingMethod
  +String originalLicense
  +Bibliography bibliography
  +List[Source] metaAnalyses
  +Term sampleDesign
  +Term experimentDesign
}

class Term {
  +List[String] synonyms
  +Number iccCode
  +Number gadmLevel
  +Number latitude
  +Number longitude
  +Number area
  +Object ecoinventReferenceProductId
  +Object website
  +Object agrovoc
  +Object aquastatSpeciesFactSheet
  +Object cornellBiologicalControl
  +Object ecolabelIndex
  +Object feedipedia
  +Object fishbase
  +Object pubchem
  +Object wikipedia
  +String name
  +String definition
  +String description
  +String units
  +String unitsDescription
  +String casNumber
  +String fishstatName
  +String hsCode
  +String iso31662Code
  +String gadmFullName
  +String gadmId
  +String gadmName
  +String gadmCountry
  +String gtin
  +String canonicalSmiles
  +String openLCAId
  +String scientificName
  +String termType
  +List[Property] defaultProperties
  +List[Term] subClassOf
}

class Transformation {
  +Number transformationDuration
  +Number transformedShare
  +String transformationId
  +String description
  +String startDate
  +String endDate
  +String previousTransformationId
  +List[Emission] emissions
  +List[Input] inputs
  +List[Practice] practices
  +List[Product] products
  +Site site
  +Term term
}

class Transport {
  +Bool returnLegIncluded
  +List[Number] observations
  +List[Number] distanceObservations
  +Number value
  +Number sd
  +Number min
  +Number max
  +Number distance
  +Number distanceSd
  +Number distanceMin
  +Number distanceMax
  +String description
  +String statsDefinition
  +String distanceStatsDefinition
  +String methodModelDescription
  +String methodClassification
  +String methodClassificationDescription
  +List[Emission] emissions
  +List[Input] inputs
  +List[Practice] practices
  +Source source
  +Term term
  +Term methodModel
}

Animal "1" --> "1" Term

Animal "1" --> "1" Source

Animal "1" --> "0..*" Property

Animal "1" --> "0..*" Input

Animal "1" --> "0..*" Practice

Cycle "1" --> "0..*" Site

Cycle "1" --> "0..*" Source

Cycle "1" --> "1" Completeness

Cycle "1" --> "0..*" Practice

Cycle "1" --> "0..*" Animal

Cycle "1" --> "0..*" Input

Cycle "1" --> "0..*" Product

Cycle "1" --> "0..*" Transformation

Cycle "1" --> "0..*" Emission

Emission "1" --> "0..*" Term

Emission "1" --> "0..*" Property

Emission "1" --> "1" Source

ImpactAssessment "1" --> "1" Cycle

ImpactAssessment "1" --> "1" Product

ImpactAssessment "1" --> "1" Site

ImpactAssessment "1" --> "1" Term

ImpactAssessment "1" --> "1" Organisation

ImpactAssessment "1" --> "0..*" Source

ImpactAssessment "1" --> "0..*" Indicator

Indicator "1" --> "0..*" Term

Infrastructure "1" --> "1" Term

Infrastructure "1" --> "1" Source

Infrastructure "1" --> "1" ImpactAssessment

Infrastructure "1" --> "0..*" Input

Infrastructure "1" --> "0..*" Transport

Input "1" --> "1" Term

Input "1" --> "1" ImpactAssessment

Input "1" --> "1" Source

Input "1" --> "0..*" Property

Input "1" --> "0..*" Transport

Management "1" --> "1" Term

Management "1" --> "1" Source

Management "1" --> "0..*" Property

Measurement "1" --> "1" Term

Measurement "1" --> "1" Source

Measurement "1" --> "0..*" Property

Organisation "1" --> "1" Term

Organisation "1" --> "0..*" Infrastructure

Practice "1" --> "1" Term

Practice "1" --> "1" Source

Practice "1" --> "0..*" Property

Product "1" --> "1" Term

Product "1" --> "1" Source

Product "1" --> "0..*" Property

Product "1" --> "0..*" Transport

Property "1" --> "1" Term

Property "1" --> "1" Source

Site "1" --> "1" Organisation

Site "1" --> "1" Term

Site "1" --> "0..*" Source

Site "1" --> "0..*" Measurement

Site "1" --> "0..*" Management

Site "1" --> "0..*" Infrastructure

Source "1" --> "1" Bibliography

Source "1" --> "1" Term

Term "1" --> "0..*" Property

Transformation "1" --> "1" Term

Transformation "1" --> "1" Site

Transformation "1" --> "0..*" Input

Transformation "1" --> "0..*" Emission

Transformation "1" --> "0..*" Product

Transformation "1" --> "0..*" Practice

Transport "1" --> "1" Term

Transport "1" --> "1" Source

Transport "1" --> "0..*" Input

Transport "1" --> "0..*" Practice

Transport "1" --> "0..*" Emission
```
