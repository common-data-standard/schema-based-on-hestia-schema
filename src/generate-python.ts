import { writeFileSync } from 'fs';
import { join } from 'path';

import {
  VERSION,
  ensureDir,
  listYmlDir,
  readYmlFile,
  IProperty,
  PropertyType,
  nestedKeys,
  unique,
  getValidations,
  IValidation,
  getSortConfig,
  sortedProperties,
  ISortedProperties,
  propertyIsEnum,
  propertyEnumName
} from './generate-utils';

const SPACER = '    ';
const newLineJoin = '\n\n\n';

const typeToDefaultValue: {
  [type in PropertyType]?: string;
} = {
  [PropertyType.array]: '[]',
  [PropertyType.string]: "''",
  [PropertyType.boolean]: 'False'
};

interface IGeneratedClass {
  name: string;
  isNode: boolean;
  properties: IProperty[];
  sortedProperties: ISortedProperties;
  nestedKeys: string[];
  validations: IValidation[];
}

export const cleanType = (type: string) =>
  type
    .replace(/(Embed\[)([a-zA-Z]*)(\])/g, '$2')
    .replace(/(Ref\[)([a-zA-Z]*)(\])/g, '$2')
    .replace(/(List\[)([a-zA-Z]*)(\])/g, 'array');

const propertyDefault = (type: string) => (type in typeToDefaultValue ? typeToDefaultValue[type] : 'None');

const OUTPUT_DIR = join(__dirname, '..', 'hestia_earth', 'schema');

const cleanEnumName = (name: string) =>
  `${isNaN(+name.charAt(0)) ? '' : '_'}${name.replace(/[\-\s]/g, '_').replace(/[\(\)]/g, '')}`.toUpperCase();

const enumValue = val => `${cleanEnumName(val)} = '${val}'`;

const multilineArray = (values: any[]) =>
  values.length
    ? `[\n${SPACER}${SPACER}${SPACER}${values.join(`,\n${SPACER}${SPACER}${SPACER}`)}\n${SPACER}${SPACER}]`
    : '[]';

const generateProperty = ({ name, type, const: constValue }: IProperty) =>
  `self.fields['${name}'] = ${constValue || propertyDefault(cleanType(type))}`;

const hasField = (field: keyof IProperty) => (property: IProperty) => field in property;

const generateNodeFieldUniqueArrayItems = ({ name, uniqueArrayItem }: IProperty) => `${SPACER}'${name}': [
${SPACER}${SPACER}${SPACER}${uniqueArrayItem?.map(item => `'${item}'`).join(`,\n${SPACER}${SPACER}${SPACER}`)}
${SPACER}${SPACER}]`;

const toDict = `${SPACER}def to_dict(self):
${SPACER}${SPACER}values = OrderedDict()
${SPACER}${SPACER}for key, value in self.fields.items():
${SPACER}${SPACER}${SPACER}if (value is not None and value != '' and value != []) or key in self.required:
${SPACER}${SPACER}${SPACER}${SPACER}values[key] = value
${SPACER}${SPACER}return values`;

const classContent = ({ className, isNode, properties }) => `class ${className}:
${SPACER}def __init__(self):
${SPACER}${SPACER}self.required = ${multilineArray(
  ['type', ...properties].filter(({ required }) => required).map(({ name }) => `'${name}'`)
)}
${SPACER}${SPACER}self.fields = OrderedDict()
${SPACER}${SPACER}self.fields['type'] = ${isNode ? 'NodeType' : 'SchemaType'}.${className.toUpperCase()}.value
${SPACER}${SPACER}${properties.map(generateProperty).join(`\n${SPACER}${SPACER}`)}

${toDict}`;

const jsonldContent = ({ className, isNode, properties }) => `class ${className}JSONLD:
${SPACER}def __init__(self):
${SPACER}${SPACER}self.required = ${multilineArray(
  ['@type', ...(isNode ? ['@id'] : []), ...properties].filter(({ required }) => required).map(({ name }) => `'${name}'`)
)}
${SPACER}${SPACER}self.fields = OrderedDict()
${SPACER}${SPACER}self.fields['@type'] = ${isNode ? 'NodeType' : 'SchemaType'}.${className.toUpperCase()}.value
${SPACER}${SPACER}${[isNode ? { name: '@id', type: 'string', required: true } : null, ...properties]
  .filter(Boolean)
  .map(generateProperty)
  .join(`\n${SPACER}${SPACER}`)}

${toDict}`;

const generateEnums = (className: string, properties: IProperty[]) =>
  properties.filter(propertyIsEnum).map(
    property => `class ${propertyEnumName(className, property)}(Enum):
${SPACER}${property.enum.sort().map(enumValue).join(`\n${SPACER}`)}`
  );

const generateValidations = (className: string, validations: IValidation[]) =>
  validations
    .map(
      ({ paths, values }) => `${cleanEnumName(`${className}_${paths.join('')}`)} = [
    ${values.map(v => `${v.split('.')[0]}.${cleanEnumName(v.split('.')[1])}`).join(`,\n${SPACER}`)}
]`
    )
    .join(newLineJoin);

const generateUniquenessFields = (name: string, properties: IProperty[]) =>
  properties.some(hasField('uniqueArrayItem'))
    ? `${SPACER}'${name}': {
${SPACER}${properties
        .filter(hasField('uniqueArrayItem'))
        .map(generateNodeFieldUniqueArrayItems)
        .join(`,\n${SPACER}`)}\n${SPACER}}`
    : '';

const generateAggregatedQualityScoreIncluded = (name: string, properties: IProperty[]) =>
  properties.some(hasField('aggregatedQualityScoreIncluded'))
    ? `${SPACER}'${name}': [
${SPACER}${properties
        .filter(prop => prop.aggregatedQualityScoreIncluded)
        .map(prop => `${SPACER}'${prop.name}'`)
        .join(`,\n${SPACER}`)}\n${SPACER}]`
    : '';

const getClass = (file: string): IGeneratedClass => {
  console.log(`Processing file: ${file}`);
  const data = readYmlFile(file);
  const properties: IProperty[] = data.properties;
  const validations = data.validation ? getValidations(data.validation) : [];
  const hasJSONLD = data.type === 'Node';
  return {
    name: data.name,
    isNode: hasJSONLD,
    properties,
    sortedProperties: sortedProperties(properties),
    validations,
    nestedKeys: nestedKeys(data)
  };
};

const writeClasses = (classes: IGeneratedClass[]) => {
  const content = `# auto-generated content
from collections import OrderedDict
from enum import Enum
from pkgutil import extend_path


__path__ = extend_path(__path__, __name__)
SCHEMA_VERSION = '${VERSION}'
NESTED_SEARCHABLE_KEYS = [
    ${unique(classes.flatMap(({ nestedKeys }) => nestedKeys))
      .map(v => `'${v}'`)
      .join(`,\n${SPACER}`)}
]


class NodeType(Enum):
${SPACER}${classes
    .filter(({ isNode }) => isNode)
    .map(({ name }) => enumValue(name))
    .join(`\n${SPACER}`)}


class SchemaType(Enum):
${SPACER}${classes.map(({ name }) => enumValue(name)).join(`\n${SPACER}`)}


NODE_TYPES = [e.value for e in NodeType]
SCHEMA_TYPES = [e.value for e in SchemaType]


def is_node_type(type: str): return type in NODE_TYPES


def is_type_valid(type: str): return type in SCHEMA_TYPES


def is_schema_type(type: str): return is_type_valid(type) and not is_node_type(type)


${classes
  .map(({ name: className, isNode, properties }) =>
    [...generateEnums(className, properties), classContent({ className, isNode, properties })].join(newLineJoin)
  )
  .join(newLineJoin)}


${classes
  .map(({ name: className, isNode, properties }) => jsonldContent({ className, isNode, properties }))
  .join(newLineJoin)}


${classes
  .map(({ name: className, validations }) => generateValidations(className, validations))
  .filter(Boolean)
  .join(newLineJoin)}


UNIQUENESS_FIELDS = {
${classes
  .map(({ name: className, properties }) => generateUniquenessFields(className, properties))
  .filter(Boolean)
  .join(',\n')}
}


AGGREGATED_QUALITY_SCORE_FIELDS = {
${classes
  .map(({ name: className, properties }) => generateAggregatedQualityScoreIncluded(className, properties))
  .filter(Boolean)
  .join(',\n')}
}
`;
  writeFileSync(join(OUTPUT_DIR, '__init__.py'), content);
};

const writeUtils = (classes: IGeneratedClass[]) => {
  const content = `# auto-generated content

SORT_CONFIG = ${JSON.stringify(getSortConfig(classes), null, 4).replace(/"/g, "'")}
`;
  writeFileSync(join(OUTPUT_DIR, 'utils', '__init__.py'), content);
};

const run = () => {
  ensureDir(OUTPUT_DIR);
  const classes = listYmlDir().map(getClass);
  writeClasses(classes);
  writeUtils(classes);
};

run();
