import { toCsv } from '@hestia-earth/schema-convert/csv';

interface INode {
  '@type'?: string;
  '@id'?: string;
  type?: string;
  id?: string;
}

type Event = string | INode | INode[] | { nodes: INode[] };

const parseNodes = (data: Event) =>
  typeof data === 'object'
    ? Array.isArray(data)
      ? data
      : 'nodes' in data
      ? data.nodes
      : [data]
    : parseNodes(JSON.parse(data));

export const convert = async (event: any) => {
  console.debug('invoking with payload', event);
  const nodes = parseNodes(event);
  // non-async will need callback function
  return await Promise.resolve(toCsv(nodes, { includeAllHeaders: true }));
};
