import { expect } from 'chai';
import * as sinon from 'sinon';
import 'mocha';

import * as schemaConvert from '@hestia-earth/schema-convert/csv';
import { convert } from './handler';

let stubs: sinon.SinonStub[] = [];

describe('lambdas/convert-to-csv/handler', () => {
  beforeEach(() => {
    stubs = [];
  });

  afterEach(() => {
    stubs.forEach(stub => stub.restore());
  });

  describe('convert', () => {
    const nodes = [
      { '@type': 'Cycle', '@id': '1' },
      { '@type': 'Cycle', '@id': '2' }
    ];
    let convertStub: sinon.SinonStub;

    beforeEach(() => {
      stubs.push((convertStub = sinon.stub(schemaConvert, 'toCsv')));
    });

    describe('nodes as object', () => {
      it('should convert', async () => {
        await convert({ nodes });
        expect(convertStub.calledWith(nodes)).to.equal(true);
      });
    });

    describe('nodes as array', () => {
      it('should convert', async () => {
        await convert(nodes);
        expect(convertStub.calledWith(nodes)).to.equal(true);
      });
    });
  });
});
