/* eslint-disable no-eval */
import { ModuleKind, transpileModule } from 'typescript';
import { readFileSync } from 'fs';
import { join } from 'path';

const migrationsDir = join(__dirname, '..', '..', 'migrations');

export interface IMigration<T> {
  findNodesQuery: any;
  migrateNode: (node: T) => T;
}

const evalMigration = <T>(migration: string) => {
  const { outputText } = transpileModule(migration, { compilerOptions: { module: ModuleKind.CommonJS } });
  const res = eval(outputText);
  return res as IMigration<T>;
};

export const readMigration = <T>(filename: string) =>
  evalMigration<T>(readFileSync(join(migrationsDir, `${filename}.ts`), 'utf-8'));
