import { expect } from 'chai';
import 'mocha';

import { readFileAsJson } from '../../test/utils';
import { IMigration, readMigration } from './utils.spec';

const migrationNumber = '1701856302481';

describe('migrations > 1701856302481', () => {
  let migration: IMigration<any>;

  before(() => {
    migration = readMigration(migrationNumber);
  });

  describe('findNodesQuery', () => {
    it('should be defined', () => {
      expect(migration.findNodesQuery).not.to.equal(undefined);
    });
  });

  describe('migrateNode', () => {
    it('should migrate the data', () => {
      const original = readFileAsJson(`migrations/${migrationNumber}/original.json`);
      const expected = readFileAsJson(`migrations/${migrationNumber}/expected.json`);
      expect(migration.migrateNode(original)).to.deep.equal(expected);
    });
  });
});

