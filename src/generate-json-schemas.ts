import { readFileSync, writeFileSync } from 'fs';
import { join } from 'path';
import * as removeMd from 'remove-markdown';

import { VERSION, ensureDir, listYmlDir, readYmlFile, IProperty, PropertyType } from './generate-utils';

const OUT_DIR = join(__dirname, '@hestia-earth', 'json-schema', 'json-schema');
const EXAMPLE_DIR = join(__dirname, '..', 'examples');
const [DOMAIN] = process.argv.slice(2);

const hestiaSchema = `https://${DOMAIN || process.env.DOMAIN || 'www.hestia.earth'}/schema-data/json-schema`;

const isGeneric = (type: PropertyType) =>
  [PropertyType.string, PropertyType.number, PropertyType.boolean].includes(type);

const typeToJsonSchemaType = {
  [PropertyType.date]: () => 'string',
  [PropertyType['date-time']]: () => 'string',
  default: type => (type.includes('|') ? type.split('|').map(v => handleType({ type: v }).type) : type)
};

const cleanDescription = (description: string) => removeMd(description).replace('\n', '');

const handleType = ({ type, doc, default: defaultValue, pattern }: Partial<IProperty>) => {
  const extraData = {
    ...(doc ? { description: cleanDescription(doc) } : {}),
    ...(defaultValue ? { default: defaultValue } : {})
  };
  if (type.includes('List')) {
    const t = type.replace('List[', '').replace('Ref[', '').replace('Embed[', '').replace(/\]/g, '') as PropertyType;
    return {
      type: 'array',
      items: isGeneric(t) ? { type: t } : { $ref: type.includes('Ref') ? `./${t}-deep.json#` : `./${t}.json#` },
      ...extraData
    };
  } else if (type.includes('Embed')) {
    return {
      $ref: `./${type.replace('Embed[', '').replace(/\]/g, '')}.json#`,
      ...extraData
    };
  } else if (type.includes('Ref')) {
    return {
      $ref: `./${type.replace('Ref[', '').replace(/\]/g, '')}-deep.json#`,
      ...extraData
    };
  } else if (type === PropertyType.iri) {
    return {
      type: 'object',
      required: ['@id'],
      properties: {
        '@id': {
          type: 'string'
        }
      },
      ...extraData
    };
  } else if (type.startsWith('array[')) {
    type = type.replace('array[', '').replace(/\]/g, '') as PropertyType;
    return {
      type: 'array',
      items: isGeneric(type) ? { type, ...(pattern ? { pattern } : {}) } : handleType({ type }),
      ...extraData
    };
  }
  return {
    type: type in typeToJsonSchemaType ? typeToJsonSchemaType[type]() : typeToJsonSchemaType.default(type),
    ...(pattern ? { pattern } : {}),
    ...extraData
  };
};

const handledProperties = ['name', 'type', 'doc', 'required', 'pattern'];

const extraProperties = value =>
  Object.keys(value)
    .filter(prop => !handledProperties.includes(prop))
    .reduce((prev, property) => {
      if (property in value) {
        prev[property] = value[property];
      }
      return prev;
    }, {});

const generateJsonSchema = (file: string) => {
  console.log(`Processing file: ${file}`);
  const data = readYmlFile(file);
  const { name, doc, examples, properties, type, validation } = data;
  const isNode = type === 'Node';
  let examplesData = [];
  try {
    examplesData = examples ? examples.map(e => JSON.parse(readFileSync(join(EXAMPLE_DIR, e), 'utf8'))) : [];
  } catch (err) {}
  const baseSchema: any = {
    $id: `${hestiaSchema}/${name}.json`,
    $schema: 'http://json-schema.org/draft-07/schema#',
    $comment: `Schema version ${VERSION}`,
    title: name,
    description: doc,
    type: 'object',
    additionalProperties: false,
    properties: properties.reduce((prev, curr) => {
      const example = examplesData.map(data => data[curr.name]).filter(val => !!val);
      return {
        ...prev,
        [curr.name]: {
          ...handleType(curr),
          ...(example ? { examples: example } : {}),
          ...extraProperties(curr)
        }
      };
    }, {}),
    ...(validation || {})
  };
  const required = properties.filter(({ required }) => required).map(({ name }) => name);
  const existingNodeRequired = {
    required: ['@type', ...(isNode ? ['@id'] : [])]
  };
  const schema = {
    ...baseSchema,
    oneOf:
      name === 'Term'
        ? [existingNodeRequired]
        : [existingNodeRequired, { required: ['type', ...(isNode ? ['id'] : [])] }],
    properties: {
      ...(isNode
        ? {
            id: {
              type: 'string',
              description: 'My local unique ID',
              examples: ['my-unique-id-1'],
              exportDefaultCSV: true
            },
            '@id': {
              type: 'string',
              description: 'Unique id assigned by Hestia',
              examples: ['@hestia-unique-id-1'],
              extend: true,
              exportDefaultCSV: true
            }
          }
        : {}),
      type: {
        type: 'string',
        description: 'Type of the Node',
        default: name,
        enum: [name]
      },
      '@type': {
        type: 'string',
        description: 'Type of the Node',
        default: name,
        enum: [name],
        extend: true
      },
      ...baseSchema.properties
    }
  };
  writeFileSync(join(OUT_DIR, `${name}.json`), JSON.stringify({ ...schema, required }, null, 2));

  // schema when used by $ref
  isNode &&
    writeFileSync(
      join(OUT_DIR, `${name}-deep.json`),
      JSON.stringify(
        {
          ...schema,
          $id: `${hestiaSchema}/${name}-deep.json`
        },
        null,
        2
      )
    );

  // schema used to validate JSON-LD examples only
  const jsonldSchema = {
    ...schema,
    properties: {
      '@context': {
        type: 'string'
      },
      ...schema.properties
    }
  };
  return [
    {
      schema: {
        ...jsonldSchema,
        required
      },
      schemaName: `${name}.json`,
      examples: examplesData
    },
    ...(isNode
      ? [
          {
            schema: {
              ...jsonldSchema,
              $id: `${hestiaSchema}/${name}-deep.json`
            },
            schemaName: `${name}-deep.json`,
            examples: [] // not running against examples
          }
        ]
      : [])
  ];
};

const run = () => {
  ensureDir(OUT_DIR);
  listYmlDir().flatMap(generateJsonSchema);
};

run();
