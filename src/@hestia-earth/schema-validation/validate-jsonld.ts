import { readdirSync, readFileSync, lstatSync } from 'fs';
import { join } from 'path';

import { validator } from './validate';

const [DOMAIN, folder, strictMode] = process.argv.slice(2);

const domain = `https://${DOMAIN || process.env.DOMAIN || 'www.hestia.earth'}`;

const loadFile = (filename: string) => JSON.parse(readFileSync(filename, 'utf8'));

const extensions = ['jsonld', 'json', 'hestia'];

const findFiles = (directory: string): string[] =>
  readdirSync(directory)
    .map(path => {
      const isDirectory = lstatSync(join(directory, path)).isDirectory();
      return isDirectory
        ? findFiles(join(directory, path))
        : extensions.includes(path.split('.')[1])
        ? join(directory, path)
        : null;
    })
    .flat()
    .filter(Boolean);

export const run = async () => {
  const contentValidator = validator(domain, strictMode === 'true');

  const jsonldFiles = findFiles(folder);
  const results = [];

  for (const filepath of jsonldFiles) {
    console.log('Validating', filepath);
    const content = loadFile(filepath);
    const nodes = (Array.isArray(content) ? content : 'nodes' in content ? content.nodes : [content])
      .map(node => (node['@type'] || node.type ? node : null))
      .filter(Boolean);

    const allErrors = [];
    for (const node of nodes) {
      const { errors } = await contentValidator(node);
      allErrors.push(errors);
    }
    const success = !allErrors.some(v => v.length);

    if (!success) {
      results.push({
        filepath,
        success,
        errors: allErrors
      });
    }
  }

  if (results.length) {
    throw new Error(`Validation errors: ${JSON.stringify(results, null, 2)}`);
  }
};
