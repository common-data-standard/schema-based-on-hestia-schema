import { expect } from 'chai';
import 'mocha';
import { readFileSync, readdirSync } from 'fs';
import { join } from 'path';

import { FIXTURES_FOLDER } from '../../../test/utils';
import { validator } from './validate';

describe('schema-validation/validate', () => {
  describe('validator', () => {
    const folder = join(__dirname, '..', '..', '..', 'examples');
    const files = readdirSync(folder).filter(v => v.endsWith('.jsonld'));
    const validate = validator();

    files.map(file => {
      describe(file, () => {
        it('should validate', async () => {
          const data = JSON.parse(readFileSync(join(folder, file), 'utf8'));
          const result = await validate(data);
          expect(result.success).to.equal(true);
        });
      });
    });

    describe('unknown type', () => {
      const data: any = {
        type: 'Unknown'
      };

      it('should throw an error', async () => {
        try {
          await validate(data);
          expect(true).to.equal(false);
        } catch (err) {
          expect(err.message).to.equal('Unknown or invalid type "Unknown"');
        }
      });
    });

    describe('Cycle', () => {
      const folder = join(FIXTURES_FOLDER, 'json-schema', 'Cycle');

      describe('inputs', () => {
        const inputsFolder = join(folder, 'inputs');

        describe('fromCycle', () => {
          const testFolder = join(inputsFolder, 'fromCycle');
          const filename = 'invalid.json';

          it('should return an error', async () => {
            const data = JSON.parse(readFileSync(join(testFolder, filename), 'utf8'));
            const result = await validate(data);
            expect(result.success).to.equal(false);

            expect(result.errors).to.deep.contain({
              keyword: 'not',
              dataPath: '.inputs[0]',
              schemaPath: '#/allOf/5/then/properties/inputs/items/not',
              params: {},
              message: 'should NOT be valid',
              schema: {
                required: ['fromCycle']
              }
            });
          });
        });
      });

      describe('products', () => {
        const productsFolder = join(folder, 'products');

        describe('primary crop must be 1 ha', () => {
          const testFolder = join(productsFolder, 'crop-functionalUnit-1-ha');

          describe('not primary relative FU', () => {
            const filename = 'not-primary-relative.json';

            it('should return success', async () => {
              const data = JSON.parse(readFileSync(join(testFolder, filename), 'utf8'));
              const result = await validate(data);
              expect(result.success).to.equal(true);
            });
          });

          describe('primary 1 ha FU', () => {
            const filename = 'primary-1-ha.json';

            it('should return success', async () => {
              const data = JSON.parse(readFileSync(join(testFolder, filename), 'utf8'));
              const result = await validate(data);
              expect(result.success).to.equal(true);
            });
          });

          describe('functionalUnit is relative', () => {
            const filename = 'relative.json';

            it('should return an error', async () => {
              const data = JSON.parse(readFileSync(join(testFolder, filename), 'utf8'));
              const result = await validate(data);
              expect(result.success).to.equal(false);

              expect(result.errors).to.deep.contain({
                keyword: 'const',
                dataPath: '.functionalUnit',
                schemaPath: '#/allOf/1/then/properties/functionalUnit/const',
                params: { allowedValue: '1 ha' },
                message: 'should be equal to constant',
                schema: {}
              });
            });
          });
        });
      });
    });

    describe('Practice', () => {
      const folder = join(FIXTURES_FOLDER, 'json-schema', 'Practice');

      describe('% area term should not use areaPercent', () => {
        const testFolder = join(folder, 'area-percent-value');

        describe('units is not "% area"', () => {
          const filename = 'no-percent-area.json';

          it('should return success', async () => {
            const data = JSON.parse(readFileSync(join(testFolder, filename), 'utf8'));
            const result = await validate(data);
            expect(result.success).to.equal(true);
          });
        });

        describe('units is "% area" > valid', () => {
          const filename = 'percent-area-value.json';

          it('should return success', async () => {
            const data = JSON.parse(readFileSync(join(testFolder, filename), 'utf8'));
            const result = await validate(data);
            expect(result.success).to.equal(true);
          });
        });

        describe('units is "% area" > error', () => {
          const filename = 'percent-area-areaPercent.json';

          it('should return an error', async () => {
            const data = JSON.parse(readFileSync(join(testFolder, filename), 'utf8'));
            const result = await validate(data);
            expect(result.success).to.equal(false);

            expect(result.errors).to.deep.contain({
              keyword: 'not',
              dataPath: '',
              schemaPath: '#/allOf/2/then/not',
              params: {},
              message: 'should NOT be valid',
              schema: { required: ['areaPercent'] }
            });
          });
        });
      });
    });

    describe('Site', () => {
      const folder = join(FIXTURES_FOLDER, 'json-schema', 'Site');

      describe('boundary', () => {
        const testFolder = join(folder, 'boundary');

        describe('invalid GeoJSON', () => {
          const filename = 'invalid.json';

          it('should return error', async () => {
            const data = JSON.parse(readFileSync(join(testFolder, filename), 'utf8'));
            const result = await validate(data);
            expect(result.success).to.equal(false);

            expect(result.errors).to.deep.contain({
              keyword: 'geojson',
              dataPath: '.boundary',
              schemaPath: '#/properties/boundary/geojson',
              message: 'not a valid GeoJSON',
              params: {},
              schema: {}
            });
          });
        });
      });
    });

    describe('Term', () => {
      const folder = join(FIXTURES_FOLDER, 'json-schema', 'Term');

      describe('canonicalSmiles', () => {
        const testFolder = join(folder, 'canonicalSmiles');

        describe('valid value', () => {
          const filename = 'valid.json';

          it('should return success', async () => {
            const data = JSON.parse(readFileSync(join(testFolder, filename), 'utf8'));
            const result = await validate(data);
            expect(result.success).to.equal(true);
          });
        });

        describe('invalid value', () => {
          const filename = 'invalid.json';

          it('should return an error', async () => {
            const data = JSON.parse(readFileSync(join(testFolder, filename), 'utf8'));
            const result = await validate(data);
            expect(result.success).to.equal(false);

            expect(result.errors).to.deep.contain({
              keyword: 'pattern',
              dataPath: '.canonicalSmiles',
              schemaPath: '#/properties/canonicalSmiles/pattern',
              params: { pattern: '^[^J][A-Za-z0-9@+%\\.\\-\\[\\]\\(\\)\\\\=#$]*$' },
              message: 'should match pattern "^[^J][A-Za-z0-9@+%\\.\\-\\[\\]\\(\\)\\\\=#$]*$"',
              schema: {}
            });
          });
        });
      });
    });
  });
});
