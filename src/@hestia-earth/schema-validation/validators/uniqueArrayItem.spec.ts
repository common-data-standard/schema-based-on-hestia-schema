import { expect } from 'chai';
import 'mocha';

import { keyword, validate } from './uniqueArrayItem';

describe('schema-validation/validators/uniqueArrayItem', () => {
  describe('validate', () => {
    const keys = ['term.@id', 'description'];

    describe('with valid items', () => {
      const items = [
        {
          term: {
            '@id': 'term-1'
          },
          description: 'test'
        },
        {
          term: {
            '@id': 'term-2'
          }
        },
        {
          term: {
            '@id': 'term-3'
          },
          description: 'test'
        }
      ];

      it('should return true', () => {
        expect(validate(keys, items)).to.equal(true);
      });
    });

    describe('with invalid items', () => {
      const items = [
        {
          term: {
            '@id': 'term-1'
          },
          description: 'test'
        },
        {
          term: {
            '@id': 'term-2'
          },
          description: 'test'
        },
        {
          term: {
            '@id': 'term-2'
          },
          description: 'test',
          value: 10
        },
        {
          term: {
            '@id': 'term-3'
          }
        },
        {
          term: {
            '@id': 'term-3'
          }
        },
        {
          term: {
            '@id': 'term-3'
          },
          value: 11
        }
      ];

      it('should return false', () => {
        expect(validate(keys, items)).to.equal(false);
      });

      it('should include the duplicated indexes', () => {
        validate(keys, items) as boolean;

        expect(validate.errors).to.deep.equal([
          {
            dataPath: '[1]',
            keyword,
            schemaPath: '#/invalid',
            message: 'every item in the list should be unique',
            params: { keyword, keys, duplicatedIndexes: [2] }
          },
          {
            dataPath: '[2]',
            keyword,
            schemaPath: '#/invalid',
            message: 'every item in the list should be unique',
            params: { keyword, keys, duplicatedIndexes: [1] }
          },
          {
            dataPath: '[3]',
            keyword,
            schemaPath: '#/invalid',
            message: 'every item in the list should be unique',
            params: { keyword, keys, duplicatedIndexes: [4, 5] }
          },
          {
            dataPath: '[4]',
            keyword,
            schemaPath: '#/invalid',
            message: 'every item in the list should be unique',
            params: { keyword, keys, duplicatedIndexes: [3, 5] }
          },
          {
            dataPath: '[5]',
            keyword,
            schemaPath: '#/invalid',
            message: 'every item in the list should be unique',
            params: { keyword, keys, duplicatedIndexes: [3, 4] }
          }
        ]);
      });
    });

    describe('with simple arrays', () => {
      const items = [
        {
          key: 'key1',
          array: ['value1', 'value2'],
          unique: true
        },
        {
          key: 'key1',
          array: ['value1', 'value2'],
          unique: false
        }
      ];

      it('should validate', () => {
        expect(validate(['array', 'key'], items)).to.equal(false);
        expect(validate(['key', 'unique'], items)).to.equal(true);
      });
    });

    describe('with blank node arrays', () => {
      const items = [
        {
          '@type': 'Animal',
          term: {
            '@id': 'cattleBeefHeifers',
            '@type': 'Term'
          },
          properties: [
            {
              '@type': 'Property',
              term: {
                '@id': 'age',
                '@type': 'Term'
              },
              value: 730
            },
            {
              '@type': 'Property',
              term: {
                '@id': 'liveweightPerHead',
                '@type': 'Term'
              },
              value: 200
            }
          ]
        },
        {
          '@type': 'Animal',
          term: {
            '@id': 'cattleBeefHeifers',
            '@type': 'Term'
          },
          properties: [
            {
              '@type': 'Property',
              term: {
                '@id': 'age',
                '@type': 'Term'
              },
              value: 730
            },
            {
              '@type': 'Property',
              term: {
                '@id': 'liveweightPerHead',
                '@type': 'Term'
              },
              value: 500
            }
          ]
        }
      ];

      it('should validate', () => {
        expect(validate(['term.@id', 'properties.term.@id'], items)).to.equal(false);
        expect(validate(['term.@id', 'properties.term.@id', 'properties.value'], items)).to.equal(true);
      });
    });
  });
});
