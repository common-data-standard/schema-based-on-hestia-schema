import { Ajv } from 'ajv';

import { keyword as arraySameSize, init as arraySameSizeInit } from './arraySameSize';
import { keyword as datePattern, init as datePatternInit } from './datePattern';
import { keyword as geojson, init as geojsonInit } from './geojson';
import { keyword as uniqueArrayItem, init as uniqueArrayItemInit } from './uniqueArrayItem';

export { arraySameSize, datePattern, geojson, uniqueArrayItem };

export const init = (ajv: Ajv) => {
  arraySameSizeInit(ajv);
  datePatternInit(ajv);
  geojsonInit(ajv);
  uniqueArrayItemInit(ajv);
};
