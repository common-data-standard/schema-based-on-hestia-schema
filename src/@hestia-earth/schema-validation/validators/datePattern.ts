import { Ajv, SchemaValidateFunction } from 'ajv';

const isValidDate = (date: string) => isFinite(new Date(date).getTime());

export const keyword = 'datePattern';

export const validate: SchemaValidateFunction = (
  _schema: any,
  value: string | string[],
  _parentSchema: object,
  dataPath: string
) => {
  const values = Array.isArray(value) ? value : [value];
  const errors = values
    .map((v, index) =>
      isValidDate(v)
        ? null
        : {
            dataPath: `${dataPath}${Array.isArray(value) ? `[${index}]` : ''}`,
            keyword,
            schemaPath: '#/invalid',
            message: 'not a valid date',
            params: {}
          }
    )
    .filter(Boolean);
  validate.errors = errors;
  return errors.length === 0;
};

export const init = (ajv: Ajv) =>
  ajv.addKeyword(keyword, {
    type: ['string', 'array'],
    errors: 'full',
    validate
  });
