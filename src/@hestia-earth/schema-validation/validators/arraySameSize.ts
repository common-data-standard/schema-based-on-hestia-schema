import { Ajv, SchemaValidateFunction } from 'ajv';

export const keyword = 'arraySameSize';

export const validate: SchemaValidateFunction = (
  schema: string[],
  data: any[],
  _parentSchema: any,
  dataPath: string,
  parentData: any
) => {
  const current = data.length;
  const errors = schema
    .filter(key => key in parentData)
    .map(key => {
      const expected = parentData[key].length;
      return current !== expected
        ? {
            dataPath,
            keyword,
            schemaPath: '#/invalid',
            message: `must contain as many items as ${key}s`,
            params: { keyword, current, expected }
          }
        : null;
    })
    .filter(Boolean);
  validate.errors = errors;
  return errors.length === 0;
};

export const init = (ajv: Ajv) =>
  ajv.addKeyword(keyword, {
    type: 'array',
    errors: 'full',
    validate
  });
