import * as Ajv from 'ajv';
import { SchemaType, JSON as HestiaJson, isTypeNode } from '@hestia-earth/schema';
import { loadSchemas, definitions } from '@hestia-earth/json-schema';

import { init as initValidators } from './validators';

const existingNodeRequired = Object.freeze({ required: ['@id', '@type'] });

const validateSchemaType = (schemas: definitions, content) => {
  const type = content['@type'] || content.type;
  if (!(type in schemas)) {
    throw new Error(`Unknown or invalid type "${type}"`);
  }
  return true;
};

const schemaData = ({ required }: any) => ({
  ...(required?.length ? { required } : {})
});

// using `verbose: true` includes data which we don't need
const cleanErrors = (errors: Ajv.ErrorObject[]) =>
  errors.map(({ keyword, data, schema, parentSchema, ...error }) => ({
    keyword,
    ...error,
    ...(schema ? { schema: schemaData(schema) } : {})
  }));

/**
 * Validate a single node. Function is asynchronous to return a list of `success` and a list of `errors`.
 *
 * @param ajv The AJV object. Use `initAjv()` or pass your own.
 * @param schemas The schema definitions. Use `loadSchemas` from `@hestia-earth/json-schema`  or pass your own.
 * @returns
 */
export const validateContent = (ajv: Ajv.Ajv, schemas: definitions) => async (content: HestiaJson<SchemaType>) =>
  validateSchemaType(schemas, content) && {
    success: await ajv.validate(schemas[content['@type'] || content.type], content),
    errors: cleanErrors(ajv.errors || [])
  };

export const initAjv = () => {
  const ajv = new Ajv({
    schemaId: 'auto',
    allErrors: true,
    verbose: true
  });
  ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-04.json'));

  initValidators(ajv);

  return ajv;
};

/**
 * Create a validator instance. Call with a Node to validate it.
 *
 * @param domain The domain of the validator (defaults to Hestia's website)
 * @param strictMode Allow validating non-existing nodes, i.e. without unique `@id`.
 * @returns Function to validate a Node. Use `await validator()(node)`
 */
export const validator = (domain = 'https://www.hestia.earth', strictMode = true) => {
  const ajv = initAjv();
  const schemas = loadSchemas();

  Object.keys(schemas).map(schemaName => {
    const schema = schemas[schemaName];

    schema.properties = {
      '@context': {
        type: 'string'
      },
      ...schema.properties
    };
    schema.oneOf = strictMode && isTypeNode(schemaName as any) ? [existingNodeRequired] : schema.oneOf;
    ajv.addSchema(schema, `${domain}/schema/json-schema/${schemaName}#`);
  });
  return validateContent(ajv, schemas);
};
