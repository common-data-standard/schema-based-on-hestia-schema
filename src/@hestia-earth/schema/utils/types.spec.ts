import { expect } from 'chai';
import 'mocha';

import { TermTermType } from '../Term';
import { SchemaType } from '../types';
import { schemaFromTermType } from './types';

describe('schema/utils/types', () => {
  describe('schemaFromTermType', () => {
    it('should return a list of schema keys', () => {
      expect(schemaFromTermType(TermTermType.emission)).to.deep.equal([
        { type: SchemaType.Emission, key: 'term' },
        { type: SchemaType.Indicator, key: 'term' }
      ]);

      expect(schemaFromTermType(TermTermType.crop)).to.deep.equal([
        { type: SchemaType.Emission, key: 'inputs' },
        { type: SchemaType.Input, key: 'term' },
        { type: SchemaType.Product, key: 'term' }
      ]);

      expect(schemaFromTermType(TermTermType.landUseManagement)).to.deep.equal([
        { type: SchemaType.Management, key: 'term' },
        { type: SchemaType.Practice, key: 'term' }
      ]);

      expect(schemaFromTermType(TermTermType.transport)).to.deep.equal([
        { type: SchemaType.Emission, key: 'inputs' },
        { type: SchemaType.Transport, key: 'term' }
      ]);
    });
  });
});
