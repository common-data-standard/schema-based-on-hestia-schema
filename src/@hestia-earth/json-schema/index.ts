import { SchemaType, NodeType } from '@hestia-earth/schema';

import { IDefinition, definitions } from './types';

export * from './types';
export * from './schema-utils';

export const loadSchemas = () => {
  const nodeTypes = Object.values(NodeType);
  const allFiles = Object.values(SchemaType).flatMap(type =>
    [type, nodeTypes.includes(type as any) ? `${type}-deep` : null].filter(Boolean)
  );
  const schemas = allFiles.map((file: string) => ({
    schemaName: file.split('.')[0] as SchemaType,
    schema: require(`./json-schema/${file}.json`) as IDefinition
  }));
  return schemas.reduce((prev, { schema, schemaName }) => ({ ...prev, [schemaName]: schema }), {} as definitions);
};
