import { SchemaType } from '@hestia-earth/schema';

export type genericType = 'string' | 'boolean' | 'number' | 'integer' | 'null';
export type definitionPropertyType = 'object' | 'array' | genericType | genericType[] | 'auto';

interface IDefinitionProperty {
  $id?: string;
  type?: definitionPropertyType;
  $comment?: string;
  description?: string;
  title?: string;
  /**
   * Makes this Node unique.
   */
  unique?: boolean;
  /**
   * Can be searched for on the Hestia Platform.
   */
  searchable?: boolean;
  /**
   * This field is recommended (but not required).
   */
  recommended?: boolean;
  /**
   * Default generated value if field is not set.
   */
  default?: any;
  /**
   * Will be set by Hestia.
   */
  internal?: boolean;
  /**
   * Will be included for existing Node.
   */
  extend?: boolean;
  /**
   * Should export to CSV by default.
   */
  exportDefaultCSV?: boolean;
  /**
   * Define keys used for item unicity.
   */
  uniqueArrayItem?: string[];
  /**
   * Should this field by included when calculating the quality score.
   */
  aggregatedQualityScoreIncluded?: boolean;
}

interface IDefinitionGeneric extends IDefinitionProperty {
  type: genericType | genericType[];
  enum?: string[];
  const?: string | number | boolean;
}

export interface IDefinitionObject extends IDefinitionProperty {
  type: 'object';
  properties: IDefinitionProperties;
  required?: string[];
  $ref?: string;
  /**
   * If this is a GeoJSON object.
   */
  geojson?: boolean;
  oneOf?: Partial<IDefinitionObject>[];
  anyOf?: Partial<IDefinitionObject>[];
  allOf?: Partial<IDefinitionObject>[];
}

export interface IDefinitionArray extends IDefinitionProperty {
  type: 'array';
  items: definition;
}

export type definition = IDefinitionObject | IDefinitionArray | IDefinitionGeneric;

export interface IDefinitionProperties {
  [key: string]: definition;
}

export interface IDefinition extends IDefinitionObject {
  definitions?: IDefinitionProperties;
}

export type definitions = { [type in SchemaType]: IDefinition };

export const recommendedProperties = ({ properties }: IDefinition) =>
  Object.keys(properties || {}).filter(property => properties[property].recommended);

export const uniqueProperties = ({ properties }: IDefinition) =>
  Object.keys(properties || {}).filter(property => properties[property].unique);

export const searchableProperties = ({ properties }: IDefinition) =>
  Object.keys(properties || {}).filter(property => properties[property].searchable);

export const extendableProperties = ({ properties }: IDefinition) =>
  Object.keys(properties || {}).filter(property => properties[property].extend);

export const excludedDefaultProperties = ['@type', 'type'];

export const defaultProperties = ({ properties }: IDefinition) =>
  Object.keys(properties || {}).filter(
    property => !excludedDefaultProperties.includes(property) && 'default' in properties[property]
  );
