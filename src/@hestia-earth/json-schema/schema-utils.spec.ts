import { expect } from 'chai';
import 'mocha';

import { loadSchemas } from '.';
import { isDefaultCSVSelected, isCSVIncluded, schemaFromKey } from './schema-utils';

describe('json-schema/index', () => {
  const schemas = loadSchemas();

  describe('isDefaultCSVSelected', () => {
    const headers = [
      'cycle.emissions.0.term.@id',
      'cycle.emissions.0.term.name',
      'cycle.emissions.0.term.defaultProperties.0.term.@id',
      'cycle.emissions.0.methodModel.@id',
      'cycle.emissions.0.methodModel.name',
      'cycle.emissions.0.uploadBy',
      'cycle.emissions.0.value'
    ];

    it('should select default keys', () => {
      const results = headers.filter(isDefaultCSVSelected(schemas));

      expect(results).to.deep.equal([
        'cycle.emissions.0.term.@id',
        'cycle.emissions.0.methodModel.@id',
        'cycle.emissions.0.value'
      ]);
    });
  });

  describe('isCSVIncluded', () => {
    describe('Cycle', () => {
      const headers = [
        'cycle.@id',
        'cycle.name',
        'cycle.emissions.0.term.@id',
        'cycle.emissions.0.term.name',
        'cycle.emissions.0.updated'
      ];

      it('should filter non-included keys', () => {
        const results = headers.filter(isCSVIncluded(schemas));

        expect(results).to.deep.equal(['cycle.@id', 'cycle.emissions.0.term.@id', 'cycle.emissions.0.term.name']);
      });
    });

    describe('Transformation', () => {
      const headers = [
        'transformation.@id',
        'transformation.schemaVersion',
        'transformation.emissions.0.term.@id',
        'transformation.emissions.0.term.name',
        'transformation.emissions.0.updated'
      ];

      it('should filter non-included keys', () => {
        const results = headers.filter(isCSVIncluded(schemas));

        expect(results).to.deep.equal([
          'transformation.@id',
          'transformation.emissions.0.term.@id',
          'transformation.emissions.0.term.name'
        ]);
      });
    });
  });

  describe('schemaFromKey', () => {
    describe('key is valid', () => {
      it('should return the schema', () => {
        expect(schemaFromKey(schemas, 'cycle.defaultSource.name').title).to.equal('Source');
        expect(schemaFromKey(schemas, 'cycle.defaultSource.bibliography.title').title).to.equal('Bibliography');
        expect(schemaFromKey(schemas, 'cycle.inputs.0.term').title).to.equal('Input');
        expect(schemaFromKey(schemas, 'cycle.inputs.0.term.@id').title).to.equal('Term');
      });
    });

    describe('key is not valid', () => {
      it('should return undefined', () => {
        expect(schemaFromKey(schemas, 'cycle.random.name')).to.equal(undefined);
        expect(schemaFromKey(schemas, 'random')).to.equal(undefined);
        // TODO: this should work as well?
        // expect(schemaFromKey(schemas, 'cycle.defaultSource.random')).to.equal(undefined);
      });
    });
  });
});
