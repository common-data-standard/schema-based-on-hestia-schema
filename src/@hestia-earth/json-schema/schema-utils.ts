import { typeToSchemaType, refToSchemaType, isTypeValid } from '@hestia-earth/schema';

import { definitions, definition, IDefinitionObject } from './types';

const propertyRef = (schema: definition, key: string) => {
  const property = schema && (schema as any).properties ? (schema as any).properties[key] : {};
  return property ? ('items' in property ? property.items.$ref : property.$ref) : null;
};

const keyType = (schema: definition, key: string) => {
  const ref = propertyRef(schema, key);
  return ref ? refToSchemaType(ref) : null;
};

const isNumber = (key: string) => key.match(/^\d+$/g) !== null;

type recursiveKeyCheckFunction = (schema: IDefinitionObject, key: string) => boolean;

const recursiveKeyCheck = (
  schemas: definitions,
  schema: definition,
  key: string,
  checkFunction: recursiveKeyCheckFunction
): boolean => {
  const [firstKey, ...keys] = key.split('.');
  return isNumber(firstKey)
    ? // for arrays, we can continue the recursion without checking if it is selected
      recursiveKeyCheck(schemas, schema, keys.join('.'), checkFunction)
    : // each key needs to be set as selected to continue the recursion
      schema &&
        checkFunction(schema as IDefinitionObject, firstKey) &&
        // either exit if end of recursion or keep going
        (keys.length < 1 ||
          recursiveKeyCheck(schemas, schemas[keyType(schema, firstKey)], keys.join('.'), checkFunction));
};

const isPropertySelected = ({ properties }: IDefinitionObject, key: string) => (properties[key] || {}).exportDefaultCSV;

/**
 * Check whether the field should be selected by default or not while exporting as CSV.
 *
 * @param schemas {definitions} List of definitions loaded with `loadSchemas`.
 * @param key {string} The full key of the field, such as `cycle.inputs.0.value`.
 * @returns true if it should be selected by default, false otherwise.
 */
export const isDefaultCSVSelected = (schemas: definitions) => (key: string) => {
  const [firstKey, ...keys] = key.split('.');
  const type = typeToSchemaType(firstKey);
  const schema = isTypeValid({ type }) ? schemas[type] : null;
  return (schema ? recursiveKeyCheck(schemas, schema, keys.join('.'), isPropertySelected) : null) || false;
};

const isPropertyNotInternal = ({ properties }: IDefinitionObject, key: string) => !(properties[key] || {}).internal;

/**
 * Check wether the field should be included while exporting as CSV.
 *
 * @param schemas {definitions} List of definitions loaded with `loadSchemas`.
 * @param key {string} The full key of the field, such as `cycle.inputs.0.value`.
 * @returns true if it should be included, false otherwise.
 */
export const isCSVIncluded = (schemas: definitions) => (key: string) => {
  const [firstKey, ...keys] = key.split('.');
  const type = typeToSchemaType(firstKey);
  const schema = isTypeValid({ type }) ? schemas[type] : null;
  return (schema ? recursiveKeyCheck(schemas, schema, keys.join('.'), isPropertyNotInternal) : null) || false;
};

const recursiveSchemaFromKey = (schemas: definitions, schema: definition, key: string): definition => {
  const [firstKey, ...keys] = key.split('.');
  return isNumber(firstKey)
    ? // for arrays, we can continue the recursion
      recursiveSchemaFromKey(schemas, schema, keys.join('.'))
    : !!schema
    ? keys.length < 1
      ? schema
      : recursiveSchemaFromKey(schemas, schemas[keyType(schema, firstKey)], keys.join('.')) // keep going deep
    : undefined;
};

/**
 * Get the schema definition associated with a certain key. Can be `undefined` if key is unknown.
 *
 * @param schemas {definitions} List of definitions loaded with `loadSchemas`.
 * @param key {string} The full key of the field, such as `cycle.inputs.0.value`.
 * @returns The definition if found.
 */
export const schemaFromKey = (schemas: definitions, key: string) => {
  const [firstKey, ...keys] = key.split('.');
  const type = typeToSchemaType(firstKey);
  const schema = isTypeValid({ type }) ? schemas[type] : null;
  return (schema ? recursiveSchemaFromKey(schemas, schema, keys.join('.')) : null) || undefined;
};
