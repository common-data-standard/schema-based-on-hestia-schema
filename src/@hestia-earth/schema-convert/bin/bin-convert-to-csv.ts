#!/usr/bin/env node
import { run } from '../convert-csv';

run()
  .then(paths => {
    console.log('Done converting', paths.length, 'files.');
    console.log(paths.join('\n'));
    process.exit(0);
  })
  .catch(err => {
    console.log('Error converting files.');
    console.error(err);
    process.exit(1);
  });
