import {
  SchemaType,
  JSON as HestiaJson,
  Cycle,
  Product,
  Site,
  Organisation,
  Term,
  ImpactAssessment
} from '@hestia-earth/schema';

import { ellipsis, keyToLabel, diffInDays, reduceUndefinedValues } from './utils';

const findLinkedNode = <T extends SchemaType, R extends HestiaJson<T>>(
  nodes: HestiaJson<SchemaType>[],
  type: T,
  node?: R
) =>
  node
    ? (nodes.find(
        v => (v.type === type && v.id === node.id) || (v['@type'] === type && v['@id'] === node['@id'])
      ) as R) || node
    : undefined;

export const siteLocationName = (region?: Term, country?: Term) =>
  [
    region?.name,
    // make sure country doesnt appear in region already, if so don't add it
    country?.name ? (region?.name && region.name.includes(country?.name) ? undefined : country?.name) : undefined
  ]
    .filter(Boolean)
    .join(', ');

export const primaryProduct = (products?: Product[], defaultValue = {} as Product) =>
  (products && products.length
    ? products.find(product => product.primary || product.economicValueShare > 50) || products[0]
    : null) || defaultValue;

const defaultName = (
  product?: Term,
  country?: Term,
  region?: Term,
  endDate?: string,
  treatment?: string,
  description?: string
) =>
  [
    product?.name || 'No Product',
    siteLocationName(region, country),
    endDate,
    ellipsis(treatment, 20),
    ellipsis(description, 30)
  ]
    .filter(Boolean)
    .join(' - ');

export const cycleDefaultName = ({ endDate, treatment, description }: Cycle, site?: Site, product?: Product) =>
  defaultName(product?.term, site?.country, site?.region, endDate, treatment, description);

const cycleDurationDefault = 365;

export const calculateCycleDuration = ({ cycleDuration, startDate, endDate }: Cycle) =>
  (!cycleDuration || cycleDuration === cycleDurationDefault) &&
  startDate &&
  endDate &&
  new Date(startDate).getTime() < new Date(endDate).getTime();

export const cycleDefaultDuration = ({ startDate, endDate }: Cycle) => diffInDays(startDate, endDate);

export const extendCycle = (nodes: HestiaJson<SchemaType>[], cycle: Cycle): Cycle => {
  const product = primaryProduct(cycle.products);
  const site = findLinkedNode(nodes, SchemaType.Site, cycle.site);
  return {
    ...cycle,
    name: cycleDefaultName(cycle, site, product),
    ...(calculateCycleDuration(cycle) ? { cycleDuration: cycleDefaultDuration(cycle) } : {})
  };
};

export const impactAssessmentDefaultName = ({ product, country, region, endDate }: Partial<ImpactAssessment>) =>
  defaultName(product?.term, country, region, endDate);

export const extendImpactAssessment = (nodes: HestiaJson<SchemaType>[], impactAssessment: ImpactAssessment) => {
  const cycle = findLinkedNode(nodes, SchemaType.Cycle, impactAssessment.cycle);
  // replace the product if there is a single match in the Cycle products
  const products = cycle?.products?.filter(v => v.term?.['@id'] === impactAssessment?.product?.term?.['@id']);
  return {
    ...impactAssessment,
    name: impactAssessment.name || impactAssessmentDefaultName(impactAssessment),
    source: impactAssessment.source || cycle?.defaultSource,
    product: products?.length === 1 ? products[0] : impactAssessment.product
  };
};

export const siteDefaultName = ({ siteType, region, country, description }: Site, organisation?: Organisation) =>
  [
    siteType ? keyToLabel(siteType) : null,
    organisation?.name,
    siteLocationName(region, country),
    ellipsis(description, 30)
  ]
    .filter(Boolean)
    .join(' - ');

export const extendSite = (nodes: HestiaJson<SchemaType>[], site: Site): Site => {
  const org = findLinkedNode(nodes, SchemaType.Organisation, site.organisation);
  return {
    ...site,
    name: siteDefaultName(site, org)
  };
};

const extendNodeType: {
  [type in SchemaType]?: (nodes: HestiaJson<SchemaType>[], node: HestiaJson<SchemaType>) => Partial<HestiaJson<type>>;
} = {
  [SchemaType.Cycle]: extendCycle,
  [SchemaType.ImpactAssessment]: extendImpactAssessment,
  [SchemaType.Site]: extendSite
};

const extendNode = (nodes: HestiaJson<SchemaType>[]) => (node: HestiaJson<SchemaType>) =>
  node.type in extendNodeType ? reduceUndefinedValues(extendNodeType[node.type](nodes, node), true) : node;

export const setDefaultValues = (nodes: HestiaJson<any>[]) => nodes.map(extendNode(nodes));
