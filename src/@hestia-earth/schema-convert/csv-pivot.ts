import { json2csv } from 'json-2-csv';

import { omit, reduceUndefinedValues } from './utils';

const omitKeys = [
  '@context',
  'schemaVersion',
  'dataPrivate',
  ...['bibliography', 'site', 'cycle', 'impactAssessment', 'defaultSource', 'source'].flatMap(k => [
    `${k}.id`,
    `${k}.type`,
    `${k}.@type`
  ])
];

const convertValue = (value: any) =>
  Array.isArray(value) ? value.map(String).join(';') : typeof value === 'object' ? JSON.stringify(value) : value;

const parseValue = (value: any) => {
  const result = convertValue(value);
  return typeof result === undefined || result === null ? '' : result.toString();
};

export const toCsv = (nodes: any[], keys?: string[], excludeKeys = omitKeys) =>
  json2csv(nodes, {
    keys,
    excludeKeys,
    emptyFieldValue: '-',
    expandArrayObjects: true,
    arrayIndexesAsKeys: true,
    sortHeader: true,
    parseValue
  });

const arrayKeys = (node: any) =>
  Object.keys(node).filter(
    key =>
      Array.isArray(node[key]) &&
      node[key].length &&
      typeof node[key][0] === 'object' &&
      ('type' in node[key][0] || '@type' in node[key][0])
  );

const termId = (term: any) => (term ? term['@id'] || term.id : undefined);

const convertTerm = (term: any) => (term ? { methodModel: termId(term) } : {});

const pivotList = (values: any[]) =>
  values.reduce((prev, { '@type': _t, term, methodModel, value }) => {
    const key = termId(term);
    const data = reduceUndefinedValues({
      ...convertTerm(methodModel),
      value: convertValue(value)
    });
    return {
      ...prev,
      ...(key && Object.keys(data).length ? { [key]: (prev[key] || []).concat([data]) } : {})
    };
  }, {});

export const pivotNode = (node: any) => {
  const keys = arrayKeys(node);
  return reduceUndefinedValues({
    ...omit(node, [...omitKeys, ...keys]),
    ...keys.reduce((prev, key) => {
      const value = pivotList(node[key]);
      return {
        ...prev,
        ...(Object.keys(value).length ? { [key]: value } : {})
      };
    }, {})
  });
};

/**
 * CSV format for data processing.
 *
 * @param nodes List of nodes to convert.
 * @returns CSV content formatted with pivoted blank nodes.
 */
export const toCsvPivot = (nodes: any[]) => toCsv(nodes.map(pivotNode));
