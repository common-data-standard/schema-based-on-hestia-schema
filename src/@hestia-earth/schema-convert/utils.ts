const unset = require('lodash.unset');

export const omit = (data: any, keys: string[]) => {
  const obj = { ...data };
  keys.forEach(key => unset(obj, key));
  return obj;
};

export const isExpandable = (val: any) =>
  !!val && typeof val === 'object' && (Array.isArray(val) ? val.every(isExpandable) : Object.keys(val).length);

export const isIri = (value?: string) => (value || '').startsWith('http');

export const isBoolean = (value: string) => value.toLowerCase() === 'true' || value.toLowerCase() === 'false';

export const isNumber = (n: string | number) => !isNaN(parseFloat(`${n}`)) && isFinite(parseFloat(`${n}`));

const ignoreKey = (key: string) => !['@type', 'type'].includes(key);

export const isEmpty = (value: any, minKeys = 1) =>
  value === null ||
  typeof value === 'undefined' ||
  (typeof value === 'object'
    ? Array.isArray(value)
      ? !value.length
      : Object.keys(value).filter(key => key !== 'type').length < minKeys
    : value === '');

export const nonEmptyValue = (value: any) => !isEmpty(value) && value !== '-';

export const nonEmptyNode = (node: any) =>
  typeof node === 'object'
    ? Object.keys(node).filter(key => ignoreKey(key) && !isEmpty(node[key])).length > 0
    : nonEmptyValue(node);

const isUndefined = <T>(value: T, allowNull: boolean) =>
  (value === null && !allowNull) ||
  typeof value === 'undefined' ||
  (typeof value === 'object' && value !== null && !(value instanceof Date) && !Object.keys(value).length);

export const reduceUndefinedValues = <T>(obj: T, allowNull = false) =>
  Object.keys(obj).reduce((prev, key) => {
    const value = obj[key];
    return { ...prev, ...(isUndefined(value, allowNull) ? {} : { [key]: value }) };
  }, {} as Partial<T>);

export const ellipsis = (text = '', maxlength = 20) =>
  text.length > maxlength ? `${text.substring(0, maxlength)}...` : text;

export const keyToLabel = (key: string) =>
  `${key[0].toUpperCase()}${key
    .replace(/([a-z])([A-Z])/g, '$1 $2')
    .replace(/([_])([a-zA-Z])/g, g => ` ${g[1].toUpperCase()}`)
    .substring(1)}`;

export const diffInDays = (date1: Date | string, date2: Date | string) =>
  Math.abs(Math.round((new Date(date2).getTime() - new Date(date1).getTime()) / (24 * 60 * 60 * 1000)));
