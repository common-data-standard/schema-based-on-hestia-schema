import { expect } from 'chai';
import * as sinon from 'sinon';
import 'mocha';

import * as json2csv from 'json-2-csv';
import { pivotNode, toCsv, toCsvPivot } from './csv-pivot';
import * as specs from './csv-pivot';

let stubs: sinon.SinonStub[] = [];

describe('schema-convert/csv-pivot', () => {
  const cycle = {
    type: 'Cycle',
    id: 'id',
    inputs: [
      {
        type: 'Input',
        term: {
          '@type': 'Term',
          '@id': 'term-1',
          name: 'Term 1'
        },
        inputs: [
          {
            '@type': 'Term',
            '@id': 'term-2',
            name: 'Term 2'
          }
        ],
        value: [100, 200]
      }
    ],
    practices: [
      {
        type: 'Practice',
        term: {
          '@type': 'Term',
          '@id': 'term-3',
          name: 'Term 3'
        }
      }
    ]
  };

  beforeEach(() => {
    stubs = [];
  });

  afterEach(() => {
    stubs.forEach(stub => stub.restore());
  });

  describe('pivotNode', () => {
    it('should pivot blank nodes', () => {
      expect(pivotNode(cycle)).to.deep.equal({
        type: 'Cycle',
        id: 'id',
        inputs: {
          'term-1': [
            {
              value: '100;200'
            }
          ]
        }
      });
    });
  });

  describe('toCsv', () => {
    let csvStub: sinon.SinonStub;

    beforeEach(() => {
      stubs.push((csvStub = sinon.stub(json2csv, 'json2csv').returns('')));
    });

    it('should convert', () => {
      toCsv([]);
      expect(csvStub.called).to.equal(true);
    });
  });

  describe('toCsvPivot', () => {
    const nodes = [cycle];
    const data = 'random data';

    beforeEach(() => {
      stubs.push(sinon.stub(specs, 'toCsv').returns(data));
    });

    it('should upload the csv', () => {
      expect(toCsvPivot(nodes)).to.equal(data);
    });
  });
});
