import { expect } from 'chai';
import * as sinon from 'sinon';
import 'mocha';
import { loadSchemas } from '../json-schema';
import { SchemaType } from '../schema';

import { readFileAsString } from '../../../test/utils';
import { ICSVErrors, cleanStringValue, toJson } from './json';
import * as defaultValues from './default-values';

const fixturesFolder = 'schema-convert/json';

const readFixture = (filename: string) => readFileAsString(`${fixturesFolder}/${filename}`);

let stubs: sinon.SinonStub[] = [];

describe('schema-convert/json', () => {
  beforeEach(() => {
    stubs = [];

    stubs.push(sinon.stub(defaultValues, 'setDefaultValues').callsFake(v => v));
  });

  afterEach(() => {
    stubs.forEach(stub => stub.restore());
  });

  describe('cleanStringValue', () => {
    it('should remove extra floating 0', () => {
      expect(cleanStringValue('1960.0')).to.equal('1960');
      expect(cleanStringValue('1960.01')).to.equal('1960.01');

      expect(cleanStringValue('test with .0 anywhere')).to.equal('test with .0 anywhere');
      // could be a version number, so keep the .0 suffix
      expect(cleanStringValue('18.12.0')).to.equal('18.12.0');
    });
  });

  describe('toJson', () => {
    const schemas = loadSchemas();

    it('should convert sample-1', async () => {
      const filekey = 'sample-1';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));
      const expected = JSON.parse(readFixture(`${filekey}.json`));
      expect({ nodes }).to.deep.equal(expected);
    });

    it('should convert sample-2', async () => {
      const filekey = 'sample-2';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));
      const expected = JSON.parse(readFixture(`${filekey}.json`));
      expect({ nodes }).to.deep.equal(expected);
    });

    it('should convert sample-3', async () => {
      const filekey = 'sample-3';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));
      const expected = JSON.parse(readFixture(`${filekey}.json`));
      expect({ nodes }).to.deep.equal(expected);
    });

    it('should detect delimiter', async () => {
      const filekey = 'delimiter-semi-colon';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));
      expect(nodes.length).to.equal(2);
    });

    it('should generate nested node with no id column', async () => {
      const filekey = 'default-node-no-id';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));
      const cycle = nodes.find(({ type }) => type === SchemaType.Cycle);

      expect(cycle.defaultSource).to.deep.equal({
        type: 'Source',
        id: '1'
      });

      expect(cycle.inputs[0].term).to.deep.equal({
        type: 'Term',
        name: 'Seed'
      });

      expect(cycle.practices[0].term).to.deep.equal({
        type: 'Term',
        name: 'Organic'
      });
    });

    it('should set default values', async () => {
      const filekey = 'default-values';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));
      const cycle = nodes.find(({ type }) => type === SchemaType.Cycle);
      expect(cycle.dataPrivate).to.equal(false);
      expect(cycle.completeness.product).to.equal(true);
      // column is present but there is no value
      const source = nodes.find(({ type }) => type === SchemaType.Source);
      expect(source.dataPrivate).to.equal(false);
    });

    it('should NOT set default values on nested nodes except Actor', async () => {
      const filekey = 'default-values';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));
      const cycle = nodes.find(({ type }) => type === SchemaType.Cycle);
      expect(cycle.defaultSource.dataPrivate).to.equal(undefined);
      const source = nodes.find(({ type }) => type === SchemaType.Source);
      expect(source.bibliography.authors[0].dataPrivate).to.equal(false);
    });

    it('should handle default null value', async () => {
      const filekey = 'default-null';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));
      expect(nodes[0].impacts[0].value).to.equal(null);
    });

    it('should handle property as ref', async () => {
      const filekey = 'property-as-ref';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));

      expect(nodes[0].region).to.deep.equal({
        type: 'Term',
        name: 'Srpski Krstur'
      });
    });

    it('should handle empty numbers', async () => {
      const filekey = 'empty-numbers';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));

      expect(nodes[0]).to.deep.equal({
        type: 'ImpactAssessment',
        id: '1',
        impacts: [
          {
            type: 'Indicator',
            term: {
              '@type': 'Term',
              '@id': 'biodiversityLossTotalLandUseEffects'
            },
            value: null,
            observations: 10
          }
        ],
        irrigated: false,
        organic: false,
        autoGenerated: false,
        dataPrivate: false
      });
    });

    it('should handle null values in array', async () => {
      const filekey = 'array-null-values';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));
      const expected = JSON.parse(readFixture(`${filekey}.json`));
      expect({ nodes }).to.deep.equal(expected);
    });

    it('should skip empty columns', async () => {
      const filekey = 'empty-headers';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));
      expect(nodes.length).to.equal(42);
    });

    it('should not generate empty nodes', async () => {
      const filekey = 'empty-nodes';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));
      const cycle = nodes.find(({ type }) => type === SchemaType.Cycle);

      expect(cycle.inputs).not.to.deep.includes({
        term: {
          type: SchemaType.Term,
          name: '-'
        },
        type: SchemaType.Input
      });

      expect(cycle.emissions).not.to.deep.includes({
        source: {
          type: SchemaType.Source
        },
        type: SchemaType.Emission
      });
    });

    it('should not generate empty IRI', async () => {
      const filekey = 'empty-iri';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));
      expect(nodes[0].website).to.equal(undefined);

      expect(nodes[1].website).to.deep.equal({
        '@id': 'https://test.com'
      });
    });

    it('should skip empty Terms', async () => {
      const filekey = 'empty-terms';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));
      const sites = nodes.filter(({ type }) => type === SchemaType.Site);
      expect(sites[0].practices).to.equal(undefined);
    });

    it('should determine values from valueType', async () => {
      const filekey = 'valueType';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));

      expect(nodes[0].emissions[0].properties[0]).to.deep.equal({
        type: SchemaType.Property,
        value: 123,
        share: 100
      });

      expect(nodes[1].emissions[0].properties[0]).to.deep.equal({
        type: SchemaType.Property,
        value: 0,
        share: 100
      });

      expect(nodes[2].emissions[0].properties[0]).to.deep.equal({
        type: SchemaType.Property,
        value: true,
        share: 100
      });

      expect(nodes[3].emissions[0].properties[0]).to.deep.equal({
        type: SchemaType.Property,
        value: false,
        share: 100
      });
    });

    it('should handle mutliple types', async () => {
      const filekey = 'multiple-types';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));

      expect(nodes[0].practices[0]).to.deep.equal({
        type: SchemaType.Practice,
        value: [10]
      });

      expect(nodes[1].practices[0]).to.deep.equal({
        type: SchemaType.Practice,
        value: ['test']
      });
    });

    it('should handle boolean as numbers', async () => {
      const filekey = 'boolean-as-numbers';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));

      expect(nodes[0].completeness).to.deep.equal({
        type: SchemaType.Completeness,
        product: true,
        cropResidue: false
      });
    });

    it('should handle GeoJSON', async () => {
      const filekey = 'geojson';
      const nodes = await toJson(schemas, readFixture(`geojson/${filekey}.csv`));
      const expected = JSON.parse(readFixture(`geojson/${filekey}.json`));
      expect({ nodes }).to.deep.equal(expected);
    });

    it('should not generate invalid GeoJson', async () => {
      const filekey = 'invalid-geojson';
      try {
        await toJson(schemas, readFixture(`geojson/${filekey}.csv`));
        expect(true).to.equal(false);
      } catch (error) {
        const { errors } = JSON.parse(error.message) as ICSVErrors;
        expect(errors[0].message).to.equal('property-invalid-format');
        expect(errors[0].error).to.equal('Unable to parse GeoJSON value. Please make sure the formatting is correct.');
      }
    });

    it('should not generate empty GeoJson', async () => {
      const filekey = 'empty';
      try {
        await toJson(schemas, readFixture(`geojson/${filekey}.csv`));
        expect(true).to.equal(false);
      } catch (error) {
        const { errors } = JSON.parse(error.message) as ICSVErrors;
        expect(errors[0].message).to.equal('property-invalid-format');

        expect(errors[0].error).to.equal(
          'This GeoJSON appears to be empty. Please either add coordinates or remove the field.'
        );
      }
    });

    it('should convert WTK to GeoJSON format', async () => {
      const filekey = 'valid-wtk';
      const nodes = await toJson(schemas, readFixture(`geojson/${filekey}.csv`));
      const expected = JSON.parse(readFixture(`geojson/${filekey}.json`));
      expect({ nodes }).to.deep.equal(expected);
    });

    it('should not generate invalid WTK type', async () => {
      const filekey = 'invalid-wtk';
      try {
        await toJson(schemas, readFixture(`geojson/${filekey}.csv`));
        expect(true).to.equal(false);
      } catch (error) {
        const { errors } = JSON.parse(error.message) as ICSVErrors;
        expect(errors[0].message).to.equal('property-invalid-format');
        expect(errors[0].error).to.equal('use a "Polygon" instead of "MultiPoint"');
      }
    });

    it('should not generate with invalid node', async () => {
      const filekey = 'schema-not-found';
      try {
        await toJson(schemas, readFixture(`${filekey}.csv`));
        expect(true).to.equal(false);
      } catch (error) {
        const { errors } = JSON.parse(error.message) as ICSVErrors;
        expect(errors).to.deep.equal([{ message: filekey }]);
      }
    });

    it('should not generate with unsupported node type', async () => {
      const filekey = 'schema-type-error';
      try {
        await toJson(schemas, readFixture(`${filekey}.csv`));
        expect(true).to.equal(false);
      } catch (error) {
        const { errors } = JSON.parse(error.message) as ICSVErrors;
        expect(errors).to.deep.equal([{ message: 'schema-not-found', schema: SchemaType.Completeness }]);
      }
    });

    it('should not generate with invalid properties', async () => {
      const filekey = 'property-not-found';
      try {
        await toJson(schemas, readFixture(`${filekey}.csv`));
        expect(true).to.equal(false);
      } catch (error) {
        const { errors } = JSON.parse(error.message) as ICSVErrors;

        expect(errors).to.deep.equal([
          {
            schema: SchemaType.Completeness,
            message: filekey,
            schemaKey: 'random1',
            key: 'completeness.random1',
            value: { random1: 'true', random2: 'false' },
            suggestions: []
          },
          {
            schema: SchemaType.Completeness,
            message: filekey,
            schemaKey: 'random2',
            key: 'completeness.random2',
            value: { random1: 'true', random2: 'false' },
            suggestions: []
          },
          {
            schema: SchemaType.Cycle,
            message: filekey,
            schemaKey: 'practice',
            key: 'practice',
            value: {
              id: '1',
              completeness: { random1: 'true', random2: 'false' },
              practice: { term: { id: 'tillage' } }
            },
            suggestions: ['practices']
          }
        ]);
      }
    });

    it('should suggest using the name of the Term', async () => {
      const filekey = 'property-not-found-suggest-term';
      try {
        await toJson(schemas, readFixture(`${filekey}.csv`));
        expect(true).to.equal(false);
      } catch (error) {
        const { errors } = JSON.parse(error.message) as ICSVErrors;

        expect(errors).to.deep.equal([
          {
            schema: SchemaType.Term,
            message: 'property-not-found',
            schemaKey: 'term',
            key: 'impacts.1.inputs.0.term',
            value: {
              term: {
                name: 'Wheat, grain'
              }
            },
            suggestions: ['@id', 'name']
          }
        ]);
      }
    });

    it('should not generate with invalid format', async () => {
      const filekey = 'property-invalid-format';
      try {
        await toJson(schemas, readFixture(`${filekey}.csv`));
        expect(true).to.equal(false);
      } catch (error) {
        const { errors } = JSON.parse(error.message) as ICSVErrors;
        expect(errors[0].message).to.equal(filekey);
      }
    });

    it('should not generate with invalid number format', async () => {
      const filekey = 'number-with-commas';
      try {
        await toJson(schemas, readFixture(`${filekey}.csv`));
        expect(true).to.equal(false);
      } catch (error) {
        const { errors } = JSON.parse(error.message) as ICSVErrors;
        expect(errors[0].message).to.equal('property-invalid-format');
      }
    });

    it('should not generate with invalid boolean format', async () => {
      const filekey = 'incorrect-boolean-format';
      try {
        await toJson(schemas, readFixture(`${filekey}.csv`));
        expect(true).to.equal(false);
      } catch (error) {
        const { errors } = JSON.parse(error.message) as ICSVErrors;
        expect(errors[0].message).to.equal('property-invalid-format');
      }
    });

    it('should no generate with array that has no position', async () => {
      const filekey = 'object-array-invalid';
      try {
        await toJson(schemas, readFixture(`${filekey}.csv`));
        expect(true).to.equal(false);
      } catch (error) {
        const { errors } = JSON.parse(error.message) as ICSVErrors;
        expect(errors[0].message).to.equal(filekey);
      }
    });

    it('should generate with duplicated id fields not on the same node', async () => {
      const filekey = 'duplicated-id-fields-different-nodes';
      const nodes = await toJson(schemas, readFixture(`${filekey}.csv`));

      expect(nodes[0]).to.deep.equal({
        type: SchemaType.Cycle,
        id: '1',
        inputs: [
          {
            type: SchemaType.Input,
            impactAssessment: {
              type: SchemaType.ImpactAssessment,
              id: '2'
            }
          }
        ],
        dataPrivate: false,
        cycleDuration: 365
      });
    });

    it('should no generate with duplicated id fields', async () => {
      const filekey = 'duplicated-id-fields';
      try {
        await toJson(schemas, readFixture(`${filekey}.csv`));
        expect(true).to.equal(false);
      } catch (error) {
        const { errors } = JSON.parse(error.message) as ICSVErrors;

        expect(errors).to.deep.equal([
          {
            schema: SchemaType.Input,
            message: filekey,
            schemaKey: 'impactAssessment',
            key: 'inputs.0.impactAssessment',
            value: { id: '2', '@id': '1' }
          }
        ]);
      }
    });
  });
});
