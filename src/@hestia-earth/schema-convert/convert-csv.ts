import { promises } from 'fs';
import { join } from 'path';

import { toCsvPivot } from './csv-pivot';
import { toCsv } from './csv';

const [path] = process.argv.slice(2);

const { readdir, readFile, writeFile, lstat } = promises;
const encoding = 'utf8';

const listFiles = async folder => {
  const files = await readdir(folder);
  return files.filter(file => file.endsWith('.jsonld') || file.endsWith('.json'));
};

const processFile = async (filepath: string, pivot: boolean) => {
  const data = await readFile(filepath, encoding);
  const content = JSON.parse(data.trim());
  const nodes = Array.isArray(content) ? content : 'nodes' in content ? content.nodes : [content];
  const res = pivot ? toCsvPivot(nodes) : toCsv(nodes);
  const destPath = filepath.replace('.jsonld', '.csv').replace('.json', '.csv');
  await writeFile(destPath, res, encoding);
  return destPath;
};

export const run = async (pivot = false) => {
  const stat = await lstat(path);
  return Promise.all(
    stat.isDirectory()
      ? (await listFiles(path)).map(file => processFile(join(path, file), pivot))
      : [processFile(path, pivot)]
  );
};
