import { promises } from 'fs';
import { join } from 'path';
import { loadSchemas, definitions } from '@hestia-earth/json-schema';

import { toJson } from './json';

const [path] = process.argv.slice(2);

const { readdir, readFile, writeFile, lstat } = promises;
const encoding = 'utf8';

const listFiles = async folder => {
  const files = await readdir(folder);
  return files.filter(file => file.endsWith('.csv'));
};

const processFile = async (schemas: definitions, filepath: string) => {
  const content = await readFile(filepath, encoding);
  const nodes = await toJson(schemas, content);
  const destPath = filepath.replace('.csv', '.json');
  await writeFile(destPath, JSON.stringify({ nodes }, null, 2), encoding);
  return destPath;
};

export const run = async () => {
  const stat = await lstat(path);
  const schemas = loadSchemas();
  return Promise.all(
    stat.isDirectory()
      ? (await listFiles(path)).map(file => processFile(schemas, join(path, file)))
      : [processFile(schemas, path)]
  );
};
