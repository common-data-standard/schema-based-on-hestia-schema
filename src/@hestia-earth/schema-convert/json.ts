import * as csvtojson from 'csvtojson';
import * as levenshtein from 'fast-levenshtein';
import { wktToGeoJSON } from '@terraformer/wkt';
import { NodeType, typeToSchemaType, refToSchemaType, SchemaType, isTypeNode, isBlankNode } from '@hestia-earth/schema';
import {
  IDefinition,
  IDefinitionObject,
  IDefinitionArray,
  definitions,
  definition,
  defaultProperties,
  excludedDefaultProperties
} from '@hestia-earth/json-schema/types';

import { nonEmptyValue, reduceUndefinedValues, isEmpty, isIri, isBoolean, isNumber } from './utils';
import { setDefaultValues } from './default-values';

export const acceptedNodeTypes = Object.values(NodeType);

export interface INode {
  type: SchemaType;
  id?: string;
  name?: string;
}

export interface ICSVContent {
  [key: string]: string | ICSVContent;
}

interface ICSVHeader {
  name: string;
  index: number;
}

export interface ICSVError {
  message: string;
  schema?: SchemaType | string;
  schemaKey?: string;
  key?: string;
  property?: string;
  value?: any;
  error?: string;
  headers?: ICSVHeader[];
  suggestions?: string[];
}

export interface ICSVErrors {
  errors: ICSVError[];
}

export enum ErrorKeys {
  SchemaNotFound = 'schema-not-found',
  PropertyNotFound = 'property-not-found',
  PropertyInvalidFormat = 'property-invalid-format',
  DuplicatedIdFields = 'duplicated-id-fields',
  ObjectArrayInvalid = 'object-array-invalid'
}

const IGNORE_FIELD_KEY = '-';
const VALUE_TYPE_KEY = 'valueType';
const DEFAULT_ARRAY_DELIMITER = ';';

const arrayDelimiter = () => {
  try {
    return process.env.CSV_ARRAY_DELIMITER || DEFAULT_ARRAY_DELIMITER;
  } catch (err) {
    return DEFAULT_ARRAY_DELIMITER;
  }
};

const safeParseJSON = <T>(obj: string) => {
  // throw already handled error or throw a generic invalid-format error
  let data: T;
  try {
    data = JSON.parse(obj);
  } catch (err) {}
  return data;
};

const internalProperties = ({ properties }: Partial<IDefinition>) =>
  Object.keys(properties).filter(property => properties[property].internal);

const isEmptyCell = (value: any) => value === IGNORE_FIELD_KEY || value === '';
const nonEmptyCell = (value: any) => !isEmptyCell(value);

const isEmptyNode = (node: any, schemas: definitions) => {
  const schema = node.type ? schemas[node.type] : null;
  const properties = Object.keys(node).filter(key => !excludedDefaultProperties.includes(key));
  const defaultProperties = schema ? getDefaultProperties(schema).map(({ property }) => property) : [];
  // get the list of properties that do not have a default value
  const nonDefaultProperties = properties.filter(p => !defaultProperties.includes(p));
  return nonDefaultProperties.length === 0;
};

const isEmptyValueType = {
  undefined: () => true,
  number: (value: any) => isNaN(value),
  [SchemaType.Term]: (value: any) => !(value['@id'] || value.id || value.name),
  blankNode: isEmptyNode,
  array: (value: any[]) => value.filter(v => !isEmpty(v)).length === 0,
  object: (value: any, schemas: definitions) =>
    value === null
      ? false
      : Array.isArray(value)
      ? isEmptyValueType.array(value)
      : (value['@type'] || value.type) === SchemaType.Term
      ? isEmptyValueType[SchemaType.Term](value)
      : isBlankNode(value)
      ? isEmptyValueType.blankNode(value, schemas)
      : Object.keys(value).length === 0
};

const isEmptyValue = (value: any, schemas: definitions) =>
  typeof value in isEmptyValueType ? isEmptyValueType[typeof value](value, schemas) : isEmptyCell(value);

const compileFullKey = (key1: string, key2: string) => {
  // handle arrays
  const joinKey = (key2 || '').startsWith('[') ? '' : '.';
  return [key1, key2].filter(Boolean).join(joinKey);
};

const propertyRequiredValue = (value: any, required: string[]) => {
  const val = typeof value === 'object' && required[0] in value ? value[required[0]] : value;
  return nonEmptyCell(val) ? { [required[0]]: val } : {};
};

const parseErrors = (err: Error) => safeParseJSON<ICSVErrors>(err.message)?.errors || [];

const throwError = (error: string) => {
  throw new Error(error);
};

export const throwCSVErrors = <T extends ICSVError>(errors: T[]) => throwError(JSON.stringify({ errors }));

const schemaNotFoundError = (schema: SchemaType) =>
  throwCSVErrors([
    {
      message: ErrorKeys.SchemaNotFound,
      schema
    }
  ]);

const computeSuggestions = ({ title, properties }: IDefinition, key: string) => {
  const internal = internalProperties({ properties });
  const allKeys = Object.keys(properties).filter(k => ![...excludedDefaultProperties, ...internal].includes(k));
  return [title === SchemaType.Term].every(Boolean)
    ? ['@id', 'name']
    : allKeys.filter(v => levenshtein.get(v, key) <= 3);
};

const propertyNotFoundError = (schema: IDefinition, key: string, value?: any): definition =>
  throwCSVErrors([
    {
      schema: schema.title,
      message: ErrorKeys.PropertyNotFound,
      schemaKey: key,
      key,
      value,
      suggestions: computeSuggestions(schema, key)
    }
  ]);

const propertyInvalidFormat = (schema: IDefinition, key: string, value: any, func) => {
  try {
    return func();
  } catch (err) {
    const errors = parseErrors(err);
    // throw already handled error or throw a generic invalid-format error
    return throwCSVErrors(
      errors.length
        ? errors.map(data => ({ ...data, key: compileFullKey(key, data.key) }))
        : [
            {
              schema: schema.title,
              message: ErrorKeys.PropertyInvalidFormat,
              schemaKey: key,
              key,
              value,
              error: err?.message
            }
          ]
    );
  }
};

const validateDuplicatedIdFields = (schema: IDefinition, key: string, value: any) =>
  typeof value === 'object' && [value.id, value['@id']].every(v => !!v && nonEmptyCell(v))
    ? throwCSVErrors([
        {
          schema: schema.title,
          message: ErrorKeys.DuplicatedIdFields,
          schemaKey: key,
          key,
          value
        }
      ])
    : null;

const handleArrayError = func => (value, index: number) => {
  try {
    return func(value, index);
  } catch (err) {
    const errors = parseErrors(err);
    // throw already handled error or throw a generic invalid-format error
    return errors.length
      ? throwCSVErrors(errors.map(data => ({ ...data, key: compileFullKey(`${index}`, data.key) })))
      : (() => {
          throw err;
        })();
  }
};

const allowedGeoJSONTypes = ['FeatureCollection', 'Feature', 'GeometryCollection'];

const geoJSONGeomtryTypeValidation = {
  Point: () => throwError('use "latitude" and "longitude" instead of "Point"'),
  MultiPoint: () => throwError('use a "Polygon" instead of "MultiPoint"'),
  LineString: () => throwError('use a "Polygon" instead of "LineString"'),
  MultiLineString: () => throwError('use a "MultiPolygon" instead of "MultiLineString"')
};

const validateGeoJSONGeometryType = ({ type }) =>
  type in geoJSONGeomtryTypeValidation ? geoJSONGeomtryTypeValidation[type]() : true;

/**
 * Validate the GeoJSON format provided.
 * GeoJSON is only relevant if it includes a `Polygon` or `MultiPolygon`, which would represent an area.
 * If a single `Point` is provided, `latitude` and `longitude` should be used instead.
 *
 * @param value The GeoJSON as object.
 */
const validateGeoJSONFormat = (value: any) =>
  (
    !allowedGeoJSONTypes.includes(value.type)
      ? throwError(`Invalid GeoJSON "type". Allowed values are: ${allowedGeoJSONTypes.join(', ')}`)
      : 'geometries' in value
      ? !value.geometries.length
        ? throwError('This GeoJSON appears to be empty. Please either add coordinates or remove the field.')
        : (value.geometries || []).every(validateGeoJSONGeometryType)
      : 'geometry' in value
      ? validateGeoJSONGeometryType(value.geometry)
      : 'features' in value
      ? !value.features.length
        ? throwError('This GeoJSON appears to be empty. Please either add coordinates or remove the field.')
        : (value.features || []).every(validateGeoJSONFormat)
      : true
  )
    ? value
    : null;

const parseWtkValue = (value: any) => {
  try {
    return {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: wktToGeoJSON(value)
        }
      ]
    };
  } catch (error) {
    return null;
  }
};

const parseJsonValue = (value: any) => {
  try {
    const data = JSON.parse(value);
    return ['Polygon', 'MultiPolygon'].includes(data.type)
      ? {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {},
              geometry: data
            }
          ]
        }
      : ['Feature'].includes(data.type)
      ? {
          type: 'FeatureCollection',
          features: [
            {
              ...data,
              properties: {} // make sure they are set or it is invalid
            }
          ]
        }
      : data;
  } catch (error) {
    return throwError('Unable to parse GeoJSON value. Please make sure the formatting is correct.');
  }
};

const parseGeoJSONValue = (value: any) => {
  const result = parseWtkValue(value) || parseJsonValue(value);
  return result ? validateGeoJSONFormat(result) : value;
};

/**
 * If the user provided a non-object where an object was expected, assume it was meant to be the `name` property.
 * For Blank Nodes, we assume it is the `term.name`.
 * For non-Term Nodes, we assume it is the `id`.
 *
 * @param value The value mapped as a Ref
 */
const schemaRefValue = (value: any, type: SchemaType) =>
  typeof value === 'object'
    ? value
    : type === SchemaType.Term
    ? { name: value }
    : isBlankNode({ type })
    ? { term: { name: value } }
    : { id: value };

export const cleanStringValue = (value: string) =>
  (value.match(/^[\d]+\.[\d]+\.[\d]+$/) === null ? value.replace(/\.0$/, '') : value).trim();

const propertyTypeToValue: {
  [type: string]: (value?: any, schemas?: definitions, def?: Partial<definition>, ignoreInternal?: boolean) => any;
} = {
  null: (value: string, ...args) => (isEmptyCell(value) ? null : propertyTypeToValue.auto(value, ...args)),
  string: (value: string): string => cleanStringValue(value || ''),
  number: (value: string): number =>
    isEmptyCell(value)
      ? undefined
      : isNaN(+value)
      ? (() => {
          throw new Error('failed to parse number');
        })()
      : +value,
  integer: (value: string): number => (isEmptyCell(value) ? undefined : +value),
  boolean: (value: string): boolean =>
    isEmptyCell(value)
      ? undefined
      : isBoolean(value)
      ? value.toLowerCase() === 'true'
      : isNumber(value)
      ? +value === 1 // handle setting 1 or 1.0 as true
      : (() => {
          throw new Error('failed to parse boolean, expected true or false');
        })(),
  object: (value: any, schemas, { $ref, required, geojson }: IDefinitionObject, _i) =>
    geojson
      ? propertyTypeToValue.geojson(value)
      : $ref
      ? propertyTypeToValue.$ref(value, schemas, { $ref }, _i)
      : propertyTypeToValue.required(value, schemas, { required }, _i),
  array: (value: string | any[], schemas, { items }: IDefinitionArray, _i) =>
    (Array.isArray(value) ? value : value.split(arrayDelimiter()))
      .map(
        handleArrayError(val =>
          items
            ? '$ref' in items
              ? propertyTypeToValue.object(val, schemas, items, _i)
              : Array.isArray(items.type)
              ? val === IGNORE_FIELD_KEY
                ? null
                : propertyTypeToValue.auto(val, schemas, items, _i)
              : propertyTypeToValue[items.type as any](val, schemas, items, _i)
            : val
        )
      )
      .filter(val => !isEmptyValue(val, schemas)),
  // try to determine the type automatically
  auto: (value: string, schemas, _d, _i) =>
    // iris are mapped as {@id: val}
    isIri(value)
      ? propertyTypeToValue.required(value, schemas, { required: ['@id'] }, _i)
      : isBoolean(value)
      ? propertyTypeToValue.boolean(value)
      : isNumber(value)
      ? propertyTypeToValue.number(value)
      : value === 'null'
      ? null
      : propertyTypeToValue.string(value),
  required: (value, _schemas, { required }: IDefinitionObject) => {
    const data = propertyRequiredValue(value, required);
    return isEmpty(data) ? {} : data;
  },
  $ref: (value, schemas, { $ref }: IDefinitionObject, _i) => {
    const schemaType = refToSchemaType($ref) as SchemaType;
    const schema = schemaType ? schemas[schemaType] : undefined;
    const data = schema
      ? mapContent(schemas, schema, _i)(schemaRefValue(value, schemaType))
      : $ref in propertyTypeToValue
      ? propertyTypeToValue[$ref](value)
      : safeParseJSON<any>(value);
    const includeDefaults = isBlankNode(data) || schemaType === SchemaType.Actor; // only blank nodes or actors allowed
    return isEmpty(data) ? {} : !!schema ? extendDataFromSchema(data, schema, schemaType, _i, includeDefaults) : data;
  },
  geojson: value => (isEmptyCell(value) ? undefined : parseGeoJSONValue(value))
};

const getPropertyDefinition = (schema: IDefinition, key: string, ignoreErrors = false, value?: any) =>
  key in schema.properties ? schema.properties[key] : !ignoreErrors && propertyNotFoundError(schema, key, value);

const getValueType = (schema: IDefinition, json: ICSVContent) =>
  VALUE_TYPE_KEY in schema.properties
    ? // valueType can be present but skipped
      (json[VALUE_TYPE_KEY] !== IGNORE_FIELD_KEY ? (json[VALUE_TYPE_KEY] as string) : null) ||
      schema.properties[VALUE_TYPE_KEY].default
    : null;

const getPropertyType = (def: definition, key: string, valueType?: string) =>
  // default type is object (like $ref)
  (key === 'value' && valueType
    ? valueType
    : Array.isArray(def.type)
    ? def.type.includes('null')
      ? 'null'
      : 'auto'
    : def.type) || 'object';

const setDefaultProperty = (prop: string, def: definition, node?: any, ignoreInternal = false) =>
  prop !== VALUE_TYPE_KEY &&
  // only get default if internal or not present
  ((!ignoreInternal && def.internal) || !node || !(prop in node));

const getDefaultProperties = (schema: IDefinition, node?: any, ignoreInternal = false): any[] =>
  defaultProperties(schema)
    .map((property: string) => {
      const def = schema.properties[property];
      return setDefaultProperty(property, def, node, ignoreInternal) ? { property, defaultValue: def.default } : null;
    }, {})
    .filter(Boolean);

const filterProperties = (data, excludedProps: string[]) =>
  Object.keys(data)
    .filter(key => !excludedProps.includes(key))
    .reduce((prev, curr) => ({ ...prev, [curr]: data[curr] }), {});

const nodeType = (id: string, type: SchemaType, existingNode = false) =>
  type ? (isTypeNode(type) ? (existingNode ? { '@type': type } : { id, type }) : { type }) : {};

const extendDataFromSchema = (
  { id, type: _t, ...data }: any,
  schema?: IDefinition,
  type?: SchemaType,
  ignoreInternal = false,
  addDefaults = false
) =>
  reduceUndefinedValues(
    {
      ...filterProperties(data, ignoreInternal ? [] : internalProperties(schema)),
      ...(addDefaults && !('@id' in data) && schema
        ? getDefaultProperties(schema, data, ignoreInternal).reduce((prev, { property, defaultValue }) => {
            prev[property] = defaultValue;
            return prev;
          }, {})
        : {}),
      // make sure the existing nodes are mapped to { @type, @id } without type/id
      ...nodeType(id, type, '@id' in data)
    },
    true
  );

const validateArrayType = (schema: IDefinition, def: Partial<definition>, key: string, value: any) =>
  typeof value !== 'object' ||
  def.type !== 'array' ||
  Array.isArray(value) ||
  throwCSVErrors([
    {
      schema: schema.title,
      message: ErrorKeys.ObjectArrayInvalid,
      schemaKey: key,
      key,
      value
    }
  ]);

const mapContent = (schemas: definitions, schema: IDefinition, ignoreInternal: boolean) => (json: ICSVContent) => {
  const values: (ICSVErrors & { value?: any[] })[] = Object.keys(json)
    .filter(nonEmptyCell)
    .map(key => {
      try {
        const value = json[key];
        // make sure we are not using both `id` and `@id` fields
        validateDuplicatedIdFields(schema, key, value);
        const propertyDefinition = getPropertyDefinition(schema, key, false, json);
        const valueType = getValueType(schema, json);
        const type = getPropertyType(propertyDefinition, key, valueType);
        validateArrayType(schema, propertyDefinition, key, value);
        const newValue =
          key === VALUE_TYPE_KEY
            ? ''
            : propertyInvalidFormat(schema, key, value, () =>
                Array.isArray(type)
                  ? value
                  : propertyTypeToValue[type](value, schemas, propertyDefinition, ignoreInternal)
              );

        return { errors: [], value: type !== 'null' && isEmptyValue(newValue, schemas) ? undefined : [key, newValue] };
      } catch (err) {
        const { errors } = JSON.parse(err.message) as ICSVErrors;
        return { errors };
      }
    });
  const errors = values.filter(({ errors }) => errors?.length > 0).flatMap(({ errors }) => errors);
  return errors?.length
    ? throwError(JSON.stringify({ errors }))
    : (Object.fromEntries(
        [schema.$id ? ['type', schema.title] : null, ...values.map(({ value }) => value)].filter(Boolean)
      ) as Partial<INode>);
};

export const formatNode = (
  schemas: definitions,
  type: SchemaType,
  data: ICSVContent,
  ignoreInternal: boolean,
  top = false
) => {
  const schema = type in schemas && acceptedNodeTypes.includes(type as any) ? schemas[type] : schemaNotFoundError(type);
  const content = mapContent(schemas, schema, ignoreInternal)(data);
  return reduceUndefinedValues(
    {
      id: isEmptyCell(data.id) ? undefined : data.id,
      ...extendDataFromSchema(content, schema, type, ignoreInternal, top)
    },
    true
  ) as any;
};

export const filterEmptyNode = (schemas: definitions, node: any) => {
  const schema = node.type ? schemas[node.type] : null;
  // make sure defaultProperties are not counted
  return !!schema && !isEmptyNode(node, schemas);
};

const convetToNode = (schemas: definitions) => (data: any) =>
  Object.keys(data)
    .filter(nonEmptyValue)
    .map(type => formatNode(schemas, typeToSchemaType(type), data[type], true, true))
    .filter(node => filterEmptyNode(schemas, node));

/**
 * Convert CSV to Hestia JSON-LD format.
 *
 * @param schemas The definitions of the Hestia Schema (`import { loadSchemas } from "@hestia-earth/json-schema"`)
 * @param content The content of the CSV as a string
 * @returns A list of JSON-LD content.
 */
export const toJson = async (schemas: definitions, content: string): Promise<any[]> => {
  const nodes = (
    await csvtojson({
      delimiter: 'auto',
      ignoreColumns: /^$/
    }).fromString(content)
  ).flatMap(convetToNode(schemas));
  return setDefaultValues(nodes);
};
