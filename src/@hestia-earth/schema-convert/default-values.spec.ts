import { expect } from 'chai';
import * as sinon from 'sinon';
import 'mocha';
import { SchemaType, SiteSiteType, Cycle, Site, ImpactAssessment, Source } from '@hestia-earth/schema';

import { extendCycle, extendSite, extendImpactAssessment, primaryProduct, setDefaultValues } from './default-values';

let stubs: sinon.SinonStub[] = [];

describe('schema-convert/default-values', () => {
  beforeEach(() => {
    stubs = [];
  });

  afterEach(() => {
    stubs.forEach(stub => stub.restore());
  });

  describe('extendCycle', () => {
    const nodes = [
      {
        type: SchemaType.Site,
        id: 'site-id',
        region: {
          name: 'Champagne, France'
        },
        country: {
          name: 'France'
        }
      }
    ];
    const cycle: Cycle = {
      type: SchemaType.Cycle,
      endDate: '1998-05',
      treatment: 'UAN treatment',
      site: {
        type: SchemaType.Site,
        id: 'site-id'
      },
      products: [
        {
          type: SchemaType.Product,
          term: {
            type: SchemaType.Term,
            name: 'Grapes, for wine'
          }
        }
      ]
    };

    it('should set the name', () => {
      const { name } = extendCycle(nodes, cycle);
      expect(name).to.equal('Grapes, for wine - Champagne, France - 1998-05 - UAN treatment');
    });

    describe('with reduced info', () => {
      it('should set the name', () => {
        const { name } = extendCycle(nodes, {
          type: SchemaType.Cycle,
          endDate: '1998-05'
        });
        expect(name).to.equal('No Product - 1998-05');
      });
    });

    describe('with startDate and endDate', () => {
      beforeEach(() => {
        cycle.startDate = '2018-06-01';
        cycle.endDate = '2019-01-01';
      });

      describe('no cycleDuration', () => {
        beforeEach(() => {
          cycle.cycleDuration = null;
        });

        it('should set cycleDuration', () => {
          const { cycleDuration } = extendCycle(nodes, cycle);
          expect(cycleDuration).to.equal(214);
        });
      });

      describe('existing cycleDuration', () => {
        describe('matching default value', () => {
          beforeEach(() => {
            cycle.cycleDuration = 365;
          });

          it('should override cycleDuration', () => {
            const { cycleDuration } = extendCycle(nodes, cycle);
            expect(cycleDuration).to.not.equal(cycle.cycleDuration);
          });
        });

        describe('NOT matching default value', () => {
          beforeEach(() => {
            cycle.cycleDuration = 10;
          });

          it('should NOT override cycleDuration', () => {
            const { cycleDuration } = extendCycle(nodes, cycle);
            expect(cycleDuration).to.equal(cycle.cycleDuration);
          });
        });
      });
    });
  });

  describe('extendSite', () => {
    const nodes = [
      {
        type: SchemaType.Organisation,
        id: 'organisation-id-1',
        name: 'LVMH Vineyards'
      },
      {
        type: SchemaType.Organisation,
        id: 'organisation-id-2',
        name: 'LVMH Vineyards'
      }
    ];
    const site: Site = {
      type: SchemaType.Site,
      siteType: SiteSiteType.cropland,
      description: 'Southern aspect with a very long description that will be shortened anyways.',
      region: {
        type: SchemaType.Term,
        name: 'Champagne'
      },
      country: {
        type: SchemaType.Term,
        name: 'France'
      },
      organisation: {
        type: SchemaType.Organisation,
        id: 'organisation-id-1'
      }
    };

    it('should set the name', () => {
      const { name } = extendSite(nodes, site);
      expect(name).to.equal('Cropland - LVMH Vineyards - Champagne, France - Southern aspect with a very lo...');
    });

    describe('with reduced info', () => {
      it('should set the name', () => {
        const { name } = extendSite(nodes, {
          type: SchemaType.Site,
          siteType: SiteSiteType.cropland
        });
        expect(name).to.equal('Cropland');
      });

      it('should set the name with country', () => {
        const { name } = extendSite(nodes, {
          type: SchemaType.Site,
          siteType: SiteSiteType.cropland,
          country: {
            type: SchemaType.Term,
            name: 'France'
          }
        });
        expect(name).to.equal('Cropland - France');
      });
    });
  });

  describe('extendImpactAssessment', () => {
    const defaultSource: Source = {
      type: SchemaType.Source,
      id: 'source'
    };
    const impactAssessment: ImpactAssessment = {
      type: SchemaType.ImpactAssessment,
      product: {
        type: SchemaType.Product,
        term: {
          type: SchemaType.Term,
          name: 'product'
        }
      },
      cycle: {
        type: SchemaType.Cycle,
        id: 'cycle',
        defaultSource
      },
      country: {
        type: SchemaType.Term,
        name: 'country'
      },
      endDate: '2010',
      organic: true,
      irrigated: false
    };

    it('should set name', () => {
      expect(extendImpactAssessment([], impactAssessment).name).to.equal('product - country - 2010');
    });

    it('should set the source', () => {
      expect(extendImpactAssessment([], impactAssessment).source).to.deep.equal(defaultSource);
    });

    describe('with a linked Cycle and only 1 matching product term @id', () => {
      const product = {
        type: SchemaType.Product,
        term: {
          type: SchemaType.Term,
          name: 'product'
        },
        properties: [
          {
            term: {
              type: SchemaType.Property,
              '@id': 'dryMatter'
            },
            value: 50
          }
        ]
      };
      const nodes = [
        {
          type: SchemaType.Cycle,
          id: 'cycle',
          products: [product]
        }
      ];

      it('should update the product', () => {
        expect(extendImpactAssessment(nodes, impactAssessment).product).to.deep.equal(product);
      });
    });
  });

  describe('primaryProduct', () => {
    let products: any[] = [];

    describe('no products', () => {
      it('should return the default value', () => {
        expect(primaryProduct(products, null)).to.equal(null);
      });
    });

    describe('with a single product', () => {
      const product: any = {
        type: SchemaType.Product,
        value: [0]
      };

      beforeEach(() => {
        products = [product];
      });

      it('should return the product', () => {
        expect(primaryProduct(products)).to.deep.equal(product);
      });
    });

    describe('with multiple products', () => {
      beforeEach(() => {
        products = [
          {
            type: SchemaType.Product,
            value: [0]
          },
          {
            type: SchemaType.Product,
            value: [1]
          },
          {
            type: SchemaType.Product,
            value: [2]
          }
        ];
      });

      it('should return the first product', () => {
        expect(primaryProduct(products)).to.deep.equal(products[0]);
      });

      describe('with a primary product', () => {
        beforeEach(() => {
          products[1].primary = true;
        });

        it('should return the product', () => {
          expect(primaryProduct(products)).to.deep.equal(products[1]);
        });
      });

      describe('with economic value share above 50', () => {
        beforeEach(() => {
          products[0].economicValueShare = 20;
          products[1].economicValueShare = 20;
          products[2].economicValueShare = 60;
        });

        it('should return the product', () => {
          expect(primaryProduct(products)).to.deep.equal(products[2]);
        });
      });
    });
  });

  describe('setDefaultValues', () => {
    const source = {
      type: SchemaType.Source,
      id: '1'
    };
    const defaultSource = {
      type: SchemaType.Source,
      id: '2'
    };
    const cycle = {
      type: SchemaType.Cycle,
      id: '1',
      defaultSource,
      emissions: [
        {
          type: SchemaType.Emission
        }
      ],
      products: [
        {
          type: SchemaType.Product,
          term: {
            '@type': SchemaType.Term,
            '@id': 'beefCattle'
          },
          economicValueShare: 10,
          revenue: 100,
          price: 10
        }
      ],
      value: 1
    };
    const impact = {
      type: SchemaType.ImpactAssessment,
      id: '1',
      cycle: {
        type: SchemaType.Cycle,
        id: '1'
      },
      impacts: [
        {
          type: SchemaType.Indicator,
          term: {
            '@type': SchemaType.Emission,
            '@id': 'co2'
          },
          value: null
        }
      ]
    };
    const nodes = [source, cycle, impact];

    it('should set default values', () => {
      const results = setDefaultValues(nodes);

      expect(results).to.deep.equal([
        {
          ...source
        },
        {
          ...cycle,
          name: 'No Product'
        },
        {
          ...impact,
          name: 'No Product',
          source: defaultSource
        }
      ]);
    });
  });
});
