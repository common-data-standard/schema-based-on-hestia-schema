import { writeFileSync } from 'fs';
import { join } from 'path';

import {
  VERSION,
  ensureDir,
  unique,
  readYmlFile,
  listYmlDir,
  IProperty,
  dependencies,
  nestedKeys,
  IValidation,
  validationClass,
  validationEnumName,
  getValidations,
  getSortConfig,
  sortedProperties,
  ISortedProperties,
  propertyEnumName,
  propertyIsEnum,
  propertyType,
  asType
} from './generate-utils';

const defaultUploadLimit = 1000;
const newLineJoin = ',\n  ';
interface IGeneratedClass {
  name: string;
  isNode: boolean;
  searchable: string[];
  sortedProperties: ISortedProperties;
  uploadLimit: number;
  nestedKeys: string[];
  validations: IValidation[];
}

const CLASS_DIR = join(__dirname, '@hestia-earth', 'schema');
const SORT_CONFIG_DIR = join(CLASS_DIR, 'sort-config');

const cleanPropertyName = (name: string) => (name.includes('-') || name.includes(' ') ? `'${name}'` : name);

const variableName = (...parts: string[]) =>
  parts
    .flat()
    .filter(Boolean)
    .map((part, index) =>
      index === 0
        ? `${part.charAt(0).toLowerCase()}${part.substring(1)}`
        : `${part.charAt(0).toUpperCase()}${part.substring(1)}`
    )
    .join('');

const generateProperty =
  (className: string, isClass = true, isJSONLD = false) =>
  ({ doc, const: constValue, ...prop }: IProperty) =>
    `${
      doc
        ? `/**
  * ${doc.trim()}
  */`
        : ''
    }
  ${cleanPropertyName(prop.name)}?: ${propertyType(prop, className, isJSONLD)}${
    isClass && constValue ? ` = ${constValue}` : ''
  };
`;

const JSONContent = ({ className, doc, properties }) => `
/**
 * ${doc.trim()}
 */
export class ${className} extends JSON<SchemaType.${className}> {
  ${properties.map(generateProperty(className)).join('\n  ')}
}`;

const JSONLDContent = ({ className, doc, properties }) => `
/**
 * ${doc.trim()}
 */
export interface I${className}JSONLD extends JSONLD<NodeType.${className}> {
  ${properties
    .filter(({ name }) => name !== 'name')
    .map(generateProperty(className, false, true))
    .join('\n  ')}
}`;

const generateEnums = (className: string, properties: IProperty[]) =>
  properties
    .filter(propertyIsEnum)
    .map(
      property => `
export enum ${propertyEnumName(className, property)} {
  ${property.enum
    .sort()
    .map(val => `${cleanPropertyName(val)} = '${val}'`)
    .join(newLineJoin)}
}`
    )
    .join('\n\n');

const generateValidations = (className: string, validations: IValidation[]) => `
/**
 * Contains all the ${validationEnumName} with override on the ${className}.
 * Note: it does not contain the default ${validationEnumName} on related Blank Nodes.
 */
export const ${variableName(className, validationEnumName)} = {
  ${validations
    .map(({ paths, values }) =>
      `
    ${paths.join(': {')}: [
    ${values.join(newLineJoin)}
  ]${paths.map(_v => '').join(' }')}
    `.trim()
    )
    .join(newLineJoin)},
  all: [
    ${unique(validations.flatMap(({ values }) => values)).join(newLineJoin)}
  ]
};`;

const generateClass = (file: string): IGeneratedClass => {
  console.log(`Processing file: ${file}`);
  const data = readYmlFile(file);
  const properties: IProperty[] = data.properties;
  const importedClasses = dependencies(data, true);
  const validations = data.validation ? getValidations(data.validation) : [];
  const hasJSONLD = data.type === 'Node';
  const content = `// auto-generated content
  import { JSON, ${hasJSONLD ? 'JSONLD, NodeType, ' : ''}SchemaType } from './types';
${unique(importedClasses)
  .map(name => [name, asType(name)])
  .filter(([name, type]) => name !== data.name && type !== data.name)
  .map(([name, type]) => `import { ${name} } from './${type}';`)
  .join('\n')}
${validations.length ? `import { ${validationEnumName} } from './${validationClass}';` : ''}
${generateEnums(data.name, properties)}
${validations.length ? generateValidations(data.name, validations) : ''}
${JSONContent({ className: data.name, doc: data.doc, properties })}
${hasJSONLD ? JSONLDContent({ className: data.name, doc: data.doc, properties }) : ''}`;
  writeFileSync(join(CLASS_DIR, `${data.name}.ts`), content);
  return {
    name: data.name,
    isNode: hasJSONLD,
    searchable: properties.filter(prop => prop.searchable).map(prop => prop.name),
    sortedProperties: sortedProperties(properties),
    uploadLimit: data.uploadLimit || defaultUploadLimit,
    nestedKeys: nestedKeys(data),
    validations
  };
};

const generateTypes = (classes: IGeneratedClass[]) => {
  const content = `// auto-generated content

export enum NodeType {
  ${classes
    .filter(({ isNode }) => isNode)
    .map(({ name }) => `${name} = '${name}'`)
    .join(newLineJoin)}
}

export enum SchemaType {
  ${classes.map(({ name }) => `${name} = '${name}'`).join(newLineJoin)}
}

export const searchableProperties: {
  [type in SchemaType]: string[];
} = {
  ${classes.map(({ name, searchable }) => `${name}: [${searchable.map(v => `'${v}'`).join(', ')}]`).join(newLineJoin)}
};

export enum UploadLimit {
  ${classes
    .filter(({ isNode }) => isNode)
    .map(({ name, uploadLimit }) => `${name} = ${uploadLimit}`)
    .join(newLineJoin)}
}

export const nestedSearchableKeys = [
  ${unique(classes.flatMap(({ nestedKeys }) => nestedKeys))
    .map(v => `'${v}'`)
    .join(', ')}
];

export class JSON<T extends SchemaType> {
  type: T;
  /**
   * Not required, used to generate contextualized unique id.
   */
  id?: string;
}

export interface IContext {
  '@base': string;
  '@vocab': string;
}

export interface JSONLD<T extends NodeType> {
  '@context': string|Array<string|IContext>;
  '@type': T;
  '@id': string;
  name: string;
}
`;
  writeFileSync(join(CLASS_DIR, 'types.ts'), content, 'utf8');

  const sortConfig = getSortConfig(classes);
  writeFileSync(join(SORT_CONFIG_DIR, 'config.json'), JSON.stringify(sortConfig), 'utf8');
};

const generateTermTypes = (classes: IGeneratedClass[]) => {
  const content = `// auto-generated content

import { SchemaType } from './types';

${classes
  .filter(({ validations }) => validations.length)
  .map(({ name }) => `import * as ${name} from './${name}';`)
  .join('\n')}

export const schemaTermTypes = [
  ${classes
    .filter(({ validations }) => validations.length)
    .map(({ name }) => `{ type: SchemaType.${name}, mappings: ${name}.${variableName(name, validationEnumName)} }`)
    .join(newLineJoin)}
];
`;
  writeFileSync(join(CLASS_DIR, 'termTypes.ts'), content, 'utf8');
};

const generateIndex = (classes: string[]) => {
  const content = `// auto-generated content

export const SCHEMA_VERSION = '${VERSION}';
export * from './types';
export * from './termTypes';
export * from './utils/sort';
export * from './utils/types';

${classes.map(name => `export * from './${name}';`).join('\n')}
`;
  writeFileSync(join(CLASS_DIR, 'index.ts'), content);
};

const run = () => {
  ensureDir(CLASS_DIR);
  ensureDir(SORT_CONFIG_DIR);
  const files = listYmlDir();
  const classes = files.map(generateClass);
  generateTypes(classes);
  generateTermTypes(classes);
  generateIndex(classes.map(({ name }) => name));
};

run();
