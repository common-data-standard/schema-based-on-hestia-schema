#!/bin/sh

# exit when any command fails
set -e

PACKAGE_VERSION=$(cat ../package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

zip -r layer.zip ./nodejs/*

aws lambda publish-layer-version \
    --region eu-west-2 \
    --layer-name "hestia-schema" \
    --description "Hestia Schema version ${PACKAGE_VERSION} for Node" \
    --zip-file "fileb://layer.zip" \
    --compatible-runtimes nodejs14.x nodejs16.x nodejs18.x nodejs20.x
