#!/bin/sh

# exit when any command fails
set -e

npm install --no-save --prefix ./nodejs \
  @hestia-earth/schema@latest \
  @hestia-earth/json-schema@latest \
  @hestia-earth/schema-convert@latest \
  @hestia-earth/schema-validation@latest \
