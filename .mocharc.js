'use strict'

module.exports = {
  require: [
    'ts-node/register',
    'tsconfig-paths/register',
    'source-map-support/register'
  ],
  'full-trace': true,
  timeout: 30000
}
