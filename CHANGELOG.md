# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [26.6.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v26.5.0...v26.6.0) (2024-02-09)


### Features

* **cycle:** allow `producedInCycle` to determine uniquess of `inputs` ([89909a5](https://gitlab.com/hestia-earth/hestia-schema/commit/89909a5129c0d943937b33d7a8e14281907804d3))
* **cycle:** allow `producedInCycle` to determine uniquess of `inputs` ([e0728b8](https://gitlab.com/hestia-earth/hestia-schema/commit/e0728b84a1c154dd5f017a53a1ad816f589ce431))


### Bug Fixes

* **serverless:** fix convert include all headers ([d17689a](https://gitlab.com/hestia-earth/hestia-schema/commit/d17689a5bb8f5f5d970d85852a1e53ec84fa7974))

## [26.5.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v26.4.0...v26.5.0) (2024-02-06)


### Features

* **cycle:** allow `animals` to determine uniqueness for `emissions` ([476620b](https://gitlab.com/hestia-earth/hestia-schema/commit/476620bd03ca5e6c38ec608dda00950951db6a74))

## [26.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v26.3.0...v26.4.0) (2024-02-06)


### Features

* **emission:** add `animals` field to link background emissions with animals ([23a794e](https://gitlab.com/hestia-earth/hestia-schema/commit/23a794ef9c8fd7a9e863aa1e65d9bbf9bab60092))
* **input:** add `producedInCycle` field ([efc8ddd](https://gitlab.com/hestia-earth/hestia-schema/commit/efc8ddd931e92a08f7460c24c1741f2455c21e10)), closes [#401](https://gitlab.com/hestia-earth/hestia-schema/issues/401)
* **transformation:** prevent using `producedInCycle` on `inputs` ([6a5a4ec](https://gitlab.com/hestia-earth/hestia-schema/commit/6a5a4ecd5cf426054674a2f9210507a702566d7a))

## [26.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v26.2.0...v26.3.0) (2024-02-06)


### Features

* **cycle:** prevent adding `fromCycle` to `inputs` ([feba7ff](https://gitlab.com/hestia-earth/hestia-schema/commit/feba7ff21bf89c8383c81f09dc04517f24cd4373))
* **emission:** allow `otherOrganicChemical` as `termType` for `input` ([e36a9c3](https://gitlab.com/hestia-earth/hestia-schema/commit/e36a9c3c7ae585b746eb57a33629919a5b6e2dcf))

## [26.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v26.1.0...v26.2.0) (2024-01-22)


### Features

* **bibliography:** validate format of `documentDOI` ([f9ad9a0](https://gitlab.com/hestia-earth/hestia-schema/commit/f9ad9a03523a319d3f5ca15f3428cb335ac78586))
* **practice:** add internal `aggregated` and `aggregatedVersion` fields ([bd3b1e3](https://gitlab.com/hestia-earth/hestia-schema/commit/bd3b1e3de64ba59b351717f3f3893bc8cf5963c4))

## [26.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v26.0.0...v26.1.0) (2024-01-16)


### Features

* **product:** add `breeding` as enum for `fate` ([840af13](https://gitlab.com/hestia-earth/hestia-schema/commit/840af135b18d962ae7104acc00917dcb5f12229f))

## [26.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v25.1.0...v26.0.0) (2024-01-03)


### ⚠ BREAKING CHANGES

* **cycle:** Removes `properties` in uniqueness of Input and Product on Cycle.
Instead, multiple properties with different `value` and `share` can be used.

### Features

* **property:** add `share` field ([e290161](https://gitlab.com/hestia-earth/hestia-schema/commit/e290161b60370fff644c8955adb8ccaf13190282))
* **schema-convert:** update ImpactAssessment product using unique matching Cycle product ([8e74a0a](https://gitlab.com/hestia-earth/hestia-schema/commit/8e74a0adb3b615d8126a88f5e92b3151d4795c2a))


### Bug Fixes

* **cycle:** remove `properties` to determine uniqueness of `inputs` and `products` ([5e1ea8f](https://gitlab.com/hestia-earth/hestia-schema/commit/5e1ea8fa77923390032736b922f2a030c718751d))

## [25.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v25.0.1...v25.1.0) (2023-12-20)


### Features

* **schema-convert:** handle `-` as `null` in arrays ([37f8356](https://gitlab.com/hestia-earth/hestia-schema/commit/37f8356856c2bec2cd0b4a4c511af0e13cf450e6))
* **schema-convert:** return multilpe conversion errors ([af44b2f](https://gitlab.com/hestia-earth/hestia-schema/commit/af44b2ff542ac999d25e7bdfb07baf93b005efab))

### [25.0.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v25.0.0...v25.0.1) (2023-12-18)


### Bug Fixes

* **term:** fix `canonicalSmiles` pattern ([925699b](https://gitlab.com/hestia-earth/hestia-schema/commit/925699bfbb734047ae3668c04271daa9df1609c3))

## [25.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v24.3.0...v25.0.0) (2023-12-15)


### ⚠ BREAKING CHANGES

* **management:** Replace `areaPercent` field by `value`.
* **completeness:** Rename `products` completeness field `product`.
* **completeness:** Rename `soilAmendments` completeness field `soilAmendment`.
* **completeness:** Rename `operations` completeness field `operation`.
* **completeness:** Add required `ingredient` completeness field.
* **completeness:** Add required `grazedForage` completeness field.
* **completeness:** Rename `liveAnimals` completeness field `liveAnimalInput`.
* **completeness:** Add required `animalHerd` completeness field.
* **completeness:** Rename `pesticidesAntibiotics` completeness field `pesticideVeterinaryDrug`.
* **term:** rename `veterinaryDrugs` termType `veterinaryDrug`
* **completeness:** Add required `otherChemical` as completeness field.
* **completeness:** Rename `other` completeness field `seed`.
* **term:** rename `other` termType `seed`

### Features

* **completeness:** add `grazedForage` field and update definition of `animalFeed` ([47d5adb](https://gitlab.com/hestia-earth/hestia-schema/commit/47d5adba5864ea256391367fa644d3e9ee262874)), closes [#339](https://gitlab.com/hestia-earth/hestia-schema/issues/339)
* **completeness:** add `ingredient` field ([7007fde](https://gitlab.com/hestia-earth/hestia-schema/commit/7007fdeb22235f0e3e63297afc45d288cd0ef57e)), closes [#303](https://gitlab.com/hestia-earth/hestia-schema/issues/303)
* **completeness:** add `otherChemical` field ([24c6a6d](https://gitlab.com/hestia-earth/hestia-schema/commit/24c6a6d0f0997afff292d5f4c60d1360b73380eb)), closes [#314](https://gitlab.com/hestia-earth/hestia-schema/issues/314) [#356](https://gitlab.com/hestia-earth/hestia-schema/issues/356)
* **completeness:** rename `liveAnimals` field `liveAnimalInput` and add `animalHerd` ([3c95d5c](https://gitlab.com/hestia-earth/hestia-schema/commit/3c95d5c30da46ddd27b3a16de1a9d4183c9f346e)), closes [#341](https://gitlab.com/hestia-earth/hestia-schema/issues/341)
* **completeness:** rename `operations` field `operation` ([b33b674](https://gitlab.com/hestia-earth/hestia-schema/commit/b33b6743920aea9293f30bb9c8daa1e9866bf534))
* **completeness:** rename `other` field `seed` ([1d4fa48](https://gitlab.com/hestia-earth/hestia-schema/commit/1d4fa48e4de21e323be15747a280e5e5d8fe79f0)), closes [#314](https://gitlab.com/hestia-earth/hestia-schema/issues/314)
* **completeness:** rename `pesticidesAntibiotics` field `pesticideVeterinaryDrug` ([1a4b0f8](https://gitlab.com/hestia-earth/hestia-schema/commit/1a4b0f8287cd7de388ab17211e0d7873bd754294)), closes [#348](https://gitlab.com/hestia-earth/hestia-schema/issues/348)
* **completeness:** rename `products` field `product` ([48bffe7](https://gitlab.com/hestia-earth/hestia-schema/commit/48bffe7ddab19f4a0c7c3bc41bcfbb529db46117)), closes [#369](https://gitlab.com/hestia-earth/hestia-schema/issues/369)
* **completeness:** rename `soilAmendments` field `soilAmendment` ([efe2fde](https://gitlab.com/hestia-earth/hestia-schema/commit/efe2fde5afd9064fb97c4ca7dbcb815dff939478))
* **cycle:** allow `properties` to determine unicity on `inputs` ([2f6f3f6](https://gitlab.com/hestia-earth/hestia-schema/commit/2f6f3f6256a0c967959fb56448805c7f23e22b2c)), closes [#378](https://gitlab.com/hestia-earth/hestia-schema/issues/378)
* **management:** add `properties` field ([2b2ee6f](https://gitlab.com/hestia-earth/hestia-schema/commit/2b2ee6fc8aace5165c4257b0f687b9ac83cb70de)), closes [#376](https://gitlab.com/hestia-earth/hestia-schema/issues/376)
* **management:** change `areaPercent` field to `value` field ([9ee37e2](https://gitlab.com/hestia-earth/hestia-schema/commit/9ee37e233563300081e57ab5d07a047a782b6cfe)), closes [#375](https://gitlab.com/hestia-earth/hestia-schema/issues/375)
* **schema-validation:** validate geojson coordinates boundaries ([30fbc1b](https://gitlab.com/hestia-earth/hestia-schema/commit/30fbc1b476cfbcf3e2b368cef646f5d732f842be))
* **term:** rename `other` termType `seed` ([fae44d1](https://gitlab.com/hestia-earth/hestia-schema/commit/fae44d1edd1edf1616f2afd7119f857261d395c0)), closes [#365](https://gitlab.com/hestia-earth/hestia-schema/issues/365) [#314](https://gitlab.com/hestia-earth/hestia-schema/issues/314)
* **term:** rename `veterinaryDrugs` termType `veterinaryDrug` ([0816070](https://gitlab.com/hestia-earth/hestia-schema/commit/081607021fdfa77ade8b0b2bf6c4a4dbc5292114))

## [24.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v24.2.0...v24.3.0) (2023-11-27)


### Features

* **cycle:** allow `properties.term@id` and `properties.value` to determine uniqueness for inputs ([d8e7b61](https://gitlab.com/hestia-earth/hestia-schema/commit/d8e7b61e51b298750391913d7351afb105d5ae3c)), closes [#361](https://gitlab.com/hestia-earth/hestia-schema/issues/361)
* **schema-convert:** remove internal fields by default when converting to CSV ([f01d30b](https://gitlab.com/hestia-earth/hestia-schema/commit/f01d30bb27b58b7565a9e8e4cbd7aa57cfa9f51a))
* **schema-convert:** remove term extra columns by default when converting to CSV ([ff0dd3c](https://gitlab.com/hestia-earth/hestia-schema/commit/ff0dd3ce61af6452fd70568454a94aea27ae9f8a))
* **site:** allow multiple fields to determine uniqueness for `Infrastructure` ([9becefb](https://gitlab.com/hestia-earth/hestia-schema/commit/9becefb3c64d75fab9d8dbc4ddf36dc8d6b64b64)), closes [#362](https://gitlab.com/hestia-earth/hestia-schema/issues/362)


### Bug Fixes

* fix incorrect keys in uniqueArrayItem on practices ([aae2e03](https://gitlab.com/hestia-earth/hestia-schema/commit/aae2e03ca14619c34db8e8db938a7ea18609266e))

## [24.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v24.1.0...v24.2.0) (2023-11-22)


### Features

* **term:** add new termTypes ([8c21136](https://gitlab.com/hestia-earth/hestia-schema/commit/8c21136c67e1779b5b6c73b0adae05e0729f3966))

## [24.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v24.0.0...v24.1.0) (2023-11-15)


### Features

* **term:** add `landCover` as `termType` ([a981a33](https://gitlab.com/hestia-earth/hestia-schema/commit/a981a33195717892c4eb5334743cfa1e4a80a200))

## [24.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v23.7.0...v24.0.0) (2023-11-14)


### ⚠ BREAKING CHANGES

* **term:** Renames `antibiotic` `termType` to `veterinaryDrugs`.
* **measurement:** Deletes `modelled using other physical measurements` and
adds a more generic term, `modelled using other measurements`.

### Features

* **measurement:** update `methodClassification` options ([dee0c0a](https://gitlab.com/hestia-earth/hestia-schema/commit/dee0c0ac8b1e8b4d5d805aa47c25d3520fb51062))
* **product:** allow `processingAid` as `termType` ([8312d38](https://gitlab.com/hestia-earth/hestia-schema/commit/8312d38649a63fab248159c7cb2c329c9aea2a44))
* **schema-convert:** add suggestions property not found on `Term` ([affe6da](https://gitlab.com/hestia-earth/hestia-schema/commit/affe6dad73ef0b18bf72dda03065357f8a7a0289))
* **term:** rename `antibiotic` `termType` to `veterinaryDrugs` ([e740307](https://gitlab.com/hestia-earth/hestia-schema/commit/e740307f60960e53d09149ecb173f2c87221ca9a)), closes [#398](https://gitlab.com/hestia-earth/hestia-schema/issues/398)

## [23.7.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v23.6.0...v23.7.0) (2023-11-02)


### Features

* allow `date`, `startDate` and `endDate` to determine uniqueness for `properties` ([41edaf0](https://gitlab.com/hestia-earth/hestia-schema/commit/41edaf0a7df1cdfe97400ccf2412dd4ad43fdd0e)), closes [#315](https://gitlab.com/hestia-earth/hestia-schema/issues/315)
* **impact-assessment:** allow `inputs` to determine `uniqueness` ([991f36e](https://gitlab.com/hestia-earth/hestia-schema/commit/991f36ee9ee84872e6cbc365a55339b9429a6ab3))
* **property:** add `date`, `startDate`, and `endDate` fields ([d190c1e](https://gitlab.com/hestia-earth/hestia-schema/commit/d190c1e86a4860c8650a658d184bce4cb68d4876)), closes [#315](https://gitlab.com/hestia-earth/hestia-schema/issues/315)
* **schema-validation:** add duplicated array item indexes ([680d397](https://gitlab.com/hestia-earth/hestia-schema/commit/680d3974cac9a6d52488a0225cbaadf430cf7369))

## [23.6.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v23.5.0...v23.6.0) (2023-10-24)


### Features

* **measurement:** add tier 1-3 model options to `methodClassification` ([e13c1fd](https://gitlab.com/hestia-earth/hestia-schema/commit/e13c1fd59c8da898b8da8f4f9352084a34885333)), closes [#344](https://gitlab.com/hestia-earth/hestia-schema/issues/344)
* **schema-convert:** set `impactAssessment.source` = `cycle.defaultSource` ([7c9257e](https://gitlab.com/hestia-earth/hestia-schema/commit/7c9257ecb24c74966ad2c3c1f74fc65568c85c22))

## [23.5.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v23.4.1...v23.5.0) (2023-10-13)


### Features

* **product:** allow `inorganicFertiliser` as `termType` ([4e89352](https://gitlab.com/hestia-earth/hestia-schema/commit/4e89352cdb2253e9031a828aad2922d4c924702a))
* **schema-validation:** validate GeoJSON format ([22adf76](https://gitlab.com/hestia-earth/hestia-schema/commit/22adf7670155d159163bd4559abbb97645d298e8))
* **term:** make `description` searchable ([3d34d87](https://gitlab.com/hestia-earth/hestia-schema/commit/3d34d876837fcbb850148c2bb3c74626fe3ce580))

### [23.4.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v23.4.0...v23.4.1) (2023-10-03)


### Bug Fixes

* **schema-convert:** allow `Term` as NodeType ([de11189](https://gitlab.com/hestia-earth/hestia-schema/commit/de11189d3e4aa7ad6af7149b46e5619ceaab700d))

## [23.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v23.3.0...v23.4.0) (2023-10-03)


### Features

* **product:** add `bedding` as `fate` ([cf5061b](https://gitlab.com/hestia-earth/hestia-schema/commit/cf5061b7855904438f0f5feb3c2c7835a466ba23))
* **schema-convert:** restrict schema type to Node types only ([79ec120](https://gitlab.com/hestia-earth/hestia-schema/commit/79ec1200d82049afbc116fc8361a2bdda2b5c483))


### Bug Fixes

* **cycle:** error in `uniqueArrayItem` field for `practices` ([9e2b972](https://gitlab.com/hestia-earth/hestia-schema/commit/9e2b9726e841df537ebc26b6a9e668375598abe8))

## [23.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v23.2.0...v23.3.0) (2023-09-25)


### Features

* **schema-validation:** improve error message add schema ([1b37170](https://gitlab.com/hestia-earth/hestia-schema/commit/1b37170aced284d2a0b3b231c1b92ab6e6edafe3))

## [23.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v23.1.0...v23.2.0) (2023-09-17)


### Features

* **term:** rename `ecoinventReferenceProduct` `ecoinventReferenceProductId` ([56b1f5b](https://gitlab.com/hestia-earth/hestia-schema/commit/56b1f5b436790a9049eb387d4054ffeb327e57de))


### Bug Fixes

* **term:** remove `ecoinventName` ([55105ff](https://gitlab.com/hestia-earth/hestia-schema/commit/55105ff0c4d7d50b33b8e8d7254106ff3478835e))

## [23.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v23.0.0...v23.1.0) (2023-09-15)


### Features

* **cycle:** allow `isAnimalFeed` to determine `uniqueness` on `inputs` ([27304f6](https://gitlab.com/hestia-earth/hestia-schema/commit/27304f69ef58bd9f9ac57bad9802c9900f59085c)), closes [#282](https://gitlab.com/hestia-earth/hestia-schema/issues/282)
* **organisation:** add `boundaryArea` field ([92ba8a4](https://gitlab.com/hestia-earth/hestia-schema/commit/92ba8a458c6b9b329e6479ad9ca58c2d521dd05e))
* **site:** add `boundaryArea` field ([d608e53](https://gitlab.com/hestia-earth/hestia-schema/commit/d608e53e760fb8f0b9f43af0a707da2aefc821c2))

## [23.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v22.4.0...v23.0.0) (2023-09-11)


### ⚠ BREAKING CHANGES

* **input:** Adds new field `fromCycle` to Input, which is `required` for Transformations.

### Features

* **input:** add new field `fromCycle` ([5b9a45b](https://gitlab.com/hestia-earth/hestia-schema/commit/5b9a45bf788684e93da768cb37af0bd6c0071dda))


### Bug Fixes

* **schema-convert:** allow using both `id` and `[@id](https://gitlab.com/id)` with one of them empty ([9570255](https://gitlab.com/hestia-earth/hestia-schema/commit/9570255e3bc66c5f380903a786672974e7953a88))

## [22.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v22.3.0...v22.4.0) (2023-09-04)


### Features

* **product:** add `soilAmendment` as allowed `termType` ([66c3d0a](https://gitlab.com/hestia-earth/hestia-schema/commit/66c3d0a6e8f803ceec777ec34f400ab97bd03295)), closes [#326](https://gitlab.com/hestia-earth/hestia-schema/issues/326)
* **schema-convert:** prevent using both `id` and `[@id](https://gitlab.com/id)` fields at the same time ([a96b09e](https://gitlab.com/hestia-earth/hestia-schema/commit/a96b09e36626e4aadab9af91802d9c5c5ce600ef))

## [22.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v22.2.1...v22.3.0) (2023-08-08)


### Features

* **json-schema:** set node documentation in `description` ([f94be24](https://gitlab.com/hestia-earth/hestia-schema/commit/f94be24f73b0f8b4e5de3ce021de8837cde2318d))

### [22.2.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v22.2.0...v22.2.1) (2023-08-02)


### Bug Fixes

* **site:** set `defaultMethodClassificationDescription` not required by default ([7a47e42](https://gitlab.com/hestia-earth/hestia-schema/commit/7a47e4260622f1be67e7be91739660d31d011228))

## [22.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v22.1.0...v22.2.0) (2023-08-02)


### Features

* add `defaultMethodClassification` to `Site` and remove from `Infrastructure` ([9b0949d](https://gitlab.com/hestia-earth/hestia-schema/commit/9b0949decb9197f0c93a6844a2bd3c996ec917ac)), closes [#312](https://gitlab.com/hestia-earth/hestia-schema/issues/312)
* add new `Management` blank node ([05577d5](https://gitlab.com/hestia-earth/hestia-schema/commit/05577d59ab15a3dab46d1b19db29c905668a75e0)), closes [#102](https://gitlab.com/hestia-earth/hestia-schema/issues/102) [#312](https://gitlab.com/hestia-earth/hestia-schema/issues/312)
* **cycle:** add new startDateDefinition options ([35e94f7](https://gitlab.com/hestia-earth/hestia-schema/commit/35e94f7e33110a9ad92455d8dff2abf24584a788)), closes [#289](https://gitlab.com/hestia-earth/hestia-schema/issues/289)
* **schema:** add function `schemaFromTermType` ([1cd299a](https://gitlab.com/hestia-earth/hestia-schema/commit/1cd299a25556d8c5a1ccd55bc95edf786e8ce64b))

## [22.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v22.0.0...v22.1.0) (2023-07-24)


### Features

* **completeness:** mark all fields as `searchable` ([301cf5e](https://gitlab.com/hestia-earth/hestia-schema/commit/301cf5e4c40ded067eafeb647fa6e80f0f9c7417))
* **cycle:** add `aggregatedQualityScoreMax` ([d304e00](https://gitlab.com/hestia-earth/hestia-schema/commit/d304e00607db362b8c7f79bc6e5aead4c3cf44e2))
* **impact-assessment:** add `aggregatedQualityScoreMax` ([65bb6a9](https://gitlab.com/hestia-earth/hestia-schema/commit/65bb6a94eb99a3eac7df5952a19165b904a803cf))

## [22.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v21.4.0...v22.0.0) (2023-07-11)


### ⚠ BREAKING CHANGES

* **infrastructure:** New required fields for `Infrastructure`:
`defaultMethodClassification` and `defaultMethodClassificationDescription`.

### Features

* **infrastructure:** add `defaultMethodClassification` and `defaultMethodClassificationDescription` ([561f441](https://gitlab.com/hestia-earth/hestia-schema/commit/561f4417a41c9c6a60101e3af22f02e43ce89cde)), closes [#302](https://gitlab.com/hestia-earth/hestia-schema/issues/302)
* **input:** add `processingAid` as possible `termType` ([ae7350b](https://gitlab.com/hestia-earth/hestia-schema/commit/ae7350b2d7c78f665716bf457450428d49518740)), closes [#305](https://gitlab.com/hestia-earth/hestia-schema/issues/305)
* **term:** add the new `termType` `processingAid` ([a889987](https://gitlab.com/hestia-earth/hestia-schema/commit/a889987bb5d618e2b67283b61fcc8c2a88a2f6e5)), closes [#305](https://gitlab.com/hestia-earth/hestia-schema/issues/305)

## [21.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v21.3.0...v21.4.0) (2023-06-19)


### Features

* **schema-convert:** detect invalid array of objects ([fe6f889](https://gitlab.com/hestia-earth/hestia-schema/commit/fe6f8894958d5bc36c13ae806296962f05343207))


### Bug Fixes

* **cycle:** remove restriction on `liveAnimal` requires `excreta` product ([581a8d1](https://gitlab.com/hestia-earth/hestia-schema/commit/581a8d10a045c241b3c2a70e8d8f6d4d5f86dac3))
* **schema-convert:** restrict length of `treatment` in cycle `name` ([6f90005](https://gitlab.com/hestia-earth/hestia-schema/commit/6f90005406eb881b4985ec1c4ffc93bd5e7d60fe)), closes [#290](https://gitlab.com/hestia-earth/hestia-schema/issues/290)

## [21.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v21.2.0...v21.3.0) (2023-06-07)


### Features

* add time as `enum` for `statsDefinition` and improve docs ([669afc5](https://gitlab.com/hestia-earth/hestia-schema/commit/669afc551683f18ea77ed7270c8af5b2548bd8ce))
* **animal:** use same fields as Cycle to determine uniqueness for `inputs` and `practices` ([d6048eb](https://gitlab.com/hestia-earth/hestia-schema/commit/d6048ebfc63a8ac5ef9ce449032a506897a7993f)), closes [#286](https://gitlab.com/hestia-earth/hestia-schema/issues/286)
* **schema-convert:** export additional default values functions ([7462ad6](https://gitlab.com/hestia-earth/hestia-schema/commit/7462ad60ed12233b2730e35ddc8a7aa9adeb2e21))

## [21.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v21.1.0...v21.2.0) (2023-06-02)


### Features

* **term:** add `waste` `termType` ([f01eaa8](https://gitlab.com/hestia-earth/hestia-schema/commit/f01eaa8424762eb838db311a2b00fb9f48a6eb0a))
* **term:** add `wasteManagement` `termType` ([8e2f6d0](https://gitlab.com/hestia-earth/hestia-schema/commit/8e2f6d0c98305d1e05ac68ee949624650b11bc06))

## [21.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v21.0.0...v21.1.0) (2023-06-01)


### Features

* **product:** add `fuel` as possible `termType` ([c8ebc0e](https://gitlab.com/hestia-earth/hestia-schema/commit/c8ebc0ef7f8f1f18e7f6f0b9972e0b1e7a5cfca4))


### Bug Fixes

* **impact-assessment:** remove `termType` restrictions on `product` ([dd5908b](https://gitlab.com/hestia-earth/hestia-schema/commit/dd5908b872aeb5c4a327d6cca5e6707b03822d16))
* **impact-assessment:** remove unused `productValue` ([f28b87b](https://gitlab.com/hestia-earth/hestia-schema/commit/f28b87bbc0a1748a35e870f02d46b2a5d1cefd72))

## [21.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v20.2.0...v21.0.0) (2023-05-31)


### ⚠ BREAKING CHANGES

* Adds a new `required` field `transformationId` to
identify Transformations uniquely within a Cycle and replaces
`previousTransformationTerm` with `previousTransformationId`
which must be used to link transformations.
* **impact-assessment:** `ImpactAssessment.product` now references a `Product`.
* **product:** Renames `fate` `enum` from `export` to
`sold to export market` and `domestic market` to
`sold to domestic market`.
* **product:** Renames `enum` for `fate` from `self-consumption`
to `home consumption`.

### Features

* add `fertiliserBrandName` `termType` ([e7d632f](https://gitlab.com/hestia-earth/hestia-schema/commit/e7d632f57359e43a6c63a717fe420c85dba78f24))
* add `transformationId` and `previousTransformationId` fields ([fa84377](https://gitlab.com/hestia-earth/hestia-schema/commit/fa843778b34542271b4e386b5c78dcfeae2ec040)), closes [#281](https://gitlab.com/hestia-earth/hestia-schema/issues/281)
* **cycle:** add `expert opinion` as `enum` to `defaultMethodClassification` ([4cf126d](https://gitlab.com/hestia-earth/hestia-schema/commit/4cf126dac9d2fb5327e066d75fe43aa3909b990c))
* **impact-assessment:** add `biophysical` allocation as `allocationMethod` option ([f92f513](https://gitlab.com/hestia-earth/hestia-schema/commit/f92f5138c0dcbf450ce017a20dd7d8a8c7f2a483))
* **organisation:** add `infrastructure` field ([f0a5afe](https://gitlab.com/hestia-earth/hestia-schema/commit/f0a5afe462c5bd932c878d8292194dd5d4368c84))
* **product:** add `sold` as `fate` `enum` and rename export/domestic ([8ba46ca](https://gitlab.com/hestia-earth/hestia-schema/commit/8ba46ca95992cde563e723b8efb0695d1091dae5)), closes [#277](https://gitlab.com/hestia-earth/hestia-schema/issues/277)
* **transformation:** match `uniqueArrayItem` fields to Cycle ([e43f687](https://gitlab.com/hestia-earth/hestia-schema/commit/e43f687accf94013a6b321f531babcbb6ea020b5))
* **transformation:** validate all `inputs` have a `value` or `transformedShare` is set ([181ece7](https://gitlab.com/hestia-earth/hestia-schema/commit/181ece723eb26a89ae2b9692eba6574bf9e9cae5))


### Bug Fixes

* **impact-assessment:** set `product` as a `Product` ([df8e7a0](https://gitlab.com/hestia-earth/hestia-schema/commit/df8e7a0a5963c33916729962b344c59e9b8c9521))
* **product:** rename `enum` for `fate` from `self-consumption` to `home consumption` ([da169a9](https://gitlab.com/hestia-earth/hestia-schema/commit/da169a9df22f447885fd41f5acc136595dd2abac))

## [20.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v20.1.1...v20.2.0) (2023-05-08)


### Features

* **schema-convert:** handle empty GeoJSON value ([f2f36a0](https://gitlab.com/hestia-earth/hestia-schema/commit/f2f36a03c0f34fe40b2f25537ad797e6f0372862))
* **term:** add `unitsDescription` field ([0665056](https://gitlab.com/hestia-earth/hestia-schema/commit/0665056712de4925eb4f7df777496f336c0b4a2e))


### Bug Fixes

* **term:** allow `%` in `canonicalSmiles` ([55d935d](https://gitlab.com/hestia-earth/hestia-schema/commit/55d935d65d0d87c57fef7b4121d71037e8bc89ff))

### [20.1.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v20.1.0...v20.1.1) (2023-05-04)


### Bug Fixes

* **infrastructure:** remove unused `createdAt` and `updatedAt` ([1bd34a6](https://gitlab.com/hestia-earth/hestia-schema/commit/1bd34a6cfd0de0e12f5a0bc0f1b2f8ae70e57eaa))
* **term:** allow `.` char in `canonicalSmiles` ([941abc4](https://gitlab.com/hestia-earth/hestia-schema/commit/941abc4eb9039b6954ed937824aaf78217f349bc))

## [20.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v20.0.0...v20.1.0) (2023-05-03)


### Features

* make `createdAt` and `updatedAt` searchable ([350006f](https://gitlab.com/hestia-earth/hestia-schema/commit/350006f10f4b77092e240b8c017db6775fd6370e))


### Bug Fixes

* **term:** fix incorrect pattern for `canonicalSmiles` ([6e38767](https://gitlab.com/hestia-earth/hestia-schema/commit/6e38767551847b4bee6991e4ecd2f832f117092d))

## [20.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v19.0.0...v20.0.0) (2023-05-03)


### ⚠ BREAKING CHANGES

* **source:** Replaces `uploadDate` with `updatedAt` on `Source`.
* **source:** Renames `license` to `originalLicense`
* **source:** Renames `license` `other license` to `other public license`
* **source:** Renames `license` `no license` to `no public license`

### Features

* **actor:** add `createdAt` and `updatedAt` fields ([26c7be8](https://gitlab.com/hestia-earth/hestia-schema/commit/26c7be8e771981f6ea6fc206215e47dfe830e6ee))
* **cycle:** add `createdAt` and `updatedAt` fields ([37884e9](https://gitlab.com/hestia-earth/hestia-schema/commit/37884e945229924b7492300f17149ff6035d0f71))
* **cycle:** allow `site` to determine `uniqueness` on `transformation` ([6fcea9e](https://gitlab.com/hestia-earth/hestia-schema/commit/6fcea9ea1e17f2a2136d43fe3ed5f7c8f352a05f))
* **cycle:** remove `previousTransformationTerm` validator ([53733b8](https://gitlab.com/hestia-earth/hestia-schema/commit/53733b8738bb95e8b70807200dc0b96e7debbc23)), closes [#272](https://gitlab.com/hestia-earth/hestia-schema/issues/272)
* **impact-assessment:** add `createdAt` and `updatedAt` fields ([24bac89](https://gitlab.com/hestia-earth/hestia-schema/commit/24bac89dc419337b6fe2e1ff324d5a510b432ecc))
* **infrastructure:** add `createdAt` and `updatedAt` fields ([89f32a8](https://gitlab.com/hestia-earth/hestia-schema/commit/89f32a81ec76fded9b6a022824541fd5b12c1fb9))
* **organisation:** add `createdAt` and `updatedAt` fields ([3ec022a](https://gitlab.com/hestia-earth/hestia-schema/commit/3ec022a18dc8d1b7ecbd20b34348ce447e56c916))
* **site:** add `createdAt` and `updatedAt` fields ([016052d](https://gitlab.com/hestia-earth/hestia-schema/commit/016052daa7006d82ba71cfbfccffc9f2a63be614))
* **source:** add `createdAt` and `updatedAt` fields, removes `uploadDate` ([b8d8ba2](https://gitlab.com/hestia-earth/hestia-schema/commit/b8d8ba258c6e8018faa7b9868365463033768c11))
* **source:** rename `license` to `originalLicense` ([7400a40](https://gitlab.com/hestia-earth/hestia-schema/commit/7400a40cda4ef5e592c5dcd8f00bcc6d92e9c62e)), closes [#271](https://gitlab.com/hestia-earth/hestia-schema/issues/271)
* **source:** rename `license` to `other public license` and `no public license` ([6569c56](https://gitlab.com/hestia-earth/hestia-schema/commit/6569c56686f20175d7144a3a7196615fe519d09d))
* **term:** add `createdAt` and `updatedAt` fields ([738b712](https://gitlab.com/hestia-earth/hestia-schema/commit/738b7122687bf2b329186335ddeb5c517d8f0f68))
* **term:** add `smiles` field ([67b4252](https://gitlab.com/hestia-earth/hestia-schema/commit/67b4252f7b45a4f5bacea167073075bd97149c59))
* **transformation:** add `site` field ([9cb5a30](https://gitlab.com/hestia-earth/hestia-schema/commit/9cb5a302940e2d2b89b023598e4d8f78fb5aa8d0))

## [19.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v18.0.0...v19.0.0) (2023-04-18)


### ⚠ BREAKING CHANGES

* **source:** Renames `license` `enum` `CC-0 (public domain)` to `CC0`

### Features

* allow `key.[@id](https://gitlab.com/id)` for uniqueness of list of `Property` ([e8c88ed](https://gitlab.com/hestia-earth/hestia-schema/commit/e8c88ed3e2c0025b6cdc6eb59d89db29dd0e943e))
* **source:** add `GNU-FDL` as `enum` to `license` field ([2d2217a](https://gitlab.com/hestia-earth/hestia-schema/commit/2d2217a250eb69d362a5b277638bbce222d73197))
* **source:** split `license` `enum` `CC-0 (public domain)` into `CC0` and `public domain` ([e1450fb](https://gitlab.com/hestia-earth/hestia-schema/commit/e1450fb5ae13a4ba8ae3bb553d570c81acd8b0a2))


## [18.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v18.0.1...v18.1.0) (2023-04-05)


### Features

* **cycle:** allow `impactAssessment` for uniqueness of `Input` ([41edb1f](https://gitlab.com/hestia-earth/hestia-schema/commit/41edb1fbbc063aa782ae66507ec4f8169d79c223))


### Bug Fixes

* **site:** fix uniqueness fields for `measurements` ([5824259](https://gitlab.com/hestia-earth/hestia-schema/commit/58242597212da8fc23c12c74fb5f8257d65b6785))

### [18.0.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v18.0.0...v18.0.1) (2023-03-24)


### Bug Fixes

* **input:** set `impactAssessmentIsProxy` as `boolean` ([6257965](https://gitlab.com/hestia-earth/hestia-schema/commit/6257965b27966a1ca5e390aca958811f33d9f922))

## [18.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v17.0.0...v18.0.0) (2023-03-24)


### ⚠ BREAKING CHANGES

* **source:** Add new required field `license` to state the copyright license for public sources.
* Rename `methodModel` to `model` on `Input`, `Practice`, and `Product`.
* **cycle:** Rename `defaultMethodClassification` from `unsourced or assumption` to `unsourced assumption`.
* **input:** Rename `input.fate` to `input.isAnimalFeed` and set as boolean.

### Features

* **animal:** change `value` from `required` to `recommended` ([baf1925](https://gitlab.com/hestia-earth/hestia-schema/commit/baf1925031bf8ec9d2a168f16b410b27870bb467)), closes [#268](https://gitlab.com/hestia-earth/hestia-schema/issues/268)
* **input:** add `impactAssessmentIsProxy` ([a52b9d8](https://gitlab.com/hestia-earth/hestia-schema/commit/a52b9d8f04cc466cdb874b1836f671d6d5701675))
* **input:** rename `fate` to `isAnimalFeed` ([cc74267](https://gitlab.com/hestia-earth/hestia-schema/commit/cc74267892a45adb37c23ff8b80850ae6b61603c)), closes [#264](https://gitlab.com/hestia-earth/hestia-schema/issues/264)
* make `methodClassificationDescription` required and improve docs ([f3c5496](https://gitlab.com/hestia-earth/hestia-schema/commit/f3c549671fe2d49ceffa5a8cb3a5e47fb29ab7a1)), closes [#261](https://gitlab.com/hestia-earth/hestia-schema/issues/261)
* **measurement:** add `properties` field ([d858d70](https://gitlab.com/hestia-earth/hestia-schema/commit/d858d7065c7cff805cad4898e203bf14bdfb8181))
* **property:** add `methodClassification` ([513a116](https://gitlab.com/hestia-earth/hestia-schema/commit/513a1167c8129d54af7574fab02ecfdfaf811967))
* **source:** add `license` required for public data ([6123a80](https://gitlab.com/hestia-earth/hestia-schema/commit/6123a802e0bc98d07c06ec0615170a0c09435ce2))
* **term:** set `casNumber` as unique ([0bd9608](https://gitlab.com/hestia-earth/hestia-schema/commit/0bd96087426e22ba349e97942cfec9606f27ed2d))


### Bug Fixes

* **cycle:** error in `defaultMethodClassification` enum ([668060f](https://gitlab.com/hestia-earth/hestia-schema/commit/668060f2678b1ecc421f16106912cf5f5d43eb43))

## [17.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v17.2.0...v17.3.0) (2023-03-15)


### Features

* add `uniqueArrayItem` for list of blank nodes with `term` ([6bb2a41](https://gitlab.com/hestia-earth/hestia-schema/commit/6bb2a41a034b2001a79a9d8f481a362266597561))
* **cycle:** add `commercialPracticeTreatment` field ([95581fe](https://gitlab.com/hestia-earth/hestia-schema/commit/95581fe085d814486d7859e1730c809e5c715c78)), closes [#260](https://gitlab.com/hestia-earth/hestia-schema/issues/260)
* **cycle:** add `sampleWeight` field ([f4a160b](https://gitlab.com/hestia-earth/hestia-schema/commit/f4a160b8751ea18ee8784c56ea7a6c2de7c242df)), closes [#139](https://gitlab.com/hestia-earth/hestia-schema/issues/139)
* **cycle:** validate `glass or high accessible cover` for `crop` functional unit ([2fa2e39](https://gitlab.com/hestia-earth/hestia-schema/commit/2fa2e3982e88ce1e65f0d5e9b1c331be61a89754))
* **input:** add `sd`, `min`, and `max` for `price` and `cost` ([5c08b69](https://gitlab.com/hestia-earth/hestia-schema/commit/5c08b690bc48f66a8ada1fde9ec3b11b573ed51d)), closes [#224](https://gitlab.com/hestia-earth/hestia-schema/issues/224)
* **practice:** add `sd`, `min`, and `max` for `price` and `cost` ([b1b2120](https://gitlab.com/hestia-earth/hestia-schema/commit/b1b2120a64ee44d6b0fc59dc8a357728631aff89)), closes [#224](https://gitlab.com/hestia-earth/hestia-schema/issues/224)
* **practice:** term with units `% area` should not use `areaPercent` ([a154ea6](https://gitlab.com/hestia-earth/hestia-schema/commit/a154ea6a8cded2072547f7a5505f0f4b7b1ae6da))
* **product:** add `sd`, `min`, and `max` for `price` and `revenue` ([39bc698](https://gitlab.com/hestia-earth/hestia-schema/commit/39bc6980c9493e72058296268d666e503dbc4924)), closes [#224](https://gitlab.com/hestia-earth/hestia-schema/issues/224)
* **source:** add `sampleDesign`, `weightingMethod`, and `experimentDesign` ([427e3e6](https://gitlab.com/hestia-earth/hestia-schema/commit/427e3e6562565fee067d4282766e0eb1b06cedb9)), closes [#19](https://gitlab.com/hestia-earth/hestia-schema/issues/19)
* **term:** add `sampleDesign` and `experimentDesign` `termTypes` ([07233ab](https://gitlab.com/hestia-earth/hestia-schema/commit/07233ab4d35340f91f99f3bf65e6e0ca1870e08c)), closes [#19](https://gitlab.com/hestia-earth/hestia-schema/issues/19)

## [17.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v17.1.1...v17.2.0) (2023-03-09)


### Features

* **input:** add `fate` field ([cd13939](https://gitlab.com/hestia-earth/hestia-schema/commit/cd139390f05be0cbee21983b7c9fbdc96e2b1598))
* **term:** add `feedFoodAdditive` `termType` ([9298340](https://gitlab.com/hestia-earth/hestia-schema/commit/92983401d0f5b0793821af6bfb9dcc694285b3fb))


### Bug Fixes

* **cycle:** validate `functionalUnit` = `1 ha` for `crop` on `cropland` only ([0119eb3](https://gitlab.com/hestia-earth/hestia-schema/commit/0119eb325e486eb03d4a11545742dd9d5b1f9a72))

### [17.1.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v17.1.0...v17.1.1) (2023-03-08)


### Bug Fixes

* **cycle:** remove validator not working on `material` ([31bcb1c](https://gitlab.com/hestia-earth/hestia-schema/commit/31bcb1c304e8ebe7f301ae1098f916436d5ba001))

## [17.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v17.0.0...v17.1.0) (2023-03-08)


### Features

* **practice:** add `properties` field ([8c15329](https://gitlab.com/hestia-earth/hestia-schema/commit/8c1532989c6ac0aace59e14f2e3a8e417b9d24d9))

## [17.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v16.0.0...v17.0.0) (2023-03-08)


### ⚠ BREAKING CHANGES

* **practice:** renamed `area` in `Practice` to `areaPercent`, expressed in `% area`.
* **site:** `site.practices` has been removed, use `cycle.practices` instead
* **infrastructure:** Infrastructure `term` is now a `required` field
* **measurement:** renamed `methodClassification` `on-site measurement` to `on-site physical measurement`
* **measurement:** renamed `methodClassification` `measurement on nearby site` to `physical measurement on nearby site`.
* **term:** `ecoinventActivityId` field has been removed in favour of `ecoinventName` and `ecoinventReferenceProduct` because the three fields "name", "reference product" and "geography" make ecoinvent activities unique
* **practice:** make `term` a required field for `Practices`
* **practice:** removes `properties` field from `Practice` node as this will become unused following the new approach for specifying herd composition

### Features

* **animal:** add new blank node ([9913447](https://gitlab.com/hestia-earth/hestia-schema/commit/99134475f7f15fc162c2277af7f3650727f31d2e))
* **cycle:** add `fate` as field that makes `products` unique ([a37d4a3](https://gitlab.com/hestia-earth/hestia-schema/commit/a37d4a3c748a836c09508e2cfd957b2c872ccfc7)), closes [#239](https://gitlab.com/hestia-earth/hestia-schema/issues/239)
* **cycle:** restrict `1 ha` functional unit on primary `crop` product only ([b462b96](https://gitlab.com/hestia-earth/hestia-schema/commit/b462b96606cada27446ea13af3334619b6f975da)), closes [#243](https://gitlab.com/hestia-earth/hestia-schema/issues/243)
* **cycle:** validate `liveAnimal` product should also contain `excreta` product ([26aec0a](https://gitlab.com/hestia-earth/hestia-schema/commit/26aec0a41995c9a153234a30970507b68d0b87f5))
* **emission:** add `methodTier` option `not relevant` ([bc6216f](https://gitlab.com/hestia-earth/hestia-schema/commit/bc6216f0edcf0e675dfcbc38b6557161eadd0613)), closes [#232](https://gitlab.com/hestia-earth/hestia-schema/issues/232)
* **infrastructure:** make `term` a required field and set uniqueness on site ([f1beef1](https://gitlab.com/hestia-earth/hestia-schema/commit/f1beef1e54759a8dece3827821975d3d2d6dccd0))
* **measurement:** add `regional` and `country-level` options for `methodClassifiation` ([b454882](https://gitlab.com/hestia-earth/hestia-schema/commit/b45488201ffeca0ea5ba275e8cb0965c716f70b0))
* **measurement:** remove `properties` field ([f685254](https://gitlab.com/hestia-earth/hestia-schema/commit/f685254909d5182594fb45f558ce934c11e08d12))
* **measurement:** update `methodClassification` options ([2e9c811](https://gitlab.com/hestia-earth/hestia-schema/commit/2e9c8119985591a25b50da8542ddb06d3e4531a7))
* **practice:** make `term` a required field ([ad60fe4](https://gitlab.com/hestia-earth/hestia-schema/commit/ad60fe4db0ec93dc02552eb5fba03c8e9e33396d))
* **practice:** remove `properties` field ([c2d78d1](https://gitlab.com/hestia-earth/hestia-schema/commit/c2d78d1efc6f814178829648b696229714e75f4b))
* **practice:** rename `area` to `areaPercent` ([c37b505](https://gitlab.com/hestia-earth/hestia-schema/commit/c37b50522f08d33f9ef7cc4bc604689aed012292))
* **practice:** require `currency` if `cost` or `price` specified ([769f413](https://gitlab.com/hestia-earth/hestia-schema/commit/769f4132fbf4d136b9f8d787562ff097c4e6be26))
* **product:** add `fate` field ([d0e3407](https://gitlab.com/hestia-earth/hestia-schema/commit/d0e340716b9861c26f9a4589961d145c46a3acbc)), closes [#239](https://gitlab.com/hestia-earth/hestia-schema/issues/239)
* **python:** add sort config to python module ([835f954](https://gitlab.com/hestia-earth/hestia-schema/commit/835f954a0f00111705ef22c423618a125b4a73e7))
* **schema-validation:** add `datePattern` validator ([e0a2044](https://gitlab.com/hestia-earth/hestia-schema/commit/e0a2044edd6b0c3f676e5a9262f36610d0e6ff55))
* **term:** add new `termType` `animalBreed` ([1e1c4c7](https://gitlab.com/hestia-earth/hestia-schema/commit/1e1c4c756ee9e648eb4dca969bd382193580e3b7))
* **term:** replace `ecoinventActivityId` with `ecoinventName` and `ecoinventReferenceProduct` ([0d01e53](https://gitlab.com/hestia-earth/hestia-schema/commit/0d01e5350f17278357d299250d5d77d001f4fde3))


### Bug Fixes

* **input:** error in validator for `currency` required if `cost` specified ([435732c](https://gitlab.com/hestia-earth/hestia-schema/commit/435732cba79ab963252f50fba4eae2e323f85254))
* **site:** remove `practices` from `Site`  ([4ea7e00](https://gitlab.com/hestia-earth/hestia-schema/commit/4ea7e0001b2560793f4250d045471aa89e1a1d19))

## [16.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v15.2.0...v16.0.0) (2023-02-21)


### ⚠ BREAKING CHANGES

* **measurement:** renamed field `methodModel` to `method`
* **measurement:** added required `methodClassification`
* renamed `methodClassification` `unsourced or assumption` into `unsourced assumption`
* **term:** Term `chemidplus` field has been removed.
* **completeness:** `completeness.oprations` has been added and is required
* **measurement:** `measurement.methodModel` is now required

### Features

* **completeness:** add `operations` field ([88d8146](https://gitlab.com/hestia-earth/hestia-schema/commit/88d8146640fa32d73f107d6cbda31b4ed1d7a27f))
* **measurement:** make `methodModel` a `required` field ([ba56896](https://gitlab.com/hestia-earth/hestia-schema/commit/ba5689629ee30994917f0c1ef18b436b14ffbc11))
* **measurement:** split `methodModel` into `method` and `methodClassification` ([13e3d7d](https://gitlab.com/hestia-earth/hestia-schema/commit/13e3d7de854ed5efb223da72acd2c7f0ad7595aa))
* **migrations:** migrate `completeness.operations` ([4d4f182](https://gitlab.com/hestia-earth/hestia-schema/commit/4d4f1829f214972aa6f35b180aca6dded2d2953d))
* **term:** remove `chemidplus` field ([d41751a](https://gitlab.com/hestia-earth/hestia-schema/commit/d41751a2fb6b189d5a9f886d6be82695f4743b2b))
* update `methodClassification` values ([1e75c12](https://gitlab.com/hestia-earth/hestia-schema/commit/1e75c124a427b8593248d570ba155891e1f72330))
* update sharing licence to CC-BY ([2bd6a8f](https://gitlab.com/hestia-earth/hestia-schema/commit/2bd6a8f9376ebc37443633a2579b3d3b29e351cc))


### Bug Fixes

* **schema-convert:** fix error parsing geojson ([63b7e79](https://gitlab.com/hestia-earth/hestia-schema/commit/63b7e798a55aa446bf8e3fdf27597bcfda5f789b))
* **term:** remove `cropProduct` as part of `processedFood` ([456f6a7](https://gitlab.com/hestia-earth/hestia-schema/commit/456f6a710d070935ba7f85e85c92f74322c04748))

## [15.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v15.1.0...v15.2.0) (2023-02-08)


### Features

* **term:** add new `termType` `cropProduct` ([7783fa6](https://gitlab.com/hestia-earth/hestia-schema/commit/7783fa68784d53f81447a86587b968cb6a993c48))
* **transport:** add `minimum` validator to `value`, `min`, and `max` ([0b8f1d0](https://gitlab.com/hestia-earth/hestia-schema/commit/0b8f1d0d3d76b0eb4306f9826248d4ca2ecf1fe2))
* **transport:** add fields to allow `distance` to be specified ([fb6b8cb](https://gitlab.com/hestia-earth/hestia-schema/commit/fb6b8cbed3dac57367eedd95c8ae62677c863771)), closes [#235](https://gitlab.com/hestia-earth/hestia-schema/issues/235)
* **transport:** add new required field `returnLegIncluded` ([24fe767](https://gitlab.com/hestia-earth/hestia-schema/commit/24fe767f50e39f8aee719d372c81926a504ed954)), closes [#211](https://gitlab.com/hestia-earth/hestia-schema/issues/211)
* **transport:** set `type` to `number` for `value`, `sd`, `min`, and `max` ([3107819](https://gitlab.com/hestia-earth/hestia-schema/commit/3107819391231af4e5fe96c6ef344ff868feef5a)), closes [#235](https://gitlab.com/hestia-earth/hestia-schema/issues/235)


### Bug Fixes

* **cycle:** remove `description` for uniqueness of `Emission` ([9ebef58](https://gitlab.com/hestia-earth/hestia-schema/commit/9ebef58b3cb8b65769bb97d596c4f867aa841652))
* **site:** fix uniqueness on Term [@id](https://gitlab.com/id) ([1656379](https://gitlab.com/hestia-earth/hestia-schema/commit/1656379eaf699b16cb8bc9feef8eff2b32d5b0fc))
* **transformation:** fix uniqueness on Term [@id](https://gitlab.com/id) ([c76c964](https://gitlab.com/hestia-earth/hestia-schema/commit/c76c96496319ad1317bfc883bf8d7ed76e189ed2))
* **transport:** fix uniqueness on Term [@id](https://gitlab.com/id) ([b164d13](https://gitlab.com/hestia-earth/hestia-schema/commit/b164d13ef2178e3a2b065873e55fd333b6d838e0))

## [15.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v15.0.0...v15.1.0) (2023-02-06)


### Features

* **cycle:** add `description` in `inputs` uniqueness ([b21fe9a](https://gitlab.com/hestia-earth/hestia-schema/commit/b21fe9af37c36ddb726b69c1c28d88eacdee53b7))


### Bug Fixes

* **cycle:** delete `source` as field which makes `emissions` unique ([98902af](https://gitlab.com/hestia-earth/hestia-schema/commit/98902af3321b9d096b1e4521bb22aa01aec8c128))

## [15.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v14.4.0...v15.0.0) (2023-01-26)


### ⚠ BREAKING CHANGES

* renamed some `methodClassification`:
* renamed `consistent external source` to `consistent external sources`
* rename `inconsistent external source` to `inconsistent external sources`

### Features

* add `methodClassificationDescription` field ([5a8e1f7](https://gitlab.com/hestia-earth/hestia-schema/commit/5a8e1f7c012ef13a5d0985e58aedf225ec989e86)), closes [#226](https://gitlab.com/hestia-earth/hestia-schema/issues/226)
* **indicator:** set default `value` = `null` ([3c2dc56](https://gitlab.com/hestia-earth/hestia-schema/commit/3c2dc56d329e69cbcf329a3964c992de4c2f5a73))
* make the `external source` enums plural for `methodClassification` ([951a1c4](https://gitlab.com/hestia-earth/hestia-schema/commit/951a1c4bdd0f552b3bdfb2e5900f10fbf6ee1274))
* **term:** add `ecolabelIndex` field to link to the Ecolabel Index website ([26d6273](https://gitlab.com/hestia-earth/hestia-schema/commit/26d6273b14ed3904063462d3bfddfafbeebc7065))


### Bug Fixes

* **schema-convert:** fix issue trim JSON content while converting ([3038cf0](https://gitlab.com/hestia-earth/hestia-schema/commit/3038cf03525ee69bffc7cf094dc1466ca6820355))
* **site:** set `exportDefaultCSV` to `true` for `area` ([9082352](https://gitlab.com/hestia-earth/hestia-schema/commit/90823526a229210dae4b1f97f3f8796e2869c981))
* **source:** remove `doiHESTIA` field as currently not used ([bce904c](https://gitlab.com/hestia-earth/hestia-schema/commit/bce904c0ab2481fc1351fa61cabb5151f4ef0f7b))

## [14.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v14.3.1...v14.4.0) (2023-01-09)


### Features

* **cycle:** add list of aggregated `Cycle` and `Source` ([5cabae3](https://gitlab.com/hestia-earth/hestia-schema/commit/5cabae3c386f459f7cce5de8a10bddf2981b4a51))
* **impact-assessment:** add list of aggregated `ImpactAssessment` and `Source` ([092653f](https://gitlab.com/hestia-earth/hestia-schema/commit/092653f2a8ad9120f19005acbe3a6d4d1882e2c3))
* **property:** add `methodMeasurement` as allowed `termType` for `methodModel` ([ab49741](https://gitlab.com/hestia-earth/hestia-schema/commit/ab4974131b7d0c0be4fbb8369f57878b2fa1cb66)), closes [#223](https://gitlab.com/hestia-earth/hestia-schema/issues/223)
* **site:** add list of aggregated `Site` and `Source` ([be59ea4](https://gitlab.com/hestia-earth/hestia-schema/commit/be59ea402999916e0bb76fc23e6ceca820e3b7ba))

### [14.3.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v14.3.0...v14.3.1) (2022-12-15)


### Bug Fixes

* **schema-convert:** handle no value in GeoJSON field ([c7e5387](https://gitlab.com/hestia-earth/hestia-schema/commit/c7e538732f2ca6a0acc5b1dd919393f26720456e))

## [14.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v14.2.0...v14.3.0) (2022-12-15)


### Features

* **product:** add `material` as allowed `termType` ([7c0644b](https://gitlab.com/hestia-earth/hestia-schema/commit/7c0644bc02d7a2496959132eba605ca79edc8aea)), closes [#217](https://gitlab.com/hestia-earth/hestia-schema/issues/217)


### Bug Fixes

* **schema-convert:** throw error if GeoJSON can not be parsed ([320a6d7](https://gitlab.com/hestia-earth/hestia-schema/commit/320a6d70401b2e8a26228489126b99f9d7a898cf))

## [14.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v14.1.0...v14.2.0) (2022-12-13)


### Features

* **schema-convert:** convert to `term.name` for blank nodes not providing full path ([dca17e4](https://gitlab.com/hestia-earth/hestia-schema/commit/dca17e4d7f26d73a7297abe4e338a366224417c4))

## [14.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v14.0.0...v14.1.0) (2022-12-08)


### Features

* **cycle:** add `transplanting date` enum to `startDateDefinition` ([d886a7f](https://gitlab.com/hestia-earth/hestia-schema/commit/d886a7feaac5b9abc06058d8a8b69f35f7314298))
* **cycle:** material inputs units must be in `kg/functional unit/Cycle` ([d60130f](https://gitlab.com/hestia-earth/hestia-schema/commit/d60130f88bd037736416a3d5f049ec5aa7820d79))
* **infrastructure:** material inputs units must be in `kg` ([4ca14e7](https://gitlab.com/hestia-earth/hestia-schema/commit/4ca14e7021fde88b258824d21c8f7284abf33155))
* **python:** add UNIQUENESS_FIELDS object ([abb5878](https://gitlab.com/hestia-earth/hestia-schema/commit/abb5878409d2166374afcf9ff332628c7a89c44d))
* **site:** add `areaMin` and `areaMax` fields ([bc82f35](https://gitlab.com/hestia-earth/hestia-schema/commit/bc82f351b4d9809efe9551c7933d181097e2d26f))
* **site:** allow practices with termType `aquacultureManagement` ([184fff1](https://gitlab.com/hestia-earth/hestia-schema/commit/184fff1b301795d4ffa107899ab6e443517807d8))
* **transport:** add `practices` field ([f1d2227](https://gitlab.com/hestia-earth/hestia-schema/commit/f1d2227c54c9947018ed7df5dfa3d8c8513cf043)), closes [#212](https://gitlab.com/hestia-earth/hestia-schema/issues/212)

## [14.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v13.0.1...v14.0.0) (2022-11-29)


### ⚠ BREAKING CHANGES

* **impact-assessment:** renamed `noneRequired` for `ImpactAssessment.allocationMethod` to `none required`, renamed `systemExpansion ` for `ImpactAssessment.allocationMethod` to `system expansion`
* added new required field `defaultMethodClassification` to Cycle and replace `reliability` by `methodClassification` in Blank Nodes
* **transformation:** `previousTransformationShare` renamed to `transformedShare`
* **cycle:** `dataCompleteness` renamed to `completeness`
* **term:** `termType` `grass` has been renamed to `forage`
* **completeness:** added new `dataCompleteness` field for `waste`
* **completeness:** renamed `dataCompleteness` field from `excretaManagement` to `excreta`
* **completeness:** adds new required completeness field

### Features

* **completeness:** add `liveAnimals` field ([3b57f5c](https://gitlab.com/hestia-earth/hestia-schema/commit/3b57f5ce60e15a2bef5365f423e7dc70f957cc11))
* **completeness:** add new `waste` field ([ca6f79c](https://gitlab.com/hestia-earth/hestia-schema/commit/ca6f79ca875e8fd8d20b9121d220a4e5c0189cf5))
* update following Style Guide v1 ([c966cf0](https://gitlab.com/hestia-earth/hestia-schema/commit/c966cf076ae67628cf320dbb4061da2a959164ec))


### Bug Fixes

* **impact-assessment:** rewrite camelCase values for `allocationMethod` ([4436346](https://gitlab.com/hestia-earth/hestia-schema/commit/44363461c2754c1457619ef8d4674255cb6e0ba9))
* replace `reliability` field by `methodClassification` ([b7e61e3](https://gitlab.com/hestia-earth/hestia-schema/commit/b7e61e3c5af3723fb73684a59fb544a102afc7e5))


* **completeness:** rename `excretaManagement` to `excreta` ([143ad38](https://gitlab.com/hestia-earth/hestia-schema/commit/143ad38a0cabdc07a356a8504730c092e5f73668))
* **cycle:** rename `dataCompleteness` field `completeness` ([7dccc91](https://gitlab.com/hestia-earth/hestia-schema/commit/7dccc910b26e4a7766baaa6134e7cfeb6fb545e1))
* **term:** rename `grass` `termType` to `forage` ([2566d9b](https://gitlab.com/hestia-earth/hestia-schema/commit/2566d9b2d09701519debe1e4f5666b9cd5134273))
* **transformation:** rename `previousTransformationShare` to `transformedShare` ([ad8a25e](https://gitlab.com/hestia-earth/hestia-schema/commit/ad8a25e0f5d39409b20021cb7cbbd8cd1bcf25b6))

### [13.0.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v13.0.0...v13.0.1) (2022-11-08)


### Bug Fixes

* **transport:** delete `dataState` field ([4a9690b](https://gitlab.com/hestia-earth/hestia-schema/commit/4a9690b2b146c139033551f2bc47f60923903694))

## [13.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v12.0.0...v13.0.0) (2022-10-19)


### ⚠ BREAKING CHANGES

* **term:** `cropProtection` Terms have been merged in `building`.

### Features

* **term:** remove `cropProtection` as `termType` ([eac7072](https://gitlab.com/hestia-earth/hestia-schema/commit/eac70723ce356803755fe9ff5542e477dfe70612))


### Bug Fixes

* **json-schema:** handle all nodes on export as CSV ([3538362](https://gitlab.com/hestia-earth/hestia-schema/commit/3538362abc999a1f1a4c4302422bf8f4713edfd8))

## [12.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v11.3.1...v12.0.0) (2022-10-10)


### ⚠ BREAKING CHANGES

* **term:** `inorganicFertilizer` renamed to `inorganicFertiliser` and
`organicFertilizer` renamed to `organicFertiliser`

### Bug Fixes

* **completeness:** rename `fertilizer` in `fertiliser` ([16bf925](https://gitlab.com/hestia-earth/hestia-schema/commit/16bf92532e1221dead7105251ca72da17cbc55cf))
* **term:** rename `fertilizer` to `fertiliser` ([4daa25d](https://gitlab.com/hestia-earth/hestia-schema/commit/4daa25d7fb31e2ee2e785cdeb8b57760d3e56cd9))

### [11.3.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v11.3.0...v11.3.1) (2022-10-06)


### Bug Fixes

* **json-schema:** handle no `properties` in definition ([c1ba474](https://gitlab.com/hestia-earth/hestia-schema/commit/c1ba474f27add1ce05cfc2664bcf9e4a5db2400d))

## [11.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v11.2.0...v11.3.0) (2022-10-03)


### Features

* add `recommended` property to fields ([9d9aa7e](https://gitlab.com/hestia-earth/hestia-schema/commit/9d9aa7ecf16be929e19d1f21339eec2493b27b44)), closes [#190](https://gitlab.com/hestia-earth/hestia-schema/issues/190)
* **json-schema:** add function to return `recommended` fields ([08e3613](https://gitlab.com/hestia-earth/hestia-schema/commit/08e361346a7ec0d14baf65f2fc1219f19cdf87cd))

## [11.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v11.1.0...v11.2.0) (2022-09-26)


### Features

* add validator to ensure values present if `statsDefinition` is ([ba22635](https://gitlab.com/hestia-earth/hestia-schema/commit/ba2263549995c44ac8727fea1de1fff3f244feaa)), closes [#187](https://gitlab.com/hestia-earth/hestia-schema/issues/187)
* **impact-assessment:** add `productValue` field to store `value` of `Product` ([d7ac864](https://gitlab.com/hestia-earth/hestia-schema/commit/d7ac86444612881f5709c46d1b072b21c32a30ad)), closes [#189](https://gitlab.com/hestia-earth/hestia-schema/issues/189)

## [11.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v11.0.0...v11.1.0) (2022-09-13)


### Features

* **schema-convert:** set default values on nodes ([80719b2](https://gitlab.com/hestia-earth/hestia-schema/commit/80719b2067e199624aefc6a2018baaea8d3c7898))

## [11.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v10.0.2...v11.0.0) (2022-09-09)


### ⚠ BREAKING CHANGES

* changes `otherObservations` to `other
observations` in `statsDefinition` as other `enum` fields in the
schema do not use camel case
* **impact-assessment:** Only `termType` of `emission` or `resourceUse` allowed
for `impactAssessment.emissionsResourceUse`.

### Features

* change `otherObservations` to `other observations` in `statsDefinition` ([229c5f8](https://gitlab.com/hestia-earth/hestia-schema/commit/229c5f8ef73b43c53a241428b3529306a920b328))
* **impact-assessment:** remove `characterisedIndicator` from `emissionsResourceUse` ([1b48eee](https://gitlab.com/hestia-earth/hestia-schema/commit/1b48eeeb9b795577d0ae3d3ebe8a59a7b0645ece))
* make `aggregatedQualityScore` searchable ([cfffb18](https://gitlab.com/hestia-earth/hestia-schema/commit/cfffb1853fa1d4d7b50811fba64a7cec10eee203))

### [10.0.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v10.0.1...v10.0.2) (2022-08-23)


### Bug Fixes

* **schema:** fix migrate transport only set for cycle rows ([8a40942](https://gitlab.com/hestia-earth/hestia-schema/commit/8a4094250706fa8c9ee2725b3161b346c0e4bc08))

### [10.0.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v10.0.0...v10.0.1) (2022-08-23)


### Bug Fixes

* **schema:** fix migration `cycle.dataCompleteness.transport` ([51a12c0](https://gitlab.com/hestia-earth/hestia-schema/commit/51a12c03d1cc513f54e5ced021c9d21ff045f968))

## [10.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v9.7.1...v10.0.0) (2022-08-23)


### ⚠ BREAKING CHANGES

* **completeness:** adds new required `Completeness` field for `transport`
* **emission:** Only `termType=emission` for `Emission` blank node is allowed.

### Features

* add `aggregatedQualityScore` to `Cycle` and `ImpactAssessment` ([22138f1](https://gitlab.com/hestia-earth/hestia-schema/commit/22138f171d8e5b89caff228d67c02f6a1a042262)), closes [#185](https://gitlab.com/hestia-earth/hestia-schema/issues/185)
* **completeness:** add field for `transport` ([838f460](https://gitlab.com/hestia-earth/hestia-schema/commit/838f4605bd7fabbea78f56b527a27fb18babc685))
* **emission:** restrict `termType` to `emission` ([5ad435f](https://gitlab.com/hestia-earth/hestia-schema/commit/5ad435f0f761a0ac003a7f97191faf2eee4394eb))
* **practice:** allow `boolean` for `value` ([781854a](https://gitlab.com/hestia-earth/hestia-schema/commit/781854a5961d95e6262b9b1cccf7117614bda617))
* **transport:** add new blank node and link to `Input`, `Product` and `Emission` ([fd74f41](https://gitlab.com/hestia-earth/hestia-schema/commit/fd74f4100f5c0d0fc0e548fe2dff874bab92d1c3))

### [9.7.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v9.7.0...v9.7.1) (2022-08-04)


### Bug Fixes

* **schema-convert:** convert WKT to a `FeatureCollection` ([423ac90](https://gitlab.com/hestia-earth/hestia-schema/commit/423ac9031140a605ebd7549476bc8c0d5f1a4a88))

## [9.7.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v9.6.0...v9.7.0) (2022-08-02)


### Features

* add `biologicalControlAgent` as new `termType` ([c0c6f80](https://gitlab.com/hestia-earth/hestia-schema/commit/c0c6f802d2ac08e6eb3e5b0a3a5a377e4ad4204b))
* **impact-assessment:** require `methodModel` for `endpoints` ([3cf29b8](https://gitlab.com/hestia-earth/hestia-schema/commit/3cf29b897c9a156639852b23c6704b081b137cb4))
* **term:** add `cornellBiologicalControl` as field ([974a002](https://gitlab.com/hestia-earth/hestia-schema/commit/974a00278cb4ad1ec72c40ce5d1624f979de711f))

## [9.6.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v9.5.0...v9.6.0) (2022-07-21)


### Features

* add `operation` to unicity for `emissions` and `emissionsResourceUse` ([ff42c12](https://gitlab.com/hestia-earth/hestia-schema/commit/ff42c1200803bda3f309691addd981b223a18c28))
* **emission:** add `operation` field ([b14aec3](https://gitlab.com/hestia-earth/hestia-schema/commit/b14aec3822ee795070d2b0e39523ad4bb9a962db))


### Bug Fixes

* **schema-validation:** fix validate all items in json array ([2e3edbd](https://gitlab.com/hestia-earth/hestia-schema/commit/2e3edbd8439aeede02f13670eb1f2c9b9b489b3c))

## [9.5.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v9.4.0...v9.5.0) (2022-07-20)


### Features

* **impact-assessment:** add migration remove `systemBoundary` ([47601e5](https://gitlab.com/hestia-earth/hestia-schema/commit/47601e5ce15ce196fb5aad4dcdd7a3ddb37cb9b5))
* **indicator:** add `operation` as field and make documentation consistent ([9d2ac6b](https://gitlab.com/hestia-earth/hestia-schema/commit/9d2ac6b19a341686cdf0ad8c8e79f9d5ef2872b3))
* **schema-convert:** handle `GeoJSON` uploaded in WTK format ([4af5829](https://gitlab.com/hestia-earth/hestia-schema/commit/4af58294c8570eba35bde31fb2dae5a7935e2cb8))

## [9.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v9.3.0...v9.4.0) (2022-07-18)


### Features

* **impact-assessment:** add `methodModel` as `uniqueArrayItem` on `emissionsResourceUse` ([3267246](https://gitlab.com/hestia-earth/hestia-schema/commit/3267246981018c0efe973645c47f4eeb7d56a3b1))
* **indicator:** add `transformation` field as `Term` ([6c55942](https://gitlab.com/hestia-earth/hestia-schema/commit/6c5594291d9ba457e9b4a48c7e66c439ff9d0c75))


### Bug Fixes

* **schema:** handle arrays of multiple types ([7853f8e](https://gitlab.com/hestia-earth/hestia-schema/commit/7853f8e95122c17aa2a40f8c77cc5a80458d298a))

## [9.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v9.2.0...v9.3.0) (2022-07-11)


### Features

* allow `null` in arrays of `value`, `sd`, `min`, `max`, and `observations` ([f7ef4db](https://gitlab.com/hestia-earth/hestia-schema/commit/f7ef4db62c7f166531b15fbf62bebdfdab12f468)), closes [#175](https://gitlab.com/hestia-earth/hestia-schema/issues/175)
* **schema-convert:** handle `null` values in arrays ([c5c3052](https://gitlab.com/hestia-earth/hestia-schema/commit/c5c30523a69da3cc5a3755df8d1eaae937c8ea9f))

## [9.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v9.1.0...v9.2.0) (2022-07-08)


### Features

* **practice:** add `area` as new field ([fa85296](https://gitlab.com/hestia-earth/hestia-schema/commit/fa85296ea6f1b0bbd0340917aaeb41a87dfd3197))

## [9.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v9.0.0...v9.1.0) (2022-07-04)


### Features

* **cycle:** add `transformation` to unicity of `emissions` ([b46e15b](https://gitlab.com/hestia-earth/hestia-schema/commit/b46e15b6146ce54f1168619230dfa931ff2a1c76))
* **cycle:** ensure each `transformations` is unique by `term.[@id](https://gitlab.com/id)` ([f904c94](https://gitlab.com/hestia-earth/hestia-schema/commit/f904c94500ee542a77baf7332a67765fc7d831f2))
* **emission:** add `transformation` field ([670ea91](https://gitlab.com/hestia-earth/hestia-schema/commit/670ea919bb6267fde5b8be388aaa74d8a01fe2d8))
* **transformation:** add `uniqueArrayItem` on `inputs`, `emissions`, `products` and `practices` ([d223c79](https://gitlab.com/hestia-earth/hestia-schema/commit/d223c79d8ca619fd3c96631a1eb08badbded7bab))
* **transformation:** prevent setting `emissions.transformation` ([ef2095b](https://gitlab.com/hestia-earth/hestia-schema/commit/ef2095bc009db4da66133e2556e46fedbf98b138))

## [9.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v8.3.0...v9.0.0) (2022-07-01)


### ⚠ BREAKING CHANGES

* **term:** `biodiversity` as `termType` for `Term` has been removed.

### Features

* **site:** add `siteType` `glass or high accessible cover` ([70c7fac](https://gitlab.com/hestia-earth/hestia-schema/commit/70c7facc8b3451fcd6d82ef98c99cb107068d948)), closes [#159](https://gitlab.com/hestia-earth/hestia-schema/issues/159)


### Bug Fixes

* **term:** remove `biodiversity` `termType` ([bee1e94](https://gitlab.com/hestia-earth/hestia-schema/commit/bee1e947f7df3ef701a6aed66da23311f4aac2ca))

## [8.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v8.2.0...v8.3.0) (2022-06-29)


### Features

* add `grass` to allowed termTypes ([cd79265](https://gitlab.com/hestia-earth/hestia-schema/commit/cd792657eadb9dfee285a8ef2c41e3d0b3c3b7fc)), closes [#171](https://gitlab.com/hestia-earth/hestia-schema/issues/171)
* set additional fields as selected in CSV export ([89a444f](https://gitlab.com/hestia-earth/hestia-schema/commit/89a444fecdeabcbebcec13876e07801f2dc9275f))
* **term:** add `grass` termType ([b2b2d39](https://gitlab.com/hestia-earth/hestia-schema/commit/b2b2d3902a09c91dc7f6f361cb1976c71c5b91fd)), closes [#171](https://gitlab.com/hestia-earth/hestia-schema/issues/171)

## [8.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v8.1.0...v8.2.0) (2022-06-27)


### Features

* **input:** add `region` ([e3b0f9b](https://gitlab.com/hestia-earth/hestia-schema/commit/e3b0f9bcee75bc8e5a3549f9f4ea3deda63fab78))
* **property:** add `dataState` field ([f6713f6](https://gitlab.com/hestia-earth/hestia-schema/commit/f6713f6d991430284c172d3a0b07ca05a13e71f3)), closes [#169](https://gitlab.com/hestia-earth/hestia-schema/issues/169)
* **schema-convert:** handle float values used as `boolean` ([2c5a78e](https://gitlab.com/hestia-earth/hestia-schema/commit/2c5a78e1aab2b9948eb403c8747d9b779c2bd7f2))
* set minimum number of observations to 1 where missing ([1a309bc](https://gitlab.com/hestia-earth/hestia-schema/commit/1a309bc41b5d9d79e53f12811fff10da95677ec2)), closes [#166](https://gitlab.com/hestia-earth/hestia-schema/issues/166)


### Bug Fixes

* **schema-convert:** skip suggestion of `internal` properties and `[@type](https://gitlab.com/type)`/`type` ([9993a1d](https://gitlab.com/hestia-earth/hestia-schema/commit/9993a1daf1492fe4ef5ce1b02bf2d4821238008a))

## [8.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v8.0.0...v8.1.0) (2022-06-23)


### Features

* **cycle:** validate `otherSites` required if `otherSitesDuration` is set ([a125b6c](https://gitlab.com/hestia-earth/hestia-schema/commit/a125b6c97964db338903d9f485ef4b95a540ac14))
* **measurement:** index `endDate` ([c13fe07](https://gitlab.com/hestia-earth/hestia-schema/commit/c13fe07e2b291f06ebe33649f3047da7164933d7))
* **schema-convert:** return suggestions on `property-not-found` error ([496f196](https://gitlab.com/hestia-earth/hestia-schema/commit/496f196939fd7b9b804009b8b1bfcc18efbfc847))


### Bug Fixes

* **schema-convert:** handle null value converting to CSV ([58c6a9e](https://gitlab.com/hestia-earth/hestia-schema/commit/58c6a9e7034d2407e2015c7d310831cb0949cb97))

## [8.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.11.0...v8.0.0) (2022-06-14)


### ⚠ BREAKING CHANGES

* **impact-assessment:** `impactAssessment.systemBoundary` has been removed.
* **cycle:** `cycle.dataDescription` has been removed.

### Features

* **cycle:** add `numberOfReplications` field ([6ce1d53](https://gitlab.com/hestia-earth/hestia-schema/commit/6ce1d53b57a17f30439d997837dc0e2277330f19))
* **emission:** update `statsDefinition` possible values ([e101c7e](https://gitlab.com/hestia-earth/hestia-schema/commit/e101c7e67cb678d1ee5f5b602db933b5ab69ced5))
* **indicator:** update `statsDefinition` possible values ([8c68d87](https://gitlab.com/hestia-earth/hestia-schema/commit/8c68d87648b1571ea0a70e2b5cd1aa88e159a55b))
* **input:** update `statsDefinition` possible values ([c1a0769](https://gitlab.com/hestia-earth/hestia-schema/commit/c1a0769441559cd1d85d4f21ce0c5ff7213f8706))
* **measurement:** update `statsDefinition` possible values ([f6526dc](https://gitlab.com/hestia-earth/hestia-schema/commit/f6526dce06d9f13478ff80e95819e8b4d6d6459a))
* **practice:** update `statsDefinition` possible values ([11666e2](https://gitlab.com/hestia-earth/hestia-schema/commit/11666e2b07077a5c5a52768e7e1c1e4cb58eadd5))
* **product:** allow search by `primary` product ([b797065](https://gitlab.com/hestia-earth/hestia-schema/commit/b79706548ea6fec4d9b7e786d4bfb2069c4d4f6c))
* **product:** update `statsDefinition` possible values ([ecc8588](https://gitlab.com/hestia-earth/hestia-schema/commit/ecc8588a568d8e8883730b39d93a9fca6c6cbef5))
* **property:** update `statsDefinition` possible values ([6d6d55e](https://gitlab.com/hestia-earth/hestia-schema/commit/6d6d55e1f753e9a16115a43b01383dd59586febc))
* **site:** set `measurements` and `practices` as searchable ([1019122](https://gitlab.com/hestia-earth/hestia-schema/commit/1019122740ec6db20ee5a7212358c2274b5f5d03))


### Bug Fixes

* **cycle:** remove `dataDescription` ([a995b87](https://gitlab.com/hestia-earth/hestia-schema/commit/a995b8749a2126ce710ae04bc7faefe4642f50a6))
* **impact-assessment:** remove `systemBoundary` ([d13fb6a](https://gitlab.com/hestia-earth/hestia-schema/commit/d13fb6a6360ff6e0c72a666d4c24921c91282892)), closes [#162](https://gitlab.com/hestia-earth/hestia-schema/issues/162)
* **schema-convert:** set default values on `Actor` ([ee769ac](https://gitlab.com/hestia-earth/hestia-schema/commit/ee769ac79c492415cb31d7f1847550586072654d))

## [7.11.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.10.0...v7.11.0) (2022-06-02)


### Features

* **python:** add node type convenience methods ([91c5b6e](https://gitlab.com/hestia-earth/hestia-schema/commit/91c5b6efad5b73471d6af4dd5053aec24727cdd0))
* **site:** add `tenure` ([7590368](https://gitlab.com/hestia-earth/hestia-schema/commit/75903687e7dba4b862bd5f40abd0337a764b2f43))

## [7.10.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.9.0...v7.10.0) (2022-05-16)


### Features

* **impact-assessment:** allow multiple `emissionsResourceUse` with different `inputs` ([6c1403c](https://gitlab.com/hestia-earth/hestia-schema/commit/6c1403cfadeb796011e5b342707959b5a0865630))
* **indicator:** add `inputs` ([6da6020](https://gitlab.com/hestia-earth/hestia-schema/commit/6da602048e09932e9541386781061e99d7a38f3f))

## [7.9.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.8.0...v7.9.0) (2022-05-11)


### Features

* **json-schema:** export method `schemaFromKey` ([ea04350](https://gitlab.com/hestia-earth/hestia-schema/commit/ea0435013a9bd72e0626c53ed9bfcbaaae4fee8d))
* **schema-convert:** throw error if boolean value not recognised ([84040de](https://gitlab.com/hestia-earth/hestia-schema/commit/84040ded1637f14bcf56128cff01df235dfd7d41))


### Bug Fixes

* **schema-convert:** ignore columns with empty header when convert to JSON ([5670853](https://gitlab.com/hestia-earth/hestia-schema/commit/5670853026dc0a61a3ef0d63e4472a7702b7ae44))

## [7.8.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.7.1...v7.8.0) (2022-04-15)


### Features

* **cycle:** require `defaultSource` for public data only ([4ca7e24](https://gitlab.com/hestia-earth/hestia-schema/commit/4ca7e2430aaa023e084f899bc3cd2ba642a37675))
* **impact-assessment:** require `source` for public data only ([77a5a06](https://gitlab.com/hestia-earth/hestia-schema/commit/77a5a0690a8a785eae6463c87c9239d418c9d33a))
* **schema-convert:** convert nested node to `id` when no field specified ([aa5329f](https://gitlab.com/hestia-earth/hestia-schema/commit/aa5329fc3d68cec9ebfdf6ce4f24d4a6e1fa09ee))
* **site:** require `defaultSource` for public data only ([b4b631d](https://gitlab.com/hestia-earth/hestia-schema/commit/b4b631d22f8161e575f74a331bf6cd5e8039af91))

### [7.7.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.7.0...v7.7.1) (2022-04-07)


### Bug Fixes

* **schema-convert:** handle default value for unset field ([12e22f8](https://gitlab.com/hestia-earth/hestia-schema/commit/12e22f85f2697f026ac45ab0fb17dbd6f4b82e9d))
* **schema-convert:** handle empty `undefined` value ([1e9c5aa](https://gitlab.com/hestia-earth/hestia-schema/commit/1e9c5aa340a235a2ed6025e9af46be3042acfb8a))

## [7.7.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.6.6...v7.7.0) (2022-04-07)


### Features

* **schema-validation:** validate arrays same size ([f4c38e9](https://gitlab.com/hestia-earth/hestia-schema/commit/f4c38e9ad808d308f1669ed51404e8dc8bdb762e))


### Bug Fixes

* **schema-validation:** fix validation not working on multiple files ([6d820c1](https://gitlab.com/hestia-earth/hestia-schema/commit/6d820c14043ee7e91ca1578d29fca109da039d9c))

### [7.6.6](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.6.5...v7.6.6) (2022-03-31)


### Bug Fixes

* **schema-convert:** remove empty blank node in arrays ([29157c9](https://gitlab.com/hestia-earth/hestia-schema/commit/29157c9f79164417fd66334a3a86f212076786d5))

### [7.6.5](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.6.4...v7.6.5) (2022-03-31)


### Bug Fixes

* **schema-convert:** remove empty blank nodes ([4f576d3](https://gitlab.com/hestia-earth/hestia-schema/commit/4f576d3bcc3b5bc393be8d8c613820d74b8b590c))

### [7.6.4](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.6.3...v7.6.4) (2022-03-31)


### Bug Fixes

* **schema-convert:** fix empty cells not removed ([8695713](https://gitlab.com/hestia-earth/hestia-schema/commit/869571349bf0c84543b89193e14e6b88e784dce6))

### [7.6.3](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.6.2...v7.6.3) (2022-03-31)


### Bug Fixes

* **schema-convert:** handle empty value non object ([c6e798a](https://gitlab.com/hestia-earth/hestia-schema/commit/c6e798a211ed545f525f5c61fb2b1c8ff0983530))

### [7.6.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.6.1...v7.6.2) (2022-03-31)


### Bug Fixes

* **schema-convert:** convert empty to `null` when allowed ([d74e159](https://gitlab.com/hestia-earth/hestia-schema/commit/d74e15904d4d3c693e7641ee05d2cd1bc16f1b70))

### [7.6.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.6.0...v7.6.1) (2022-03-29)


### Bug Fixes

* **schema-validation:** always validate content asynchronously ([b983a64](https://gitlab.com/hestia-earth/hestia-schema/commit/b983a64947aed9f315f73519be1219ab10068ba0))

## [7.6.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.5.2...v7.6.0) (2022-03-29)


### Features

* make `observations` field searchable ([3800219](https://gitlab.com/hestia-earth/hestia-schema/commit/380021922184cc73d3858d729ef51215197d6512))

### [7.5.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.5.1...v7.5.2) (2022-03-26)


### Bug Fixes

* **schema-convert:** escape array values ([fb25c0d](https://gitlab.com/hestia-earth/hestia-schema/commit/fb25c0d3b2da7756420c1558448dc886dabd9a50))

### [7.5.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.5.0...v7.5.1) (2022-03-16)


### Bug Fixes

* **term:** include `area` by default when extending ([4c12de2](https://gitlab.com/hestia-earth/hestia-schema/commit/4c12de2a30a35dc5fdf9c5ef4c316e7f56f87ceb))

## [7.5.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.4.0...v7.5.0) (2022-03-16)


### Features

* **term:** add `area` field ([92c57a7](https://gitlab.com/hestia-earth/hestia-schema/commit/92c57a731fe92a1802ac466807845ef30f354849))

## [7.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.3.1...v7.4.0) (2022-03-13)


### Features

* **cycle:** add `harvestedArea` field ([c0883e2](https://gitlab.com/hestia-earth/hestia-schema/commit/c0883e247a78935d6f4951a4eac56417d05e80fd))


### Bug Fixes

* **schema-convert:** disable add default properties on nested nodes ([0ef4f15](https://gitlab.com/hestia-earth/hestia-schema/commit/0ef4f1596b0ea22621599206877464c97d8879da))

### [7.3.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.3.0...v7.3.1) (2022-02-19)


### Bug Fixes

* **schema-convert:** handle convert CSV in Browser ([cf29141](https://gitlab.com/hestia-earth/hestia-schema/commit/cf291417b5e1727c1483161a09ae92042379f85d))

## [7.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.2.0...v7.3.0) (2022-02-19)


### Features

* **cycle:** add `country` for `inputs` unicity ([b4a520a](https://gitlab.com/hestia-earth/hestia-schema/commit/b4a520ac97720a6812754021939f5c7806756a8b))
* **schema-convert:** improve conversion on multiple types ([9dd6c0b](https://gitlab.com/hestia-earth/hestia-schema/commit/9dd6c0b9ee734525f476eaf01c95a09de4180a78))


### Bug Fixes

* **json-schema:** handle schema not found in recursion ([e8bd1d6](https://gitlab.com/hestia-earth/hestia-schema/commit/e8bd1d6cd7b31e27e99952932caf7b9a69a05e69))

## [7.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.1.0...v7.2.0) (2022-02-14)


### Features

* **measurement:** handle date-time for `dates` ([63a3d2f](https://gitlab.com/hestia-earth/hestia-schema/commit/63a3d2f77d49b96722d9647381acd1dacaf8a615))


### Bug Fixes

* **site:** remove `required` on `name` ([0cf2bbe](https://gitlab.com/hestia-earth/hestia-schema/commit/0cf2bbec284ef0074efbebc09a6919c55249eba4))

## [7.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v7.0.0...v7.1.0) (2022-02-09)


### Features

* **impact-assessment:** add `systemExpansion` ([e9e0326](https://gitlab.com/hestia-earth/hestia-schema/commit/e9e032633a330883b03daa5b1d8fe84a09b77cf4))

## [7.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.15.1...v7.0.0) (2022-01-18)


### ⚠ BREAKING CHANGES

* **source:** `metaAnalysisBibliography` converted to `metaAnalyses` as a list of `Source`

### Features

* **schema-convert:** sort headers alphabetically when pivoting csv ([17b8c77](https://gitlab.com/hestia-earth/hestia-schema/commit/17b8c774b3da3f6a453c9ef1804aee7320052c6f))


### Bug Fixes

* **source:** replace `metaAnalysisBibliography` by `metaAnalyses` as `Source` list ([988fdd7](https://gitlab.com/hestia-earth/hestia-schema/commit/988fdd7a27f293fb54324ecf9f64be0aca181a14))

### [6.15.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.15.0...v6.15.1) (2022-01-14)


### Bug Fixes

* **schema-convert:** include `originalId` when pivoting csv file ([8f79a2e](https://gitlab.com/hestia-earth/hestia-schema/commit/8f79a2ec7318254f968bb579a612bb66aaca3620))

## [6.15.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.14.1...v6.15.0) (2022-01-11)


### Features

* **indicator:** allow `null` type for `value` ([7b72281](https://gitlab.com/hestia-earth/hestia-schema/commit/7b722810b41dcceff0a554a1ddd49e9917dc0c95))
* **json-schema:** add method to filter internal keys in export CSV ([ada7468](https://gitlab.com/hestia-earth/hestia-schema/commit/ada74687df04152bdcb648c353a05433a08ff2df))

### [6.14.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.14.0...v6.14.1) (2021-12-14)


### Bug Fixes

* **impact-assessment:** allow multiple impacts/endpoints different model ([671f3ec](https://gitlab.com/hestia-earth/hestia-schema/commit/671f3ecf74826dedd3855fc8a40cedc5081102f9))

## [6.14.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.13.0...v6.14.0) (2021-12-13)


### Features

* **emission:** enable search by `methodTier` ([e85bd6c](https://gitlab.com/hestia-earth/hestia-schema/commit/e85bd6cec53d1e80267a6c778da902bddaabc31c))
* **impact-assessment:** add list of `endpoints` of type `endpointIndicator` ([c206b50](https://gitlab.com/hestia-earth/hestia-schema/commit/c206b50434ee8e2bf768447a7aae9abec69d9815))
* **term:** add new `termType` = `endpointIndicator` ([b162890](https://gitlab.com/hestia-earth/hestia-schema/commit/b162890a413ff126bac4eb63e9a91a22e67c7787))

## [6.13.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.12.0...v6.13.0) (2021-11-15)


### Features

* **cycle:** add `stocking date` for `startDateDefinition` ([d5c3107](https://gitlab.com/hestia-earth/hestia-schema/commit/d5c3107a1e6f34495d574119c6c62233f406ce65))
* **cycle:** allow to search for `functionalUnit` ([d71242a](https://gitlab.com/hestia-earth/hestia-schema/commit/d71242afc57c586707eaa22b96438659cf62c583))


### Bug Fixes

* **schema convert:** fix convert csv with pivot ([9b4802c](https://gitlab.com/hestia-earth/hestia-schema/commit/9b4802c2a832465ace645828444075a84af30fcd))

## [6.12.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.11.0...v6.12.0) (2021-10-23)


### Features

* **python:** generate validation enums ([f8ec4d1](https://gitlab.com/hestia-earth/hestia-schema/commit/f8ec4d1ef675eee96170d4d41bac096587e8dfb8))

## [6.11.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.10.0...v6.11.0) (2021-10-21)


### Features

* **product:** allo `electricity` ([0f2da94](https://gitlab.com/hestia-earth/hestia-schema/commit/0f2da942d7ce4dcffff4f47e9db98099d7a24c8d))

## [6.10.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.9.0...v6.10.0) (2021-10-17)


### Features

* **product:** add `organicFertilizer` as allowed `term` ([4bfe576](https://gitlab.com/hestia-earth/hestia-schema/commit/4bfe576dede9e90f1d58800f2686a85542f5eb24))
* **schema convert:** add method to convert to csv pivoting blank nodes ([197fbd7](https://gitlab.com/hestia-earth/hestia-schema/commit/197fbd7852f013a9417c2ee75780476ee2925f6c))

## [6.9.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.8.0...v6.9.0) (2021-10-13)


### Features

* **emission:** allow `transformation.term` as `inputs` ([9db6894](https://gitlab.com/hestia-earth/hestia-schema/commit/9db689497c9e52954aa23405bafe24cf3b039094))

## [6.8.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.7.0...v6.8.0) (2021-10-04)


### Features

* export `TermTermType` validation ([e7faa4c](https://gitlab.com/hestia-earth/hestia-schema/commit/e7faa4c7a09b8031daf4186408bfdb6c443e8135))
* **input product:** require `currency` if `price` or `revenue` provided ([3fc9586](https://gitlab.com/hestia-earth/hestia-schema/commit/3fc958615ce89413994cee71719359b5db4cb47a))

## [6.7.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.6.2...v6.7.0) (2021-09-09)


### Features

* **emission:** add `deleted` field ([95bb811](https://gitlab.com/hestia-earth/hestia-schema/commit/95bb811cd9699faf270e7e7daf4273d857644ad5))
* **term:** add `tillage` `termType` ([99fc726](https://gitlab.com/hestia-earth/hestia-schema/commit/99fc726aa7fc0222810440d08a5cb3ce4e6952f1))

### [6.6.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.6.1...v6.6.2) (2021-09-03)


### Bug Fixes

* **schema validation:** fix strict mode on blank node ([e1f2895](https://gitlab.com/hestia-earth/hestia-schema/commit/e1f2895e9725a6408be356002e6f1acd9fb7ca58))

### [6.6.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.6.0...v6.6.1) (2021-09-03)


### Bug Fixes

* **schema validation:** set `strictMode` on top-level nodes only ([08851eb](https://gitlab.com/hestia-earth/hestia-schema/commit/08851eb6784118e094e64754970aa82f0078ee1b))

## [6.6.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.5.0...v6.6.0) (2021-08-30)


### Features

* **input:** add `country` field ([7ce282d](https://gitlab.com/hestia-earth/hestia-schema/commit/7ce282d828024664dfbe27b434d46462a98f699f)), closes [#128](https://gitlab.com/hestia-earth/hestia-schema/issues/128)
* **site:** add `pond` as `siteType` ([0539cd1](https://gitlab.com/hestia-earth/hestia-schema/commit/0539cd12a875c8c73af5ad5206688b6624905b84))


### Bug Fixes

* **schema-convert:** add missing schema dependencies ([9217663](https://gitlab.com/hestia-earth/hestia-schema/commit/921766337faf382b56d1d32a867107ea48ddcaa8))

## [6.5.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.4.0...v6.5.0) (2021-08-12)


### Features

* **impactAssessment:** remove `functionalUnitMeasure` and improve docs ([5243e55](https://gitlab.com/hestia-earth/hestia-schema/commit/5243e55bfd71be2a21163bfc56851f7116b8a90e)), closes [#124](https://gitlab.com/hestia-earth/hestia-schema/issues/124)
* **site:** update `siteType` enum ([cb47ac1](https://gitlab.com/hestia-earth/hestia-schema/commit/cb47ac12865c7e958ef88c703d44959113f91c7e))


### Bug Fixes

* **input:** add `transport` to `termType` validators ([07b962f](https://gitlab.com/hestia-earth/hestia-schema/commit/07b962fbf0a5f3f2e8fc44241ae3d555109f110c))

## [6.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.3.0...v6.4.0) (2021-08-11)


### Features

* **property:** change validator to require `value`, `min`, or `max` ([60430ba](https://gitlab.com/hestia-earth/hestia-schema/commit/60430baf474f7f17f4da1be0d962d355eb6b2b5e))

## [6.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.2.0...v6.3.0) (2021-08-05)


### Features

* **convert:** add conver from csv to json bin ([6f73f1c](https://gitlab.com/hestia-earth/hestia-schema/commit/6f73f1c70820a1dc4ae971b2368c4333504cd598))
* **schema convert:** move bin `hestia-convert-to-csv` from utils package ([dca4183](https://gitlab.com/hestia-earth/hestia-schema/commit/dca41839337325f8a3e756344fc71d8377869049))

## [6.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.1.0...v6.2.0) (2021-07-30)


### Features

* **property:** make `value` required ([a3d703a](https://gitlab.com/hestia-earth/hestia-schema/commit/a3d703a18774b96b93a72e6e2c35104a91b8e0bd))

## [6.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v6.0.0...v6.1.0) (2021-07-28)


### Features

* add `time` as option for `statsDefinition` ([9d519cb](https://gitlab.com/hestia-earth/hestia-schema/commit/9d519cbe3394314e77df6cb92d1d9b8d6a7960ca))
* **cycle:** add `dates` for unicity on `practices` ([518f4cd](https://gitlab.com/hestia-earth/hestia-schema/commit/518f4cd7c9cc212883f181d7b635d40096db2a5c))
* **measurement:** add fields for `latitude` and `longitude` ([bb6577f](https://gitlab.com/hestia-earth/hestia-schema/commit/bb6577fffa458f4704c55514c83f84b1f136d83a))
* **measurement:** allow any `termType` when `value` / stats is specified ([17613a9](https://gitlab.com/hestia-earth/hestia-schema/commit/17613a9689ced996e94e475ea08bd0726e155c23))

## [6.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v5.3.0...v6.0.0) (2021-07-23)


### ⚠ BREAKING CHANGES

* **practice:** Practice `value`, `sd`, `min`, `max` and `observations` are now an array

### Features

* **practice:** add `dates` field ([1796abb](https://gitlab.com/hestia-earth/hestia-schema/commit/1796abbd5677a80d3ad9f9517340308a8bc2a122))
* **practice:** set `value`, `sd`, `min`, `max` and `observations` to array ([c9ff3f3](https://gitlab.com/hestia-earth/hestia-schema/commit/c9ff3f3cdbb2785a2bd90672c51e032547f80073))

## [5.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v5.2.0...v5.3.0) (2021-07-22)


### Features

* **completeness:** merge products and coProducts fields ([84eedec](https://gitlab.com/hestia-earth/hestia-schema/commit/84eedec124ae5fb3f42791752f4a4344be4d473f)), closes [#117](https://gitlab.com/hestia-earth/hestia-schema/issues/117)
* **cycle:** rename `functionalUnitMeasure` `funtionalUnit` ([72154bd](https://gitlab.com/hestia-earth/hestia-schema/commit/72154bde2da37b11f7067ec41e6ad3946886e070)), closes [#108](https://gitlab.com/hestia-earth/hestia-schema/issues/108)

## [5.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v5.1.0...v5.2.0) (2021-07-17)


### Features

* add `aggregated` flag on aggregation engine nodes ([96dddca](https://gitlab.com/hestia-earth/hestia-schema/commit/96dddca5a084f26e98902ad8a955fa9f539d6493))
* make the node `schemaVersion` seachable ([a5c2cc9](https://gitlab.com/hestia-earth/hestia-schema/commit/a5c2cc90306983a00cb93944f0981a51e5631695))

## [5.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v5.0.3...v5.1.0) (2021-07-16)


### Features

* **cycle:** make `defaultSource` searchable ([d6405b4](https://gitlab.com/hestia-earth/hestia-schema/commit/d6405b473daea2e18e9dde87be0767787e1481bb))
* update date regex and improve documentation ([1c5b1d7](https://gitlab.com/hestia-earth/hestia-schema/commit/1c5b1d7f54c71f32bbebfdec32e84bbbc2c34364)), closes [#116](https://gitlab.com/hestia-earth/hestia-schema/issues/116)

### [5.0.3](https://gitlab.com/hestia-earth/hestia-schema/compare/v5.0.2...v5.0.3) (2021-07-15)


### Bug Fixes

* **json-schema:** move `refToSchemaType` to `schema` ([57efefb](https://gitlab.com/hestia-earth/hestia-schema/commit/57efefbb8953256ae8d39d4348d467ce3007e2a7))

### [5.0.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v5.0.1...v5.0.2) (2021-07-15)


### Bug Fixes

* **json-schema:** add missing export `schema-utils` ([d7fb724](https://gitlab.com/hestia-earth/hestia-schema/commit/d7fb72436b40c7c9203da2653379f4e53411e59d))

### [5.0.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v5.0.0...v5.0.1) (2021-07-15)


### Bug Fixes

* **json-schema:** move `isDefaultCSVSelected` to `schema-utils` file ([4de1f02](https://gitlab.com/hestia-earth/hestia-schema/commit/4de1f028d8b647c673a22b3d15164e9af4a24579))

## [5.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v4.7.0...v5.0.0) (2021-07-15)


### ⚠ BREAKING CHANGES

* **product:** use `product.dates` instead of `product.date`

### Features

* **json schema:** add method `isDefaultCSVSelected` ([d925830](https://gitlab.com/hestia-earth/hestia-schema/commit/d9258300ac3e4c30f7a556794763ea58a5729148))
* **schema validation:** validate duplicated blank nodes ([60cbec5](https://gitlab.com/hestia-earth/hestia-schema/commit/60cbec53ef6e74df10c6f01cd55295da53513b79))


### Bug Fixes

* **product:** rename `date` to `dates` for consistency ([0f0dca3](https://gitlab.com/hestia-earth/hestia-schema/commit/0f0dca3bc5f8ffc7a77b249b31abb25ec2cd64fd))

## [4.7.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v4.6.0...v4.7.0) (2021-07-14)


### Features

* **cycle:** add validation for second+ transformation ([5040217](https://gitlab.com/hestia-earth/hestia-schema/commit/5040217c50d6a12b2a4a4d4fae78d5757d5d8e76))
* **measurement:** add `methodMeasurement` to `methodModel` validator ([7705b9e](https://gitlab.com/hestia-earth/hestia-schema/commit/7705b9ef5acf271b66fc4c5eb0a582b98b99234d))

## [4.6.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v4.5.2...v4.6.0) (2021-07-09)


### Features

* add `aggregated`/`aggregatedVersion` on additional blank nodes ([0af2003](https://gitlab.com/hestia-earth/hestia-schema/commit/0af2003d31b7a8e70675f6d5f286a920c59738d3))

### [4.5.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v4.5.1...v4.5.2) (2021-07-08)


### Bug Fixes

* **cycle:** remove override `practices.termType` restriction ([5d3b43c](https://gitlab.com/hestia-earth/hestia-schema/commit/5d3b43cb9ef378a0c0507c8374614cb9fca6b315))

### [4.5.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v4.5.0...v4.5.1) (2021-07-07)


### Bug Fixes

* fix validation on `methodModel` possible `termType` ([9eb3c1f](https://gitlab.com/hestia-earth/hestia-schema/commit/9eb3c1f69c310f5b2b4711523f68605d72f3c4f2))
* remove `methodEmissionResourceUse` as `methodModel` unless Emission/Indicator ([5f50c99](https://gitlab.com/hestia-earth/hestia-schema/commit/5f50c998a5b558598c6ef7282567a406e67f7237))
* **sort:** fix sort headers with key as number ([c3ace9e](https://gitlab.com/hestia-earth/hestia-schema/commit/c3ace9e0be60d84a0133a95cd75148cf4c3b8a2b))

## [4.5.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v4.4.1...v4.5.0) (2021-07-02)


### Features

* **infrastructure:** require at least `term`, `name`, or `description` ([2be0db9](https://gitlab.com/hestia-earth/hestia-schema/commit/2be0db974e454bdd7dfc4aff4f3abfb824c862e0))
* **organisation:** add `description` field ([b3fda59](https://gitlab.com/hestia-earth/hestia-schema/commit/b3fda592597d81c76991824fa80ad7b57966115d))
* **site:** update `siteType` for water based systems ([8edd3d9](https://gitlab.com/hestia-earth/hestia-schema/commit/8edd3d9578f8f95caa892ecb33c3afb12585101c))


### Bug Fixes

* **property:** set `term` to `required` ([1d7619a](https://gitlab.com/hestia-earth/hestia-schema/commit/1d7619a5052a78b05991ed90115f06aa4882e34f)), closes [#111](https://gitlab.com/hestia-earth/hestia-schema/issues/111)

### [4.4.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v4.4.0...v4.4.1) (2021-07-01)


### Bug Fixes

* **cycle:** set `siteDuration` as a single number ([640cdd5](https://gitlab.com/hestia-earth/hestia-schema/commit/640cdd5cb8e674301ae5f685a52e0d42501954a6))
* **product:** add `other` to product `termType` validator ([28c0f4a](https://gitlab.com/hestia-earth/hestia-schema/commit/28c0f4ade38dc3c8cf7f9e422c34b7e21b3de237))

## [4.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v4.3.0...v4.4.0) (2021-06-30)


### Features

* **term:** make `subClassOf` searchable ([9897e2b](https://gitlab.com/hestia-earth/hestia-schema/commit/9897e2b2dbc02c5a9ec58c1d321d2964e6fd6afe))

## [4.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v4.2.0...v4.3.0) (2021-06-29)


### Features

* **cycle:** enable a single `Cycle` to have multiple `Site` ([39fb67f](https://gitlab.com/hestia-earth/hestia-schema/commit/39fb67f977bcaac7bd08307d7a4b74aaf14bc9bc))
* **infrastructure:** add `ownershipStatus` field ([cc5a665](https://gitlab.com/hestia-earth/hestia-schema/commit/cc5a66536fcca5473353ea22f593c0147c56e423))
* **practice:** add `ownershipStatus` field ([09682fc](https://gitlab.com/hestia-earth/hestia-schema/commit/09682fcc1927c0701632f90852e4ecb2276afd0b))
* **practice:** add `price`, `cost`, and `currency` ([60f9b00](https://gitlab.com/hestia-earth/hestia-schema/commit/60f9b00d4eb32c6d805f246dd9995ed532d041a7))


### Bug Fixes

* **practice:** add `operation` `termType` to `term` validation ([fb3830e](https://gitlab.com/hestia-earth/hestia-schema/commit/fb3830eb2c5edeac480f49211505a3129300b833))

## [4.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v4.1.0...v4.2.0) (2021-06-17)


### Features

* add minimum validation to number/integer ([3ba8de8](https://gitlab.com/hestia-earth/hestia-schema/commit/3ba8de85c29d8bc32379b7d3932523b9f45e687b))
* require `statsDefinition` if descriptive statistics ([14e8c13](https://gitlab.com/hestia-earth/hestia-schema/commit/14e8c132d8d24884ac3e6bf9bb411ac78cf0779b)), closes [#103](https://gitlab.com/hestia-earth/hestia-schema/issues/103)
* **practice:** add field for `key` ([2e309ec](https://gitlab.com/hestia-earth/hestia-schema/commit/2e309eca46b1315e1dbd4f4b374d8cb4bb35e41b)), closes [#105](https://gitlab.com/hestia-earth/hestia-schema/issues/105)

## [4.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v4.0.1...v4.1.0) (2021-06-04)


### Features

* **property:** set `statsDefinition` as an `enum` ([0922504](https://gitlab.com/hestia-earth/hestia-schema/commit/09225042cf55c31ad8ecdce162f50d9566346c03))

### [4.0.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v4.0.0...v4.0.1) (2021-06-03)


### Bug Fixes

* **practice:** fix validation required at least `term` or `description` ([e3219f6](https://gitlab.com/hestia-earth/hestia-schema/commit/e3219f6d2af4e504f29ed8471983dbe72aab7256))

## [4.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v3.9.0...v4.0.0) (2021-06-02)


### ⚠ BREAKING CHANGES

* **site:** `site.siteType = building` as been replaced by `animal housing` and `factory`

### Features

* export nested searchable keys ([759d8d6](https://gitlab.com/hestia-earth/hestia-schema/commit/759d8d630d2a008e100a2c58f812c87b4b867353))


### Bug Fixes

* **site:** replace `building` as `siteType` with `animal housing` and `factory` ([34142b1](https://gitlab.com/hestia-earth/hestia-schema/commit/34142b13ab51a3b964f0baabbc69eeaa64a5aa3b))

## [3.9.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v3.8.0...v3.9.0) (2021-06-02)


### Features

* add `otherObservations` option to `statsDefinition` field ([e223662](https://gitlab.com/hestia-earth/hestia-schema/commit/e223662d4e95c74deed949db30ade55f1abb93e4))
* **impactAssessment:** add `excreta` `termType` to product validator ([fcb9afc](https://gitlab.com/hestia-earth/hestia-schema/commit/fcb9afca6593770b499d02287699e0fab690b5e4))

## [3.8.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v3.7.0...v3.8.0) (2021-05-29)


### Features

* **transformation:** add `previousTransformationShare` field ([fb01f9e](https://gitlab.com/hestia-earth/hestia-schema/commit/fb01f9ed0bc3eff864b54d86c3c490a050fda6c8))
* **transformation:** allow `operation` as a `term` ([edc3160](https://gitlab.com/hestia-earth/hestia-schema/commit/edc31605d4c9747aa47e19b37f0e7da5da7db7f3))

## [3.7.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v3.6.0...v3.7.0) (2021-05-25)


### Features

* **completeness:** add `animalFeed` field ([18ff5a9](https://gitlab.com/hestia-earth/hestia-schema/commit/18ff5a941c44503b38cd8b343c8863fe4a06eb64))
* **practice:** require either a `term` or `description` ([3530ea4](https://gitlab.com/hestia-earth/hestia-schema/commit/3530ea46aacd048d271830d3df5e3cd132665ac1))

## [3.6.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v3.5.0...v3.6.0) (2021-05-20)


### Features

* **impact assessment:** set `methodModel` as required for `impacts` ([44bb384](https://gitlab.com/hestia-earth/hestia-schema/commit/44bb384d21127283d16376227d39b50c8d80a57f))
* **measurement:** add `properties` field ([010ff38](https://gitlab.com/hestia-earth/hestia-schema/commit/010ff38963a68fe0f04c7d8c6a83bd0b0c41ac3c))
* **term:** set `openLCAId` as `unique` and `searchable` ([d0e7a0c](https://gitlab.com/hestia-earth/hestia-schema/commit/d0e7a0c957da6423a7b5c56c685c0926795fd691))

## [3.5.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v3.4.0...v3.5.0) (2021-05-13)


### Features

* **input:** add `operation` as a `Term` ([6232498](https://gitlab.com/hestia-earth/hestia-schema/commit/6232498871aef3b5f5835cb1c88dbfc23dd97b69))

## [3.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v3.3.0...v3.4.0) (2021-05-13)


### Features

* **term:** add `operation` as a `termType` ([bb6ac4f](https://gitlab.com/hestia-earth/hestia-schema/commit/bb6ac4f9a18c9bdff417c0f17975ce7a528961b1))

## [3.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v3.2.0...v3.3.0) (2021-05-07)


### Features

* **completeness:** rename `manureManagement` to `excretaManagement` ([c9997a7](https://gitlab.com/hestia-earth/hestia-schema/commit/c9997a70d9a97abf9a4666ebd04aea635be1dc91))
* **cycle:** restrict `practices` to specific `term.termType` ([6a1e8f3](https://gitlab.com/hestia-earth/hestia-schema/commit/6a1e8f366b6edb72f98c0543fa4a247d38cd4e97))
* **schema:** handle override default sort configuration ([ee5b7cf](https://gitlab.com/hestia-earth/hestia-schema/commit/ee5b7cf670c9592a51cfda86b34b97ec0dbaf9ee))
* **site:** restrict `practices` to specific `term.termType` ([c97ed99](https://gitlab.com/hestia-earth/hestia-schema/commit/c97ed99d10b1c1582c28da0310c207a2c8418a43))
* **term:** add field for `openLCAId` ([12ac2af](https://gitlab.com/hestia-earth/hestia-schema/commit/12ac2af237b6b5939440f84f09f1927b6c526435))


### Bug Fixes

* **schema:** export `sortConfig` type ([2ab13f5](https://gitlab.com/hestia-earth/hestia-schema/commit/2ab13f525e37e32ad4c7bfff1fc6084c8427e018))
* **schema:** handle old schema version for sortingr ([16ce1d4](https://gitlab.com/hestia-earth/hestia-schema/commit/16ce1d4cfe6f79e9094c3cf99f21fd5773694a2d))

## [3.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v3.1.0...v3.2.0) (2021-05-03)


### Features

* **transformation:** create blank node ([6e15dd7](https://gitlab.com/hestia-earth/hestia-schema/commit/6e15dd79995507ff732f2afbbe903adcbf6633c0))

## [3.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v3.0.0...v3.1.0) (2021-04-27)


### Features

* **emission:** set `methodModel` to `required` ([e0ce64c](https://gitlab.com/hestia-earth/hestia-schema/commit/e0ce64c8a35c90fd70b590b54841bcedb51afc46))


### Bug Fixes

* remove `aggregated` and `aggregatedVersion` fields ([c64715a](https://gitlab.com/hestia-earth/hestia-schema/commit/c64715a0c38563ce59d4e900d6d972c569b58aec))

## [3.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.16.0...v3.0.0) (2021-04-16)


### ⚠ BREAKING CHANGES

* `date` was renamed to `dates` on every Blank Node
* `gapFilled` and `recalculated` fields are merged in `added` and `updated`.

### Features

* add `modelled` option to `statsDefinition` ([2264549](https://gitlab.com/hestia-earth/hestia-schema/commit/2264549b40d3ecfb2680230850ccae49b6ea5785))
* replace `gapFilled` and `recalculated` with `added` and `updated` ([3c4d3eb](https://gitlab.com/hestia-earth/hestia-schema/commit/3c4d3eb272109bc00c139aae2d4de9cf9210f20b))
* **blank nodes:** add or rename fields `methodModel` and `methodModelDescription` ([c0d9832](https://gitlab.com/hestia-earth/hestia-schema/commit/c0d98329551739d379652aa3d7d9e90c27fbd8dc))


### Bug Fixes

* change `date` to `dates` on Blank Nodes ([d5f61ae](https://gitlab.com/hestia-earth/hestia-schema/commit/d5f61aef697a496675c857c7f38417e1a8b5dfe9))

## [2.16.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.15.0...v2.16.0) (2021-04-08)


### Features

* **indicator:** add further `statsDefinition` options ([f7628d8](https://gitlab.com/hestia-earth/hestia-schema/commit/f7628d81f778e98c16e6d4a7987bb74463fedc16))

## [2.15.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.14.0...v2.15.0) (2021-04-07)


### Features

* **impact assessment:** index `emissionsResourceUse` and `impacts` ([ff6c45c](https://gitlab.com/hestia-earth/hestia-schema/commit/ff6c45cd75588c43ad8ae77f062aeb077d599470))

## [2.14.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.13.1...v2.14.0) (2021-03-31)


### Features

* add `spatial` and `regions` options to `statsDefinition` ([36cfbdb](https://gitlab.com/hestia-earth/hestia-schema/commit/36cfbdb3af0496646e5f361b7a6d3bb2fb9d323d))
* **impact assessment:** add `organic` and `irrigated` fields ([f3c4d35](https://gitlab.com/hestia-earth/hestia-schema/commit/f3c4d3597562c5553633c500e73299c781df78a2))
* **impact assessment:** allow `characterisedIndicator` terms in `emissionsResourceUse` ([f7d30fb](https://gitlab.com/hestia-earth/hestia-schema/commit/f7d30fbf4334c729a45a028b8e0c66eb6bbd63a4))
* **indicator:** add `distribution` field ([d618a98](https://gitlab.com/hestia-earth/hestia-schema/commit/d618a980437ce1ce6918707dd9a5903c7e8b1851))

### [2.13.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.13.0...v2.13.1) (2021-03-26)


### Bug Fixes

* **schema validation:** fix conditional validation on arrays items ([9f473c2](https://gitlab.com/hestia-earth/hestia-schema/commit/9f473c238e0a462138edee0403e58460f9f3c126))

## [2.13.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.12.0...v2.13.0) (2021-03-17)


### Features

* **impactAssessment:** allow `kWh` functional unit ([59889f5](https://gitlab.com/hestia-earth/hestia-schema/commit/59889f545a44e28ca006471f4050f69d4aec8186))

## [2.12.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.11.0...v2.12.0) (2021-03-16)


### Features

* add `description` field to Infrastructure and Measurement ([edb59aa](https://gitlab.com/hestia-earth/hestia-schema/commit/edb59aa764325e4db36b7a2078ff1acdb47b8882))
* **emission:** add `depth` field for soil emissions ([3b5c89e](https://gitlab.com/hestia-earth/hestia-schema/commit/3b5c89eee0ef0207415e0de96edd21c63b4f9715))
* **infrastructure:** add `impactAssessment` term ([d791dc9](https://gitlab.com/hestia-earth/hestia-schema/commit/d791dc95925f515cc0a63f402299722e7fa148ea))
* **json-schema:** handle `pattern` on arrays ([da8a7be](https://gitlab.com/hestia-earth/hestia-schema/commit/da8a7be0034ffb2aa77bb2d5f503583503e5d736))
* **property:** add `description` field ([b03f696](https://gitlab.com/hestia-earth/hestia-schema/commit/b03f696c16289fbc0723aae88355ae71f5da7f28))


### Bug Fixes

* **schema:** handle sorting between different node types ([a2dc47a](https://gitlab.com/hestia-earth/hestia-schema/commit/a2dc47af1b6671261650bca9526e810f28446b97))

## [2.11.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.10.0...v2.11.0) (2021-03-11)


### Features

* **schema:** add method to sort node keys by type ([57cbba6](https://gitlab.com/hestia-earth/hestia-schema/commit/57cbba6a8b3ca1c28ebca518c01bba773fdb4be5))

## [2.10.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.9.0...v2.10.0) (2021-03-08)


### Features

* add `schemaVersion` to every jsonld created ([a4a6d68](https://gitlab.com/hestia-earth/hestia-schema/commit/a4a6d687aa077141605674792089eee0bbd2493d))
* replace `relDays` in blank nodes to `date` field ([9a7ac30](https://gitlab.com/hestia-earth/hestia-schema/commit/9a7ac3015732c91a7df00dc15cac5ded8eb1b592)), closes [#80](https://gitlab.com/hestia-earth/hestia-schema/issues/80)
* **bibliography:** add `articlePdf` field ([40d0d4c](https://gitlab.com/hestia-earth/hestia-schema/commit/40d0d4cf10ca4cd854bfe32886dc803ce27ca3f5))
* **blank nodes:** convert `dataState` to lists of fields ([ee7657a](https://gitlab.com/hestia-earth/hestia-schema/commit/ee7657ae42d5f0cefadb03724bac8a75ba698cf3)), closes [#65](https://gitlab.com/hestia-earth/hestia-schema/issues/65)

## [2.9.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.8.0...v2.9.0) (2021-03-02)


### Features

* **property:** remove `valueType` field ([682869e](https://gitlab.com/hestia-earth/hestia-schema/commit/682869e35c4783c3fd4f410369655c46e2f595ca)), closes [#71](https://gitlab.com/hestia-earth/hestia-schema/issues/71)
* **site:** add awareWaterBasinId field ([16c86dd](https://gitlab.com/hestia-earth/hestia-schema/commit/16c86dd412a62391f82e7766bf9378470a0a43a1))
* **term:** make `gadmName` searchable ([dcf4cf7](https://gitlab.com/hestia-earth/hestia-schema/commit/dcf4cf790ea9f7627699ef6d4a698e01bd9f8c46))
* **term:** remove `termType=ecoregion` and `ecoregionCode` field ([a88e32f](https://gitlab.com/hestia-earth/hestia-schema/commit/a88e32f9f53ff968d54fb9bcf112e2d39e740522)), closes [#78](https://gitlab.com/hestia-earth/hestia-schema/issues/78)

## [2.8.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.7.0...v2.8.0) (2021-02-27)


### Features

* **impactAssessment:** add `startDate` ([57decda](https://gitlab.com/hestia-earth/hestia-schema/commit/57decda651b0810e6cd5e01f8ae1baf33a681e96))


### Bug Fixes

* **bibliography:** remove `documentType` field ([2242074](https://gitlab.com/hestia-earth/hestia-schema/commit/2242074e6fc33fd35ebe1cf027e3ffac03e12aba))
* **cycle:** set `cycleDuration` as not required ([2746a40](https://gitlab.com/hestia-earth/hestia-schema/commit/2746a407e8798f4baa2710b9cf252602fc28654c)), closes [#68](https://gitlab.com/hestia-earth/hestia-schema/issues/68)

## [2.7.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.6.1...v2.7.0) (2021-02-25)


### Features

* **cycle:** increase upload size limit to `2000` ([853fbeb](https://gitlab.com/hestia-earth/hestia-schema/commit/853fbebcc64efd945dffb88e40abb83afcadd3b0))
* **term:** add `gadmCountry` ([04d75e7](https://gitlab.com/hestia-earth/hestia-schema/commit/04d75e7e3a1c62900db0b161f0437c2bf0874b17))

### [2.6.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.6.0...v2.6.1) (2021-02-18)


### Bug Fixes

* **term:** fix pattern for `gadmId` ([d34fb00](https://gitlab.com/hestia-earth/hestia-schema/commit/d34fb00a4f6dcdfb42b2130b9ba85d3e3bacb004))

## [2.6.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.5.0...v2.6.0) (2021-02-18)


### Features

* add new `termTypes` `antibiotic` and `cropResidue` ([7986161](https://gitlab.com/hestia-earth/hestia-schema/commit/79861615f642fe714cea3bdbf586abf4a8442493)), closes [#72](https://gitlab.com/hestia-earth/hestia-schema/issues/72)
* **completeness:** link fields to termTypes ([abf8b2e](https://gitlab.com/hestia-earth/hestia-schema/commit/abf8b2ee75379bf0fd0643f88e09d347aad355b6))
* **cycle:** set `cycleDuration` to `required` with `default` = `365` ([f81a9d2](https://gitlab.com/hestia-earth/hestia-schema/commit/f81a9d269f19f7d0a4ff986e4d85616ab1a7d9c4)), closes [#68](https://gitlab.com/hestia-earth/hestia-schema/issues/68)
* **property:** index `key` ([eaeb1ba](https://gitlab.com/hestia-earth/hestia-schema/commit/eaeb1ba7aa1559157a91aa6e157abab76ae5996e))
* **property:** index `value` ([9e026f8](https://gitlab.com/hestia-earth/hestia-schema/commit/9e026f860133f27352ad515fe62b6f1ff86de697))


### Bug Fixes

* **property:** remove `string` type for automatic value type detection ([fd0598d](https://gitlab.com/hestia-earth/hestia-schema/commit/fd0598d03e6c78f6d22baa2cf81109ee409f4e26))

## [2.5.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.4.0...v2.5.0) (2021-02-17)


### Features

* **term:** set `defaultProperties` as `searchable` ([cd45b97](https://gitlab.com/hestia-earth/hestia-schema/commit/cd45b973c5063a2545f98edbbee5547e67b03cb9))


### Bug Fixes

* **term:** fix pattern for `casNumber` ([2e3bbcd](https://gitlab.com/hestia-earth/hestia-schema/commit/2e3bbcd74d3f47f4f23336f30290ab0421a92f6f))
* **term:** remove regex on `ecoregionCode` ([c0f6b9a](https://gitlab.com/hestia-earth/hestia-schema/commit/c0f6b9ad3944560e697a797685f187bb5d41b259))

## [2.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.3.0...v2.4.0) (2021-02-17)


### Features

* **completeness:** add dataState and dataVersion ([a9130e6](https://gitlab.com/hestia-earth/hestia-schema/commit/a9130e64e5cd11d3408b68a9f0aad3b745bc434f))
* **cycle:** add `dataDescription` field ([bad777d](https://gitlab.com/hestia-earth/hestia-schema/commit/bad777deac2fb1e1f437afe2af48fb26ce7d800f)), closes [#70](https://gitlab.com/hestia-earth/hestia-schema/issues/70)
* **term:** add `latitude` and `longitude` ([3bfa03b](https://gitlab.com/hestia-earth/hestia-schema/commit/3bfa03be00082c7d452e1bf5fca50ee836cb01b1))
* **term:** add `termType` validation on fields ([49d4c87](https://gitlab.com/hestia-earth/hestia-schema/commit/49d4c871be2218b09a79740ac86368076c863de6))

## [2.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.2.0...v2.3.0) (2021-02-17)


### Features

* **site:** index latitude / longitude ([a4165ae](https://gitlab.com/hestia-earth/hestia-schema/commit/a4165ae97cc3999f29e7a319301b4da10333c333))

## [2.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.1.0...v2.2.0) (2021-02-15)


### Features

* **emission:** set `value` as `searchable` ([39940d9](https://gitlab.com/hestia-earth/hestia-schema/commit/39940d96db1f47861afcce312973d50e72abf1c1))
* **product:** add variety as field ([3ec4293](https://gitlab.com/hestia-earth/hestia-schema/commit/3ec4293306e3c03adfc4310d90ace109f3397ab5)), closes [#62](https://gitlab.com/hestia-earth/hestia-schema/issues/62)
* **property:** remove redundant fields and description ([4ef6b96](https://gitlab.com/hestia-earth/hestia-schema/commit/4ef6b965451313c586aaa6ea6f97836383e9f42b)), closes [#62](https://gitlab.com/hestia-earth/hestia-schema/issues/62)
* **term:** move properties to fields ([11f932c](https://gitlab.com/hestia-earth/hestia-schema/commit/11f932cc84ff55c87eaa00c666067be80aa06936)), closes [#62](https://gitlab.com/hestia-earth/hestia-schema/issues/62)

## [2.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v2.0.0...v2.1.0) (2021-02-11)


### Features

* **schema-validation:** export function `initAjv` ([dfdb0a0](https://gitlab.com/hestia-earth/hestia-schema/commit/dfdb0a03673d7602f2c5ab26645fc65947661fc4))
* **source:** set max upload limit to `200` ([384f5a6](https://gitlab.com/hestia-earth/hestia-schema/commit/384f5a68d840491d7bd88ba2c1417aa22aec23af))


### Bug Fixes

* **schema:** remove unused `jsonId` function ([5f13fc7](https://gitlab.com/hestia-earth/hestia-schema/commit/5f13fc704bac2a0d8690636c53c51f4c7101d9e5))

## [2.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v1.2.2...v2.0.0) (2021-02-09)


### ⚠ BREAKING CHANGES

* **term:** `Term.termType == 'method'` has been replaced
* install `@hestia-earth/json-schema` to load the schemas

### Features

* **term:** split `method` `termType` into methods for emissions and methods for measurements ([a6c6baa](https://gitlab.com/hestia-earth/hestia-schema/commit/a6c6baa57e12d19fc44d14fc1e7fee506ed5cb7e))


* split json-schema into its own module ([b0ca606](https://gitlab.com/hestia-earth/hestia-schema/commit/b0ca606db0a8b6b5247b4d19c1d53e7bb551e874))

### [1.2.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v1.2.1...v1.2.2) (2021-02-06)

### [1.2.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v1.2.0...v1.2.1) (2021-02-06)

## [1.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v1.1.0...v1.2.0) (2021-02-06)


### Features

* add number of observations field for descriptive statistics ([c4411e3](https://gitlab.com/hestia-earth/hestia-schema/commit/c4411e308aa2bc661fba28c4252be96e49cc8c26)), closes [#58](https://gitlab.com/hestia-earth/hestia-schema/issues/58)
* change descriptive statistics fields to type array ([9352c10](https://gitlab.com/hestia-earth/hestia-schema/commit/9352c10f4dd04b4e31657ef0a7ed81f760120e5e)), closes [#67](https://gitlab.com/hestia-earth/hestia-schema/issues/67)
* **impact assessment:** add `autoGenerated` field ([cb3a760](https://gitlab.com/hestia-earth/hestia-schema/commit/cb3a7605582c3798ad6d0e962863b9c39c33d88d))

## [1.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v1.0.1...v1.1.0) (2021-02-05)


### Features

* **site:** add standard deviation of area field ([eafb664](https://gitlab.com/hestia-earth/hestia-schema/commit/eafb66427995758fc987341b6a3f57e5c8545fdc)), closes [#57](https://gitlab.com/hestia-earth/hestia-schema/issues/57)


### Bug Fixes

* **actor:** remove `required` on `email` ([cd63803](https://gitlab.com/hestia-earth/hestia-schema/commit/cd63803ea900a0be08bb2e3ace7495857658713e))
* **emission:** allow `transport` in `inputs` ([60e4ab5](https://gitlab.com/hestia-earth/hestia-schema/commit/60e4ab51c53611f9e1b3e6c7c6ccd75e75e0caa7))

### [1.0.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v1.0.0...v1.0.1) (2021-02-05)


### Bug Fixes

* set properties to extend ([9aad40c](https://gitlab.com/hestia-earth/hestia-schema/commit/9aad40c835ffa5cf752d40d86baa68445fd320e9))

## [1.0.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.6.5...v1.0.0) (2021-02-01)


### Bug Fixes

* **navbar:** fix alignment ([df56c05](https://gitlab.com/hestia-earth/hestia-schema/commit/df56c05824a21fa413a9fb00069295a056026767))

### [0.6.5](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.6.4...v0.6.5) (2021-01-29)


### Features

* **cycle:** add validation between `siteType` and `functionalUnitMeasure` ([82373a8](https://gitlab.com/hestia-earth/hestia-schema/commit/82373a862b3c4e9d36d4ca688ef95d38e081953c)), closes [#63](https://gitlab.com/hestia-earth/hestia-schema/issues/63)
* **organisation:** add `website` property ([55db072](https://gitlab.com/hestia-earth/hestia-schema/commit/55db07205a4adfeff9fed96859d1ecfa14e3b3e2))
* **organisation:** add uploadBy ([52267da](https://gitlab.com/hestia-earth/hestia-schema/commit/52267dafa3cf4c9efe80f3b3ea2d92d7b2f36286)), closes [#60](https://gitlab.com/hestia-earth/hestia-schema/issues/60)
* **source:** set uploadBy to required and simplify description ([e82f7fd](https://gitlab.com/hestia-earth/hestia-schema/commit/e82f7fdeaa887bba7f8f02b3fc0a9bc8da748c0f))

### [0.6.4](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.6.3...v0.6.4) (2021-01-21)


### Features

* **source:** add dataPrivate ([cd9f25a](https://gitlab.com/hestia-earth/hestia-schema/commit/cd9f25a68d352903ec86232db9f4225ca89263cc)), closes [#61](https://gitlab.com/hestia-earth/hestia-schema/issues/61)

### [0.6.3](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.6.2...v0.6.3) (2021-01-20)


### Features

* **term:** set `termType` as `required` ([9592f35](https://gitlab.com/hestia-earth/hestia-schema/commit/9592f3517fb7d40387212abaf318ed2364da6428))


### Bug Fixes

* **term:** remove `internal` on `termType` ([b5aba9d](https://gitlab.com/hestia-earth/hestia-schema/commit/b5aba9d9f001fc9718964d0644042f011171e870))

### [0.6.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.6.1...v0.6.2) (2021-01-19)


### Features

* **json-schema:** export `definitions` type ([c28e330](https://gitlab.com/hestia-earth/hestia-schema/commit/c28e330b8dee86a884f70017a6cb88e2398989f7))


### Bug Fixes

* **source:** set `bibliography` to not `extend` ([d337a46](https://gitlab.com/hestia-earth/hestia-schema/commit/d337a46f63f320ddd1eed987bf7a5368557a29bc))

### [0.6.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.6.0...v0.6.1) (2021-01-13)


### Features

* do not `extend` `dataPrivate` property ([f59c2c7](https://gitlab.com/hestia-earth/hestia-schema/commit/f59c2c7296c5e3e108cc861fa0e415d3ae34e1a8))
* **actor:** skip extend unused properties ([a3d377e](https://gitlab.com/hestia-earth/hestia-schema/commit/a3d377e92f546ce5814dc5535026d49ea8e456fa))
* **measurement:** restrict use of `termType` for textures ([fbcd6e5](https://gitlab.com/hestia-earth/hestia-schema/commit/fbcd6e50fb509a73a4267e190dd7947e1fc6c2a3))


### Bug Fixes

* **term:** set `termType` as `searchable` ([c8d12a8](https://gitlab.com/hestia-earth/hestia-schema/commit/c8d12a8f41a00bc5bf33abb6d56796388161026c))
* **term:** set `termType` to be extended ([3641a3b](https://gitlab.com/hestia-earth/hestia-schema/commit/3641a3bb08c283cda0e8e3ba235e8c6d1242e88f))

## [0.6.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.5.0...v0.6.0) (2021-01-12)


### ⚠ BREAKING CHANGES

* **term:** `product.destination` no longer exists, along with `term.termType == 'destination'`
* **measurement:** `measurement.value` is now an `array` of `number`

### Features

* **measurement:** change value to array ([6cfba2f](https://gitlab.com/hestia-earth/hestia-schema/commit/6cfba2f7b7b024aee06efb21f9e43a1e2637d565))


### Bug Fixes

* **term:** remove `destination` as a `termType` ([deb8835](https://gitlab.com/hestia-earth/hestia-schema/commit/deb88354e6c521fe620730b898ac44b7d649e43e))

## [0.5.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.8...v0.5.0) (2021-01-06)


### ⚠ BREAKING CHANGES

* **organisation:** `organisation.addressRegion` has been renamed to `organisation.region`

### Features

* **organisation:** change `addressRegion` to `region` as a reference to `Term` ([2c1f560](https://gitlab.com/hestia-earth/hestia-schema/commit/2c1f5607b43312e58904ea798a1cf2f44872f7e4))
* **term:** add manureManagement termType ([7d9708f](https://gitlab.com/hestia-earth/hestia-schema/commit/7d9708f7b1518c99a0f1461301dc1b7e2c4fbc10))

### [0.4.8](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.7...v0.4.8) (2020-12-29)


### Bug Fixes

* **schema:** fix `isExpandable` iterating over non-defined schemas ([8fb3e66](https://gitlab.com/hestia-earth/hestia-schema/commit/8fb3e668b8205479ead6ad095efc6da634bd3269))

### [0.4.7](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.6...v0.4.7) (2020-12-27)


### Features

* **cycle:** set `name` to `internal` and add `treatment` field ([67610a3](https://gitlab.com/hestia-earth/hestia-schema/commit/67610a318f9b4937c2a6b783817c21712a748b40)), closes [#51](https://gitlab.com/hestia-earth/hestia-schema/issues/51)

### [0.4.6](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.5...v0.4.6) (2020-12-21)


### Features

* **json schema:** add `integer` as generic type ([d2ea1ef](https://gitlab.com/hestia-earth/hestia-schema/commit/d2ea1ef0e846ecab1135f073f7937ea63199032d))
* **json-schema:** add const to definition ([21651a5](https://gitlab.com/hestia-earth/hestia-schema/commit/21651a58e3ab712e5262f126f66b8ee7d1bd6bfc))
* **product:** add pattern validation for ISO currency code ([20313d7](https://gitlab.com/hestia-earth/hestia-schema/commit/20313d70f3b42434b0fb5d984db43755d3862275))


### Bug Fixes

* **emission:** update format `startDate` and `endDate` ([6f66d4a](https://gitlab.com/hestia-earth/hestia-schema/commit/6f66d4a9cb963ce10206631696752a5360fb82e1))
* **types:** handle Date in `isExpandable` ([d64e274](https://gitlab.com/hestia-earth/hestia-schema/commit/d64e27408c77909078debdbff759899d296bd4ab))

### [0.4.5](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.4...v0.4.5) (2020-12-15)


### Bug Fixes

* remove `enum` on `number` ([78c119a](https://gitlab.com/hestia-earth/hestia-schema/commit/78c119a299dcc36ae01feb119080aa9d2b9c6fb1))

### [0.4.4](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.3...v0.4.4) (2020-12-08)


### Features

* **impact assessment:** add restrictions on list `termType` ([b3ca4fb](https://gitlab.com/hestia-earth/hestia-schema/commit/b3ca4fbc372727aa758fcbfc70a232fde7c51db6)), closes [#48](https://gitlab.com/hestia-earth/hestia-schema/issues/48)
* **term:** search by `units` ([5acc4cb](https://gitlab.com/hestia-earth/hestia-schema/commit/5acc4cb9cc0daa9de54044f2b1d3e291bbce3ab7))

### [0.4.3](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.2...v0.4.3) (2020-12-02)


### Bug Fixes

* **json-schema:** handle default value as false ([18b1993](https://gitlab.com/hestia-earth/hestia-schema/commit/18b19931541a901c200777e112bd8f2c6cf0beb4))

### [0.4.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.1...v0.4.2) (2020-11-29)


### Features

* restrict `termType` on all parent nodes ([018e383](https://gitlab.com/hestia-earth/hestia-schema/commit/018e3830e86a959b27140de38c3f1a6c712bcb6e))

### [0.4.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.4.0...v0.4.1) (2020-11-27)


### Features

* **term:** add `system` to `termType` ([06071aa](https://gitlab.com/hestia-earth/hestia-schema/commit/06071aafcb2feb5015845787b18e9525ce38d8ed))

## [0.4.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.3.3...v0.4.0) (2020-11-26)


### ⚠ BREAKING CHANGES

* **emission:** convert `emission.input` to `emission.inputs` as first element

### Features

* only set properties to not `extend` ([13e47ae](https://gitlab.com/hestia-earth/hestia-schema/commit/13e47aebbacfb1262113a1dec58ce0c05fbd15bf))
* **emission:** change `input` into a list as `inputs` ([99d3288](https://gitlab.com/hestia-earth/hestia-schema/commit/99d32886db0a45ea0e5bce030d4d2a6562279e62))
* **emission:** require `input` when using `methodTier = background` ([a1bd9e0](https://gitlab.com/hestia-earth/hestia-schema/commit/a1bd9e0851c8c249c8da2f7b5a04223ba3f7f0d9)), closes [#45](https://gitlab.com/hestia-earth/hestia-schema/issues/45)

### [0.3.3](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.3.2...v0.3.3) (2020-11-24)


### Bug Fixes

* **json-schema:** add missing `extend` on `[@id](https://gitlab.com/id)` and `[@type](https://gitlab.com/type)` ([dbe534b](https://gitlab.com/hestia-earth/hestia-schema/commit/dbe534b996bbd905753a744d8fba07d441c3e1d8))

### [0.3.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.3.1...v0.3.2) (2020-11-24)


### Features

* add `extend` fields on every node ([c453263](https://gitlab.com/hestia-earth/hestia-schema/commit/c453263188f9c5fef7671746413625411980e622))
* add `glnNumber` field to `Site` and `Organisation` ([11596aa](https://gitlab.com/hestia-earth/hestia-schema/commit/11596aa28b4a1249f6b27d447d81b7a0771911f7))

### [0.3.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.3.0...v0.3.1) (2020-11-15)


### Features

* **organisation:** set `country` as `required` ([74aed55](https://gitlab.com/hestia-earth/hestia-schema/commit/74aed557d717ce90f0004f6da3ce68511e03da98))
* **property:** add descriptive statistics ([f87c518](https://gitlab.com/hestia-earth/hestia-schema/commit/f87c518cd5a269aae907bd32a4a9bba568fb524f))


### Bug Fixes

* **measurement:** set `value` as non `required` ([32616f7](https://gitlab.com/hestia-earth/hestia-schema/commit/32616f74f5ea227e3af6c00a9aed8b9068d41e0b))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.2.0...v0.3.0) (2020-11-13)


### ⚠ BREAKING CHANGES

* **emission:** remove all references to `emission.chararacterisation`
* **term:** update `termType` = `characterisation` to `characterisedIndicator`

### Bug Fixes

* **emission:** delete chararacterisation field ([e8d1890](https://gitlab.com/hestia-earth/hestia-schema/commit/e8d189095305efe9e28ff051bc32ca9e75c2fd75))
* **term:** change characterisation to characterisedIndicator ([25454f6](https://gitlab.com/hestia-earth/hestia-schema/commit/25454f66498e368945b1064641e080fd84630611))

## [0.2.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.9...v0.2.0) (2020-11-11)


### ⚠ BREAKING CHANGES

* **term:** use `animalManagement` instead of `dairyManagement` for termType
* **term:** use `other` instead of `seed` for `termType`
* **impact assessment:** every `Inventory` reference must be moved over to `ImpactAssessment` type
* **inventory:** all `Inventory` references will need migrating to `ImpactAssessment`

### Features

* add `originalId` to every node ([7e3121d](https://gitlab.com/hestia-earth/hestia-schema/commit/7e3121d750c49f6c4ad598478bf125af259979b6))
* add option for descriptive statistics to be `simulated` ([5e0469f](https://gitlab.com/hestia-earth/hestia-schema/commit/5e0469fa3b23d47dc3b7dc846d72ba6954604716))
* set `dataPrivate` to false by default ([0f8459f](https://gitlab.com/hestia-earth/hestia-schema/commit/0f8459f387afdcdd11f4ba7b05cdccbafefbc1dc))
* **emission:** add `input` field ([3fab660](https://gitlab.com/hestia-earth/hestia-schema/commit/3fab660609c67d471ddb2d765b33d9ebe8e99385))
* **impact assessment:** add `country` and `region` as fields ([5122e26](https://gitlab.com/hestia-earth/hestia-schema/commit/5122e2682ec71641cc77b8a921023253c10900b6))
* **impact assessment:** convert from `inventory` ([124e6ab](https://gitlab.com/hestia-earth/hestia-schema/commit/124e6abb61437a0718cf73f3dd20fae51a798b1b))
* **site:** add `not specified` siteType ([e7838a3](https://gitlab.com/hestia-earth/hestia-schema/commit/e7838a3f8d4144616e63c2a933dc51734a9d8a35))
* **source:** add `unique` on `bibliography` and `bibliography.title` ([fbb553b](https://gitlab.com/hestia-earth/hestia-schema/commit/fbb553babe3b8e9de3ec756c4ac90ca27e1eb4ee))
* **term:** add `transport` termType ([ed42d96](https://gitlab.com/hestia-earth/hestia-schema/commit/ed42d962ea45234ac2678326bc5c6745c0121cd3))


### Bug Fixes

* **infrastructure:** change `dataVersion` to `string` ([ead46af](https://gitlab.com/hestia-earth/hestia-schema/commit/ead46af466a437288df1c47a9f079b903f3dea57))
* **site:** fix referenced node for `practices` ([1bd1663](https://gitlab.com/hestia-earth/hestia-schema/commit/1bd1663608156df8ab1aadb26d5cf40d7e1811b5))
* **site:** remove unused `dataState` and `dataVersion` ([1a68815](https://gitlab.com/hestia-earth/hestia-schema/commit/1a688154595f3180ac204ac4cc7d0a78ffa3b014))
* **source:** remove `design` ([29287e7](https://gitlab.com/hestia-earth/hestia-schema/commit/29287e753873707a3ab14b50710a040917aafbfc))
* **term:** change `seed` termType to `other` ([46f533d](https://gitlab.com/hestia-earth/hestia-schema/commit/46f533d1470ce218c8a452c0b061fdbacf53b81d))
* **term:** replace `dairyManagement` with `animalManagement` termType ([ed32ed5](https://gitlab.com/hestia-earth/hestia-schema/commit/ed32ed5c41e9715a55c0a2230953dff34b38d06c))


* **inventory:** change name to `impact assessment` ([3add6a7](https://gitlab.com/hestia-earth/hestia-schema/commit/3add6a7de93fb05df8025789bf878b37904c9f32))

### [0.1.9](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.8...v0.1.9) (2020-11-04)


### Features

* **term:** add `searchable` to `synonyms` ([ce4740a](https://gitlab.com/hestia-earth/hestia-schema/commit/ce4740a5c3ea279eab64fe596fda20b7e8ced12f))

### [0.1.8](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.7...v0.1.8) (2020-11-01)


### Features

* **inventory:** make `cycle`, `product` and `source` searchable ([4efa8fb](https://gitlab.com/hestia-earth/hestia-schema/commit/4efa8fbf0e2f9d184a751e795092d3b38b844f92)), closes [#33](https://gitlab.com/hestia-earth/hestia-schema/issues/33)
* **inventory:** set `source` as `required` ([020489e](https://gitlab.com/hestia-earth/hestia-schema/commit/020489efd5c51b8256c20f837df9ac09b030180a))
* **term:** add `method` to `termType` ([05616b6](https://gitlab.com/hestia-earth/hestia-schema/commit/05616b6f51ca852bc8b07fc23e94477b83e45d6b))

### [0.1.7](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.6...v0.1.7) (2020-10-24)


### Bug Fixes

* **product:** set `value` as not `required` ([a3e4a69](https://gitlab.com/hestia-earth/hestia-schema/commit/a3e4a698c64d05c3d3688db1a7fc9d6c22cc694e))
* **term:** remove `cropProduct` from `termType` ([d1d752f](https://gitlab.com/hestia-earth/hestia-schema/commit/d1d752f6d743b9bf152769f1422a9cfdc603f3cd))
* **term:** remove unused `dependentVariables` and `independentVariables` ([a141ce1](https://gitlab.com/hestia-earth/hestia-schema/commit/a141ce1be5ff7ea6eb978c0a81daf01d685870fd))

### [0.1.6](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.5...v0.1.6) (2020-10-22)


### Features

* **inventory:** add source ([09183a6](https://gitlab.com/hestia-earth/hestia-schema/commit/09183a656c6eed1c13738f9629ea21d3c91ba115))
* **inventory:** clarify terms, add midpoint ([76a6bcf](https://gitlab.com/hestia-earth/hestia-schema/commit/76a6bcf2a49411f6a7f69275fe73d9daf5eb72ed)), closes [#29](https://gitlab.com/hestia-earth/hestia-schema/issues/29)

### [0.1.5](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.4...v0.1.5) (2020-10-17)


### Features

* **product:** add primary product identifier ([3b540ff](https://gitlab.com/hestia-earth/hestia-schema/commit/3b540ff6cd39468efd5700a29a01529e4a486bdc))
* **site:** add ecoregion ([29618d1](https://gitlab.com/hestia-earth/hestia-schema/commit/29618d197d6880027aec6310fad15936d98e77c2)), closes [#30](https://gitlab.com/hestia-earth/hestia-schema/issues/30)

### [0.1.4](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.3...v0.1.4) (2020-09-29)


### Features

* **actor:** set name to internal ([48fa6ff](https://gitlab.com/hestia-earth/hestia-schema/commit/48fa6ffd9f1881a462b42fb6d7a7e85a352c813c))
* **bibliography:** change type of issue to string ([3f4eafd](https://gitlab.com/hestia-earth/hestia-schema/commit/3f4eafd199dad2f40574e4b20e85128f11e2ccc6)), closes [#27](https://gitlab.com/hestia-earth/hestia-schema/issues/27)
* **source:** set metaAnalysisBibliography to searchable ([bc1a963](https://gitlab.com/hestia-earth/hestia-schema/commit/bc1a963d406c0943fac9308001f21d96ef0f8764))
* **validate jsonld:** handle validate file for upload ([a732467](https://gitlab.com/hestia-earth/hestia-schema/commit/a73246774dc73f12a33b99895133d08ed95471f3))


### Bug Fixes

* **excel template:** remove subRegion ([7916991](https://gitlab.com/hestia-earth/hestia-schema/commit/7916991fd90718b63788b337d00fcc4797ce61c3))
* **json-schema:** generate deep version only for top nodes ([f485862](https://gitlab.com/hestia-earth/hestia-schema/commit/f48586246a5b9dcd3f7050790ee4d98f9d4e15f2))
* **source:** set name and bibliography to required ([30a1b73](https://gitlab.com/hestia-earth/hestia-schema/commit/30a1b7306ed6cf6f081cc9af2d0bf3d97ada5142))

### [0.1.3](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.2...v0.1.3) (2020-09-07)


### Features

* **bibliography:** add `name` back ([d400049](https://gitlab.com/hestia-earth/hestia-schema/commit/d400049f730d58160c92e5302c08ad40ca0a758a))
* **cycle:** change `alternativeStartDate` to `altStartDate` ([0fc600c](https://gitlab.com/hestia-earth/hestia-schema/commit/0fc600ce54c54b12f6363ea0c1b36beaf08ea493))
* **excel template:** convert all dates to ISO format ([7ab57f7](https://gitlab.com/hestia-earth/hestia-schema/commit/7ab57f7eb9f6badc088e0b8ae4683afc3eaba1b0))
* **site:** merge subRegions into `region` ([4f807f8](https://gitlab.com/hestia-earth/hestia-schema/commit/4f807f8e99459b90fc70579052b4c298b889beda)), closes [#25](https://gitlab.com/hestia-earth/hestia-schema/issues/25)
* **term:** add `termType` `standardsLabels` ([6f4ac18](https://gitlab.com/hestia-earth/hestia-schema/commit/6f4ac18834bc790b41e5feece5fcf3c352930030))
* **term:** add ecolabel as option ([575e0ae](https://gitlab.com/hestia-earth/hestia-schema/commit/575e0aec572a0abaa2663dc9881bb8059c6fb079))


### Bug Fixes

* make date formats consistent ([fe1d9c3](https://gitlab.com/hestia-earth/hestia-schema/commit/fe1d9c31cea656a05be845df37574bece73d4716)), closes [#26](https://gitlab.com/hestia-earth/hestia-schema/issues/26)
* **bibliography:** set `dateAccessed` as a list of dates ([c9a5644](https://gitlab.com/hestia-earth/hestia-schema/commit/c9a564459ac1f8550dcb6f30d1a70649da475504))
* **measurement:** merge time and date and fix ISO example ([3e9f97d](https://gitlab.com/hestia-earth/hestia-schema/commit/3e9f97d69383fec5d6174ee3b499f3d684e051d0))

### [0.1.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.1...v0.1.2) (2020-09-02)


### Bug Fixes

* replace dataVersions by dataVersion ([b212873](https://gitlab.com/hestia-earth/hestia-schema/commit/b21287374ec5f10f23e2fafb1dfc7e38b158cba0))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.1.0...v0.1.1) (2020-09-01)


### Bug Fixes

* **validate jsonld:** handle .hestia files ([c74e5a8](https://gitlab.com/hestia-earth/hestia-schema/commit/c74e5a83ae12651474d94092caae15e01871c87a))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.29...v0.1.0) (2020-09-01)


### Bug Fixes

* **validate jsonld:** handle both json/jsonld extensions ([fc0c9c8](https://gitlab.com/hestia-earth/hestia-schema/commit/fc0c9c804c5f62f35cd5490c079b8f06fb00fe3a))

### [0.0.29](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.28...v0.0.29) (2020-09-01)


### Features

* change dataVersions to string ([d129823](https://gitlab.com/hestia-earth/hestia-schema/commit/d129823cc5c7b070e9c06e860b1a4a9d75f2de40))
* **bibliography:** add required fields ([3cc26ec](https://gitlab.com/hestia-earth/hestia-schema/commit/3cc26ec7fa905d43475b649727420f56cc77f781))
* **bibliography:** make a blank node ([4a65079](https://gitlab.com/hestia-earth/hestia-schema/commit/4a65079d6097f24c9d7e3bb2bdf3188bf1587482)), closes [#23](https://gitlab.com/hestia-earth/hestia-schema/issues/23)
* **cycle:** make cycle.site mandatory ([39bfc6f](https://gitlab.com/hestia-earth/hestia-schema/commit/39bfc6f1555e2d28b70a87571ede657f718137f6))
* **practice:** add startDate and endDate ([e09fad2](https://gitlab.com/hestia-earth/hestia-schema/commit/e09fad2b465734cc860290a788fa7a4d2d23a82f))
* **term:** add `termType`: `soilTexture` ([082ef81](https://gitlab.com/hestia-earth/hestia-schema/commit/082ef81791babe5149afdc26111174816908407a))


### Bug Fixes

* **bibliography:** remove low importance fields ([389e8b4](https://gitlab.com/hestia-earth/hestia-schema/commit/389e8b4b770a19c7e20c44fc27e814578cc923ea))
* **practice:** make stats definitions consistent ([43e71d1](https://gitlab.com/hestia-earth/hestia-schema/commit/43e71d14579810109d29f163bea943fa4d9e0070))

### [0.0.28](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.27...v0.0.28) (2020-08-31)


### Features

* **actor:** add searchable fields ([da51839](https://gitlab.com/hestia-earth/hestia-schema/commit/da518391f0d13dce7a4265beb3aa1fa56db15259))
* **bibliography:** add more searchable fields ([dbdf974](https://gitlab.com/hestia-earth/hestia-schema/commit/dbdf9744a76a10414e23dbd7e67ac88b0a3c313b))
* **site:** add searchable on siteType and defaultSource ([7883815](https://gitlab.com/hestia-earth/hestia-schema/commit/7883815dbf313a48a2d0a742d01e2f5cc6c066a9))

### [0.0.27](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.26...v0.0.27) (2020-08-25)


### Features

* **cycle:** add alternativeStartDate field ([2ce5095](https://gitlab.com/hestia-earth/hestia-schema/commit/2ce5095ba69be2c573695ea392669578549d030d))
* **cycle:** add alternativeStartDateDefinition field ([2bbfd16](https://gitlab.com/hestia-earth/hestia-schema/commit/2bbfd1673fcd82d786a3eef45a7dd0bd88b610e7))
* **cycle:** remove treatment and treatmentDescription ([9bf7eda](https://gitlab.com/hestia-earth/hestia-schema/commit/9bf7eda22561ee78155a33f74387394379375491))
* **infrastructure:** add area ([8bbc244](https://gitlab.com/hestia-earth/hestia-schema/commit/8bbc244efaf15c6228d88521643065122e5274ac))
* **infrastructure:** add hours for lifespan and mass ([963377d](https://gitlab.com/hestia-earth/hestia-schema/commit/963377d896b674eb6f95fe9eddbd803089e1aa25))
* **measurement:** replace date with startDate + endDate ([96c47d9](https://gitlab.com/hestia-earth/hestia-schema/commit/96c47d96598c610ddf58ae3f4ae1516dff56a65d))
* **practice:** add sources and dataState ([494292f](https://gitlab.com/hestia-earth/hestia-schema/commit/494292fb71c9795a3a14045a3e03deedbe79e1b2))


### Bug Fixes

* **source:** remove internal from uploadNotes field ([cc0cb4b](https://gitlab.com/hestia-earth/hestia-schema/commit/cc0cb4b6ba20bafaf68f68bfbcda144a20224817))

### [0.0.26](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.25...v0.0.26) (2020-08-25)


### Features

* export version as SCHEMA_VERSION ([0a5c419](https://gitlab.com/hestia-earth/hestia-schema/commit/0a5c41921bb5887b67447313bf956769547b171a))


### Bug Fixes

* **json-schema:** exclude some properties from default values ([48b8c31](https://gitlab.com/hestia-earth/hestia-schema/commit/48b8c313c6995c7c56a2ca169d6dd8a5f832f19e))

### [0.0.25](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.24...v0.0.25) (2020-08-21)


### Features

* add searchable on all valuable terms ([5b778c2](https://gitlab.com/hestia-earth/hestia-schema/commit/5b778c21270114f4082f0b43a6995abb919076cd))

### [0.0.24](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.23...v0.0.24) (2020-08-21)


### Features

* **cycle:** add searchable on inputs, emissions, products and practices ([a3449dc](https://gitlab.com/hestia-earth/hestia-schema/commit/a3449dc9f7b0698755b9d1e38d072d0e2e7b13e6))
* **property:** add dataState and dataVersions ([40e2da0](https://gitlab.com/hestia-earth/hestia-schema/commit/40e2da04ebd3d6a0bf6987cbcd84423db476fc40))

### [0.0.23](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.22...v0.0.23) (2020-08-19)


### Bug Fixes

* **term:** set name as searchable ([84353f7](https://gitlab.com/hestia-earth/hestia-schema/commit/84353f784bb8b92d5f4825861d5ba2aa65884c8d))

### [0.0.22](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.21...v0.0.22) (2020-08-19)


### Features

* change how methodTier is defined ([ac30986](https://gitlab.com/hestia-earth/hestia-schema/commit/ac309864561d998e4920f382219b58e371776a42))
* **json-schema:** add function get default properties ([08014a0](https://gitlab.com/hestia-earth/hestia-schema/commit/08014a0bf6cab9160ead1133b4b3448fc3c4ba24))
* **site:** improve definition of siteType field ([4b278e4](https://gitlab.com/hestia-earth/hestia-schema/commit/4b278e45f688878e6e3daadcf1084b9bb9cff3dc))
* **site:** make siteType a required field ([131cf2d](https://gitlab.com/hestia-earth/hestia-schema/commit/131cf2df70dbfd9611f8d255c1d3e756c98f72b7))


### Bug Fixes

* improve clarity of dataState and dataVersions fields; add default: original to dataState ([1cb313e](https://gitlab.com/hestia-earth/hestia-schema/commit/1cb313e8b16458f6508ebc8af054495738fdf64b))
* **excel_template:** add dropdown for siteType ([13376cf](https://gitlab.com/hestia-earth/hestia-schema/commit/13376cfd43a5ac6ccc75605f47a10f5dff0c5d41))
* **inventory:** update example ([1143161](https://gitlab.com/hestia-earth/hestia-schema/commit/1143161b211e781cb336f0df6b61bf28408dd7ba))

### [0.0.21](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.20...v0.0.21) (2020-08-17)


### Features

* **source:** search by bibliography ([506e307](https://gitlab.com/hestia-earth/hestia-schema/commit/506e3072b8754d6ead0c2854faa71ee1974b1adc))
* **types:** export searchable properties ([7d392bc](https://gitlab.com/hestia-earth/hestia-schema/commit/7d392bccddfdc1c2fdc0b318bd5c466d5d6f2378))


### Bug Fixes

* **cycle:** set Site as searchable ([36f48b0](https://gitlab.com/hestia-earth/hestia-schema/commit/36f48b0ebb2232bc12126c38ae5bbed1ccd6aca8))

### [0.0.20](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.19...v0.0.20) (2020-08-16)


### Bug Fixes

* fix module not found outside node env ([fa4d01a](https://gitlab.com/hestia-earth/hestia-schema/commit/fa4d01a0583649f84b4caaa3872a1ac837ea5802))

### [0.0.19](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.18...v0.0.19) (2020-08-16)


### Features

* add min and max as well as sd ([2d9dc34](https://gitlab.com/hestia-earth/hestia-schema/commit/2d9dc340c2614ce0695c6b81ed612d889fc4e2a2)), closes [#18](https://gitlab.com/hestia-earth/hestia-schema/issues/18)
* add option to declare that multiple Cycles or Sites were aggregated ([e6e61e2](https://gitlab.com/hestia-earth/hestia-schema/commit/e6e61e2c8066980e32b48a88b945361985fadd87))
* add reliability assessment ([7ab2a21](https://gitlab.com/hestia-earth/hestia-schema/commit/7ab2a21ebf4860b3fda6e41068a80ee6254f24fa)), closes [#18](https://gitlab.com/hestia-earth/hestia-schema/issues/18)
* **practice:** enable properties to be added to practices ([bd83360](https://gitlab.com/hestia-earth/hestia-schema/commit/bd833601afff920eb84c80796e88eb4f2aadbd20))
* **product:** add dataState and dataVersion ([99f7fb8](https://gitlab.com/hestia-earth/hestia-schema/commit/99f7fb82167e9ad43c106cb2fec7f9288838edfe))
* **source:** add uploadNotes free text field to describe data upload ([3e417b6](https://gitlab.com/hestia-earth/hestia-schema/commit/3e417b6cb7c08862d52775c07a252d2006fbf1eb))
* **term:** add usdaSoilType ([78ab171](https://gitlab.com/hestia-earth/hestia-schema/commit/78ab1711f8c6be446bec95c105b4b47a20e2a28b)), closes [#32](https://gitlab.com/hestia-earth/hestia-schema/issues/32)


### Bug Fixes

* remove recalculated field ([0046591](https://gitlab.com/hestia-earth/hestia-schema/commit/00465914bff456dcdfaeec2da85727125fdebefe))
* update examples for statsDefinition field ([5e5c973](https://gitlab.com/hestia-earth/hestia-schema/commit/5e5c973bdc97daec890f6107d92aedd276676b40))
* **cycle:** clarify functional unit definition ([a978606](https://gitlab.com/hestia-earth/hestia-schema/commit/a9786065d015e835e6fbf0e87430605baedf72f8))
* **infrastructure:** clarify description ([a03e240](https://gitlab.com/hestia-earth/hestia-schema/commit/a03e240a1dd2414cfb28a43d749f47497b3bc419))
* **measurement:** better define units ([1cc525d](https://gitlab.com/hestia-earth/hestia-schema/commit/1cc525db008663dd70ed5cb53a9c37417b3bff6c))
* **organisation:** lat and lon definitions ([b5d17a9](https://gitlab.com/hestia-earth/hestia-schema/commit/b5d17a9bba84d572990b83c43cd68904b6c6d07f))
* **site:** add sdDefinition ([fb2dd88](https://gitlab.com/hestia-earth/hestia-schema/commit/fb2dd88cf6a846e2a0a971e410b5450348f7850c))
* **site:** fix longitude description ([66b48b5](https://gitlab.com/hestia-earth/hestia-schema/commit/66b48b5be992f0bece849d92c3761c37cfb497f9))
* **site:** improve field definitions ([e81f9dc](https://gitlab.com/hestia-earth/hestia-schema/commit/e81f9dcf3cb9a584dc7e4b310e54dd1ce7ae7d13))
* **site:** typo in searchable key ([ca4fcd4](https://gitlab.com/hestia-earth/hestia-schema/commit/ca4fcd41c414974e66d7150c092f91fafc487c52))
* **term:** change termType from required: true to internal: true ([30c8760](https://gitlab.com/hestia-earth/hestia-schema/commit/30c8760b498547938a7fd1454df9a20f5e348702))

### [0.0.18](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.17...v0.0.18) (2020-08-13)

### [0.0.17](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.16...v0.0.17) (2020-08-08)


### Features

* **json-schema:** add javascript helper load all schemas ([16e0e0b](https://gitlab.com/hestia-earth/hestia-schema/commit/16e0e0b4e46ac03f1ec3c8cf8658ad6e748655ab))

### [0.0.16](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.15...v0.0.16) (2020-08-07)


### Features

* add searchable key ([fe8bac1](https://gitlab.com/hestia-earth/hestia-schema/commit/fe8bac159ddbc113f92f8c8f03bda1dfdc1ec3ed))


### Bug Fixes

* **term:** remove deprecated shortName property ([9303b33](https://gitlab.com/hestia-earth/hestia-schema/commit/9303b33946066e6cc15488040b12332fc03a5ba3))

### [0.0.15](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.14...v0.0.15) (2020-08-04)


### Features

* make dataPrivate field required ([a28ebf6](https://gitlab.com/hestia-earth/hestia-schema/commit/a28ebf61d3af2ce3ac51d580cf068ccca499c510))
* **practices:** move to Practice / List[Practice] ([f8d9b48](https://gitlab.com/hestia-earth/hestia-schema/commit/f8d9b48e87fddb1e87d588811aa5e1d90f8545f8))
* **site:** add aquaculture pens as a siteType ([9aabf1f](https://gitlab.com/hestia-earth/hestia-schema/commit/9aabf1f21ad7aa7547af94f33f6a53d0c89bae26))


### Bug Fixes

* change farm to organisation in sdDefinition ([3bc8578](https://gitlab.com/hestia-earth/hestia-schema/commit/3bc8578dd6b094cac1a5ba406e0294bc6765dc5e))
* move dataState and dataVesions into the blank nodes ([81c39d9](https://gitlab.com/hestia-earth/hestia-schema/commit/81c39d902366427afa1b5f755adc8ae39f224621))
* **cycle:** remove dataState ([5bd2f1b](https://gitlab.com/hestia-earth/hestia-schema/commit/5bd2f1bdc2b7fa53ce0a989b363a2aacb2435e1b))
* **organisation:** remove unique and required from name ([c104552](https://gitlab.com/hestia-earth/hestia-schema/commit/c1045526eff292d4e40e14377d4edeaf3b181761))
* **practices:** follow Input/Property schema ([d48b086](https://gitlab.com/hestia-earth/hestia-schema/commit/d48b086270741e459a0ec76cac067aaf7be08d07))

### [0.0.14](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.13...v0.0.14) (2020-07-23)


### Features

* **python:** add JSONLD classes ([60dd58e](https://gitlab.com/hestia-earth/hestia-schema/commit/60dd58e0d94c40c3bf57003e05f6c4ddc8cc631f))
* **python:** add to_dict method to filter out non-required empty values ([8c8778b](https://gitlab.com/hestia-earth/hestia-schema/commit/8c8778b7635e08332754420b5faf0d069aa60b68))

### [0.0.13](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.12...v0.0.13) (2020-07-16)


### Features

* change country from string to Ref[Term] ([a33660b](https://gitlab.com/hestia-earth/hestia-schema/commit/a33660bf695e074d205e9f32570db543d25732a0))
* **term:** add cropProduct ([8809f30](https://gitlab.com/hestia-earth/hestia-schema/commit/8809f30ef7a77c78a72eda5c67fd2705eddb3028))
* **terms:** mark shortName as deprecated ([08acfbb](https://gitlab.com/hestia-earth/hestia-schema/commit/08acfbb4cc0abd44bdb44dfcc6f88548ffb44c64))


### Bug Fixes

* **bibliography:** typo ([4cbf69d](https://gitlab.com/hestia-earth/hestia-schema/commit/4cbf69d5b244047cf27d9ae6c5d9a0aaff2c8a20))

### [0.0.12](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.11...v0.0.12) (2020-07-16)


### Bug Fixes

* normalize country to use Term ([3ab1d5e](https://gitlab.com/hestia-earth/hestia-schema/commit/3ab1d5e30d4cc8dbd050272b1f238e7af574a56d))

### [0.0.11](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.10...v0.0.11) (2020-07-16)

### [0.0.10](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.9...v0.0.10) (2020-07-16)

### [0.0.9](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.8...v0.0.9) (2020-07-16)

### [0.0.8](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.7...v0.0.8) (2020-07-16)


### Features

* move practices into blank node ([7372c88](https://gitlab.com/hestia-earth/hestia-schema/commit/7372c88bf47d3bf0e0eaaa665bda88a595a5bced))
* **cycle:** add further fields ([9225439](https://gitlab.com/hestia-earth/hestia-schema/commit/9225439fafd35b0275e46d03c5b9ffb693f6a195))
* **cycles:** change Completeness and Practices so single Embed ([b83d7d2](https://gitlab.com/hestia-earth/hestia-schema/commit/b83d7d2c4744c579be67d01568ad1cd2e05b1494))
* **organisation:** add area and boundary for farms ([190c850](https://gitlab.com/hestia-earth/hestia-schema/commit/190c85093c95e128efe3c68056fd097513faa610))
* **site:** differentiate siteType into cropland and pasture ([51b42f0](https://gitlab.com/hestia-earth/hestia-schema/commit/51b42f0c0cdf63503bf8f154300b884b626c8294))
* **term:** add cropEstablishment term ([c0f3d6d](https://gitlab.com/hestia-earth/hestia-schema/commit/c0f3d6d93972d598607e0dfc5d93754bc024328e))


### Bug Fixes

* change term order, set name to internal ([7cda43c](https://gitlab.com/hestia-earth/hestia-schema/commit/7cda43ca0776e7139699a5d8b6c7a5e24b6ae215))
* **completeness:** add compost to definition ([b2045c6](https://gitlab.com/hestia-earth/hestia-schema/commit/b2045c62d3cec94f6c200331471ea966735758da))
* **completeness:** field names for readibility ([a56f41b](https://gitlab.com/hestia-earth/hestia-schema/commit/a56f41b00eac8642b581e9b2e6b79bc97b856a9a))
* **input:** remove required on value ([567da80](https://gitlab.com/hestia-earth/hestia-schema/commit/567da807387a79a460a2f13d9ca433f003bd3019))
* **inventory:** rename field ([ff56474](https://gitlab.com/hestia-earth/hestia-schema/commit/ff564741ce268cf69f143dec67749fb5be4ddbbb))
* **measurement:** change definition of sd ([5393ec0](https://gitlab.com/hestia-earth/hestia-schema/commit/5393ec08363b198ae4a22ccd36bc503e65ab9e64))
* **practices:** correct typos ([0a799b2](https://gitlab.com/hestia-earth/hestia-schema/commit/0a799b2c8a93d216542ebecc3105c46a5fac9bc3))
* **site:** typo ([4b59cd0](https://gitlab.com/hestia-earth/hestia-schema/commit/4b59cd05f82e8e369795066c202ba9400a703de1))
* **source:** allow custom names for sources ([1931192](https://gitlab.com/hestia-earth/hestia-schema/commit/19311921744921015e14b848d3d52a5d5b094763))
* **source:** change uploadBy and validationBy to internal properties ([b6bd3b1](https://gitlab.com/hestia-earth/hestia-schema/commit/b6bd3b1600c8856bd52f39a43f5c3f7b64269cdd))
* **term:** change termType floodingRegime to waterRegime ([4684098](https://gitlab.com/hestia-earth/hestia-schema/commit/468409895927ba48f3c344790c93a731198eea84))
* **types:** allow NodeType when generating jsonldPath ([b321a1f](https://gitlab.com/hestia-earth/hestia-schema/commit/b321a1fbfa7d6f03ab8b1793006f14ce66111458))

### [0.0.7](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.6...v0.0.7) (2020-07-15)


### Features

* add further required terms ([62b9add](https://gitlab.com/hestia-earth/hestia-schema/commit/62b9add6b6b9cd4a0c9b61035e74663dea5d8ea5))
* set some properties as internal ([03dfdc7](https://gitlab.com/hestia-earth/hestia-schema/commit/03dfdc70b5d4e20ff166aa079467f405135525c9))
* **actor:** add dataPrivate field ([346195e](https://gitlab.com/hestia-earth/hestia-schema/commit/346195ed9d4b5471276008b67b5787634a065731))
* **cycles:** add description, dataState and dataVersions properties ([ef293c6](https://gitlab.com/hestia-earth/hestia-schema/commit/ef293c686115f74ef3c0ef96a653c6bfd7fbd693))
* **emissions:** add startDate, endDate and emissionDuration properties ([8e5b8e2](https://gitlab.com/hestia-earth/hestia-schema/commit/8e5b8e216f4bc33d3cf5c494ac8a0014d78a3d3b))
* **inventories:** add dataState and dataVersions properties ([aee1418](https://gitlab.com/hestia-earth/hestia-schema/commit/aee1418a8849ab695d491bbcbf8a8b12a9ad88a1))
* **sites:** add description, dataState and dataVersions properties ([04e2476](https://gitlab.com/hestia-earth/hestia-schema/commit/04e2476b9573bfde2a9fafbdaf01e7b0af8f3205))
* **terms:** add required shortName ([1466edb](https://gitlab.com/hestia-earth/hestia-schema/commit/1466edbf941816fff267faec36cc46f594210635))


### Bug Fixes

* **terms:** remove required shortName ([db8616e](https://gitlab.com/hestia-earth/hestia-schema/commit/db8616ee1389fa2cf4b167c30a86d22930e332f6))

### [0.0.6](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.5...v0.0.6) (2020-07-07)


### Features

* add required fields ([b0fd69c](https://gitlab.com/hestia-earth/hestia-schema/commit/b0fd69c42644ceb64e37267a0bddcc4811ae3ead))
* add unique fields ([316f22d](https://gitlab.com/hestia-earth/hestia-schema/commit/316f22d11df551f51a069b356b53d3f50053cba2))
* **sites:** add extra GADM layer level 5 ([0e4f675](https://gitlab.com/hestia-earth/hestia-schema/commit/0e4f6759c202c2ed21438ced81efd159a8b61b3f))


### Bug Fixes

* **terms:** add resourceUse termType ([1bdaa55](https://gitlab.com/hestia-earth/hestia-schema/commit/1bdaa55fa76a8695d0ee8fbd397b20af763cf2ea)), closes [#9](https://gitlab.com/hestia-earth/hestia-schema/issues/9)

### [0.0.5](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.4...v0.0.5) (2020-06-29)


### Features

* add `recalculated` field on Emission, Input and Product ([a26d040](https://gitlab.com/hestia-earth/hestia-schema/commit/a26d040675191a17ebb6df05c10f7095d8150e24))
* handle array[number] type ([e1cce60](https://gitlab.com/hestia-earth/hestia-schema/commit/e1cce60ffa704f879637d260213dc7fd48ca3e22)), closes [#5](https://gitlab.com/hestia-earth/hestia-schema/issues/5)
* **property:** add valueType to determine parsing on value ([c86228e](https://gitlab.com/hestia-earth/hestia-schema/commit/c86228e215b9d96a9a56535a22ff70e848ba6c52)), closes [#4](https://gitlab.com/hestia-earth/hestia-schema/issues/4)
* **sites:** use GADM regions as Term ([f92ff11](https://gitlab.com/hestia-earth/hestia-schema/commit/f92ff11821a4f0599cc3601c300efeafacea6d2e))
* **terms:** add termTypes ecoregion, region and liveAquaticSpecies ([46fef2b](https://gitlab.com/hestia-earth/hestia-schema/commit/46fef2b5d077c5f0b2ebc9504ee3394a2479ca7a))


### Bug Fixes

* **completeness:** change `coProducts` to `products` and `residue` to `cropRes` ([fd35f82](https://gitlab.com/hestia-earth/hestia-schema/commit/fd35f82531fcf3558b6f632874f6450486777f6c))
* **generate json-schemas:** handle arrays of types ([c9c6dcd](https://gitlab.com/hestia-earth/hestia-schema/commit/c9c6dcd4f34a2f458aa57ac06da4f1088c060c5d))

### [0.0.4](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.3...v0.0.4) (2020-06-15)


### Features

* **emissions:** add characterisation property ([d8cdc3a](https://gitlab.com/hestia-earth/hestia-schema/commit/d8cdc3a82680643a72b5edd5347eff0ee3d8da6e))

### [0.0.3](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.2...v0.0.3) (2020-05-18)


### Bug Fixes

* **enums:** fix wrong enum values ([d674da9](https://gitlab.com/hestia-earth/hestia-schema/commit/d674da9f163af983c29ff99e91eb3f26f56dbe36))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-schema/compare/v0.0.1...v0.0.2) (2020-05-17)


### Bug Fixes

* **generate:** handle list for python ([4b7d2c5](https://gitlab.com/hestia-earth/hestia-schema/commit/4b7d2c5ee884c6c0d9335779c0565a700fad9435))

### 0.0.1 (2020-05-15)


### Features

* update content and examples ([984eee2](https://gitlab.com/hestia-earth/hestia-schema/commit/984eee2bcffd20efd127083c2a99eaede7d54794))
* **context:** generate context files for each node ([6cf79d7](https://gitlab.com/hestia-earth/hestia-schema/commit/6cf79d7db44ada66a4429873cf261daf8046bda4))
* **context:** handle GeoJSON type ([f04a92b](https://gitlab.com/hestia-earth/hestia-schema/commit/f04a92bac4a2117b9a6fde5cf6ce8da7cfac9931))
* **generate types:** generate enum ([62289cc](https://gitlab.com/hestia-earth/hestia-schema/commit/62289ccac25384cd6b5ddb2b7211cb6a6835a27b))
* **index:** update link to glossary ([f24dc0e](https://gitlab.com/hestia-earth/hestia-schema/commit/f24dc0e86eea53013e2a559acda7994610791243))
* **json-schema:** add generated files ([00adc55](https://gitlab.com/hestia-earth/hestia-schema/commit/00adc559468dae7038124d82b5b06315267a00e2))
* **json-schema:** generate from yaml files ([bf8a4fd](https://gitlab.com/hestia-earth/hestia-schema/commit/bf8a4fd6af63cbbe4ce3aaf13ffdd095758269e4))
* **schema:** add enum and function for type Node ([f8a22d8](https://gitlab.com/hestia-earth/hestia-schema/commit/f8a22d801e1f6fa0c18c52aac5f2c3613b2d6de9))
* **schema:** add enum values to generated types ([a5c9d1e](https://gitlab.com/hestia-earth/hestia-schema/commit/a5c9d1e51e7c9a344a69a305d10e376fe3a8a0e5))
* **schema:** add list of content types ([d5dc31d](https://gitlab.com/hestia-earth/hestia-schema/commit/d5dc31de1708c21bca24c3255a0b12a0fd7ab247))
* **schema:** generate typescript classes from yaml files ([36db121](https://gitlab.com/hestia-earth/hestia-schema/commit/36db121d0828f9bbbe00c8aff8375381e8f2b4df))
* **schema:** update types ([ca04f94](https://gitlab.com/hestia-earth/hestia-schema/commit/ca04f94ce821b08e78836815d17e34c759e021f4))
* **templates:** add json-ld views ([da91027](https://gitlab.com/hestia-earth/hestia-schema/commit/da91027897b330b2918deb44f8e1e47c92931b6b))
* **templates:** add tooltip on copy button ([6376d78](https://gitlab.com/hestia-earth/hestia-schema/commit/6376d78fb8464cfc2ef45b135b4d4ee908eb0207))
* **templates:** include navbar and sidebar ([ed6c0c5](https://gitlab.com/hestia-earth/hestia-schema/commit/ed6c0c56bd519442439b8d47465a4c8f043e4a55))
* **templates:** update class page title ([456513a](https://gitlab.com/hestia-earth/hestia-schema/commit/456513aeb9b237e67786a3817f9a208214b8d581))
* **templates:** update container styles ([f26da0b](https://gitlab.com/hestia-earth/hestia-schema/commit/f26da0b1b10e43b432e82fb4406e6be14d52fe1b))
* **templates:** update title for index page ([ee459d9](https://gitlab.com/hestia-earth/hestia-schema/commit/ee459d938c5662db7952919a7c560a28d263a11d))
* **templates:** use highlight + copy for all codes ([ae4e026](https://gitlab.com/hestia-earth/hestia-schema/commit/ae4e026d4be88a6baa2b0ca97f2563f64d31c421))
* **templates index:** add basis for content structure ([26c87f2](https://gitlab.com/hestia-earth/hestia-schema/commit/26c87f2506069a2ff6172cd2f7cdf8bd012c7019))
* **yaml:** fix typos and add type ([889df81](https://gitlab.com/hestia-earth/hestia-schema/commit/889df81dc02f7bbb3904baf0e0f0e15f1552d870))


### Bug Fixes

* fix generators ([902630c](https://gitlab.com/hestia-earth/hestia-schema/commit/902630cfef7d208993f0dbda786d07d20f148db4))
* fix links and json-schema content ([3710b9e](https://gitlab.com/hestia-earth/hestia-schema/commit/3710b9e4f22d0ffa1a68a0115ec484534dee619a))
* **context:** handle not node types ([70322b3](https://gitlab.com/hestia-earth/hestia-schema/commit/70322b3b151f178b0fb9230767f292be20b527f9))
* **context:** update geojson context ([7d92dad](https://gitlab.com/hestia-earth/hestia-schema/commit/7d92dad5f1a35d04fce2ff11e06eb5f3e20a3291))
* **cycle:** remove identical extra example ([dfdc1b7](https://gitlab.com/hestia-earth/hestia-schema/commit/dfdc1b7d4d3e2a3f6ad01e91fc34736bd491f25d))
* **dist:** add missing dist files ([b1f91f7](https://gitlab.com/hestia-earth/hestia-schema/commit/b1f91f7bcbf228e0db54030516de9975ae77609d))
* **examples:** fix term values ([4d500da](https://gitlab.com/hestia-earth/hestia-schema/commit/4d500da90597867fce465f27077a37817004e499))
* **generate:** add missing name on JSONLD content ([ab0188f](https://gitlab.com/hestia-earth/hestia-schema/commit/ab0188f780e1c56ad8618a7d81bce3d75812f4e4))
* **generate:** merge utils into generate-types ([7987557](https://gitlab.com/hestia-earth/hestia-schema/commit/7987557725a4ae5f3792d5ccc6d6ccd8b49899c0))
* **generate types:** exclude class from own imports ([3fd28d6](https://gitlab.com/hestia-earth/hestia-schema/commit/3fd28d640dcb5c7939de9f3d156d233a948dffd5))
* **json-schema:** add missing type on $ref ([a30708a](https://gitlab.com/hestia-earth/hestia-schema/commit/a30708aac1c45ee062e11cdbd584b0e71012b591))
* **json-schema:** add missing type on array items ([bbbfaca](https://gitlab.com/hestia-earth/hestia-schema/commit/bbbfaca46812ae5135ab7565e3c202c5c697c93a))
* **json-schema:** clean up description content ([248acb5](https://gitlab.com/hestia-earth/hestia-schema/commit/248acb5511b1a0a11308eea3ba17f66eef518cce))
* **json-schema:** generate schema for JSON-Node ([ec399fb](https://gitlab.com/hestia-earth/hestia-schema/commit/ec399fb415885386d60b84865a35cd29d2bd4776))
* **json-schema:** remove type object on $ref ([965d950](https://gitlab.com/hestia-earth/hestia-schema/commit/965d950a76b87a4edc1a702fd696f1ca53b592af))
* **json-schema:** skip id property for non-node ([db89ad4](https://gitlab.com/hestia-earth/hestia-schema/commit/db89ad42fb8d4ba07b66ff77c0c817b972b7adcf))
* **json-schema:** use ajv for schema validation ([f223005](https://gitlab.com/hestia-earth/hestia-schema/commit/f223005922eec557f45a76058c8ee969143f1b3a))
* **schema:** set JSONLD as extension of NodeType ([2db652c](https://gitlab.com/hestia-earth/hestia-schema/commit/2db652ce5da89e105214fe613e72aee361bd194e))
* **scripts:** fix generate models form yaml ([5bdc399](https://gitlab.com/hestia-earth/hestia-schema/commit/5bdc3991e197870bd7cb7a05a75c0bceec9f7dec))
* **scripts:** fix table layout ([6a34900](https://gitlab.com/hestia-earth/hestia-schema/commit/6a34900c64ca9eaca44750e4345505dd1457a4d3))
* **scripts:** handle geojson type link ([9ba13d5](https://gitlab.com/hestia-earth/hestia-schema/commit/9ba13d5bfa0e6ee368aa75d1c39064275f4fd2cb))
* **scripts:** run scripts from top folder ([eaf9dfb](https://gitlab.com/hestia-earth/hestia-schema/commit/eaf9dfb5a7d88715971a2f81c2a4c6b512a58934))
* **template:** enable json-ld views for Nodes only ([f858d49](https://gitlab.com/hestia-earth/hestia-schema/commit/f858d49e36abeb840f6de9b664c4c801c39b3346))
* **templates:** fix context for jsonld ([35ee6d8](https://gitlab.com/hestia-earth/hestia-schema/commit/35ee6d87fb77729973a202c96d928b4e8a3d181d))
* **templates:** fix link to glossary ([8e11745](https://gitlab.com/hestia-earth/hestia-schema/commit/8e1174505c5e6b271c32d4543f41ebb026d3a258))
* **templates:** fix table width ([c6911f4](https://gitlab.com/hestia-earth/hestia-schema/commit/c6911f4cd59384db630de2ab4e2d25c9bc0e7faa))
* **templates:** fix wrong closing tag and update colors ([66698bf](https://gitlab.com/hestia-earth/hestia-schema/commit/66698bf6c4ddebd01e59ca22fc75a7fdc1984351))
* **templates:** reduce size 2nd level in menu ([a9f704e](https://gitlab.com/hestia-earth/hestia-schema/commit/a9f704ea8075a835c693e1ed255f51f4914e5042))
* **templates:** remove unused enum ([7abd0bf](https://gitlab.com/hestia-earth/hestia-schema/commit/7abd0bfc3a97a9aff3cd8f1fca2d3fd79861238e))
* **templates:** update styles ([390f289](https://gitlab.com/hestia-earth/hestia-schema/commit/390f2899a1d465f980a7ee7d2e1a366b3f99d99e))
* **templates:** use examples as array ([515e14a](https://gitlab.com/hestia-earth/hestia-schema/commit/515e14a4c632367a350f673531967965cf2e123e))
