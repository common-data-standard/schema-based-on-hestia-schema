#!/bin/sh

FOLDER="$1"
DEST_FOLDER="s3://$BUCKET/$FOLDER"

aws s3 sync ./context $DEST_FOLDER --content-type 'application/ld+json'
aws s3 sync ./diagrams $DEST_FOLDER

aws s3 sync ./examples $DEST_FOLDER/examples --exclude "*" --include "*.jsonld" --content-type 'application/ld+json'
aws s3 sync ./examples $DEST_FOLDER/examples --exclude "*.jsonld"

aws s3 sync ./src/@hestia-earth/json-schema/json-schema $DEST_FOLDER/json-schema --content-type 'application/json'
aws s3 sync ./src/@hestia-earth/schema/sort-config $DEST_FOLDER/sort-config --content-type 'application/json'

aws s3 sync ./yaml $DEST_FOLDER/yaml

aws s3 cp versions.txt $DEST_FOLDER/versions.txt
