#!/bin/sh

docker run --rm -u `id -u`:`id -g` \
  -v ${PWD}/diagrams:/data \
  minlag/mermaid-cli \
  -i /data/schema.mmd -o /data/schema.svg

docker run --rm -u `id -u`:`id -g` \
  -v ${PWD}/diagrams:/data \
  minlag/mermaid-cli \
  -i /data/schema.mmd -o /data/schema.png
