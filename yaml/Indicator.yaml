class:
  name: Indicator
  type: Blank Node
  examples:
    - indicator.jsonld
  doc: >
    An emission, resource use, or characterised environmental impact indicator.
    Each Indicator must be unique, and the fields which determine uniqueness are defined
    in the <code>[emissionsResourceUse](/schema/ImpactAssessment#emissionsResourceUse)</code>,
    <code>[impacts](/schema/ImpactAssessment#impacts)</code>, and
    <code>[endpoints](/schema/ImpactAssessment#endpoints)</code> fields of the Impact Assessment.

  properties:
    - name: term
      type: Ref[Term]
      doc: >
        A reference to the [Term](/schema/Term) describing the emission e.g.,
        [CH4, to air, crop residue burning](/term/ch4ToAirCropResidueBurning); the resource
        use e.g., [Freshwater withdrawals, during Cycle](/term/freshwaterWithdrawalsDuringCycle);
        or the characterised environmental impact indicator e.g.,
        [Terrestrial ecotoxicity potential (1,4-DCBeq)](/term/terrestrialEcotoxicityPotential14Dcbeq).
      required: true
      searchable: true
      exportDefaultCSV: true

    - name: value
      type: number|null
      doc: The quantity. If an average, it should always be the mean.
      required: true
      default: null
      exportDefaultCSV: true

    - name: distribution
      type: array[number]
      doc: An array of random draws from the posterior distribution of the Indicator.
      exportDefaultCSV: true

    - name: sd
      type: number
      minimum: 0
      doc: The standard deviation of <code>[value](#value)</code>.
      exportDefaultCSV: true

    - name: min
      type: number
      doc: The minimum of <code>[value](#value)</code>.
      exportDefaultCSV: true

    - name: max
      type: number
      doc: The maximum of <code>[value](#value)</code>.
      exportDefaultCSV: true

    - name: statsDefinition
      type: string
      enum:
        - sites
        - cycles
        - impactAssessments
        - replications
        - other observations
        - time
        - spatial
        - regions
        - simulated
        - modelled
      doc: >
        What the descriptive statistics (<code>[sd](#sd)</code>, <code>[min](#min)</code>,
        <code>[max](#max)</code>, and <code>[value](#value)</code>) are calculated across, or
        whether they are simulated or the output of a model. <code>spatial</code> refers to
        descriptive statistics calculated across spatial units (e.g., pixels) within a region
        or country. <code>time</code> refers to descriptive statistics calculated across units
        of time (e.g., hours).
      exportDefaultCSV: true

    - name: observations
      type: integer
      minimum: 1
      doc: The number of observations the descriptive statistics are calculated over.
      searchable: true
      exportDefaultCSV: true

    - name: methodModel
      type: Ref[Term]
      doc: >
        A reference to the [Term](/schema/Term) describing the method or model for calculating these data. This
        is a required field for characterised indicators.
      searchable: true
      exportDefaultCSV: true
      recommended: true

    - name: methodModelDescription
      type: string
      doc: >
        A free text field, describing the method or model used for calculating these data. For example,
        it can be used to specify the version of the method or model, and/or the software used 
        to carry out the assessment.
      exportDefaultCSV: true

    - name: inputs
      type: List[Ref[Term]]
      doc: >
        For [emissions or resource uses](/schema/ImpactAssessment#emissionsResourceUse) from
        [Inputs production](/glossary?page=1&query=inputs%20production),
        the [Term](/schema/Term) describing the Input.
        For [characterised indicators](/schema/ImpactAssessment#impacts)
        or [endpoint indicators](/schema/ImpactAssessment#endpoints), if this Indicator represents
        the amount caused by producing one or more Inputs used by this Cycle, the
        [Term](/schema/Term) describing the Inputs.
      exportDefaultCSV: true
      uniqueArrayItem:
        - '@id'

    - name: operation
      type: Ref[Term]
      doc: >
        For [emissions or resource uses](/schema/ImpactAssessment#emissionsResourceUse)
        created by an [operation](/glossary?termType=operation), the [Term](/schema/Term) describing
        the operation.
      searchable: true
      exportDefaultCSV: true

    - name: transformation
      type: Ref[Term]
      doc: >
        For [emissions or resource uses](/schema/ImpactAssessment#emissionsResourceUse)
        created during a [Transformation](/schema/Transformation), the [Term](/schema/Term) describing the Transformation.
      exportDefaultCSV: true

    - name: schemaVersion
      type: string
      doc: The version of the schema when these data were created.
      internal: true

    - name: added
      type: array[string]
      doc: A list of fields that have been added to the original dataset.
      internal: true

    - name: addedVersion
      type: array[string]
      doc: A list of versions of the model used to add these fields.
      internal: true

    - name: updated
      type: array[string]
      doc: A list of fields that have been updated on the original dataset.
      internal: true

    - name: updatedVersion
      type: array[string]
      doc: A list of versions of the model used to update these fields.
      internal: true

    - name: aggregated
      type: array[string]
      doc: >
        A list of fields that have been 'aggregated' using data from multiple
        [Sites](/schema/Site) and [Cycles](/schema/Cycle).
      internal: true

    - name: aggregatedVersion
      type: array[string]
      doc: A list of versions of the aggregation engine corresponding to each aggregated field.
      internal: true

  validation:
    allOf:
      - if:
          required:
            - term
        then:
          properties:
            term:
              properties:
                termType:
                  enum:
                    - emission
                    - endpointIndicator
                    - characterisedIndicator
                    - resourceUse
      - if:
          required:
            - methodModel
        then:
          properties:
            methodModel:
              properties:
                termType:
                  enum:
                    - model
                    - methodEmissionResourceUse
      - if:
          required:
            - operation
        then:
          properties:
            operation:
              properties:
                termType:
                  enum:
                    - operation
      - if:
          anyOf:
            - required:
              - min
            - required:
              - max
            - required:
              - sd
        then:
          required:
            - statsDefinition
      - if:
          required:
            - statsDefinition
        then:
          anyOf:
            - required:
              - value
            - required:
              - min
            - required:
              - max
            - required:
              - sd
