class:
  name: Practice
  type: Blank Node
  examples:
    - practice.jsonld
    - practice2.jsonld
  doc: >
    A Practice used during a [Cycle](/schema/Cycle) or [Transformation](/schema/Transformation).
    Each Practice must be unique, and the fields which determine uniqueness are
    defined in the <code>[practices](/schema/Cycle#practices)</code> field of the Cycle or
    [Transformation](/schema/Transformation#inputs).

  properties:
    - name: term
      type: Ref[Term]
      doc: >
        A reference to the [Term](/schema/Term) describing the Practice. This can be replaced
        by a description instead if the Term isn't available in the [Glossary](/Glossary).
      required: true
      searchable: true
      exportDefaultCSV: true

    - name: description
      type: string
      doc: >
        A description of the Practice. This is a required field if [term](#term)
        is not provided.
      exportDefaultCSV: true

    - name: key
      type: Ref[Term]
      doc: >
        If the data associated with the Practice are in key:value form, the key.
      searchable: true
      exportDefaultCSV: true

    - name: value
      type: array[string|number|boolean|null]
      doc: The value associated with the Practice. If an average, it should always be the mean.
      exportDefaultCSV: true
      recommended: true

    - name: sd
      type: array[number|null]
      minimum: 0
      doc: The standard deviation of <code>[value](#value)</code>.
      arraySameSize:
        - value
      exportDefaultCSV: true

    - name: min
      type: array[number|null]
      doc: The minimum of <code>[value](#value)</code>.
      arraySameSize:
        - value
      exportDefaultCSV: true

    - name: max
      type: array[number|null]
      doc: The maximum of <code>[value](#value)</code>.
      arraySameSize:
        - value
      exportDefaultCSV: true

    - name: statsDefinition
      type: string
      enum:
        - sites
        - cycles
        - replications
        - other observations
        - time
        - spatial
        - regions
        - simulated
        - modelled
      doc: >
        What the descriptive statistics (<code>[sd](#sd)</code>, <code>[min](#min)</code>,
        <code>[max](#max)</code>, and <code>[value](#value)</code>) are calculated across, or
        whether they are simulated or the output of a model. <code>spatial</code> refers to
        descriptive statistics calculated across spatial units (e.g., pixels) within a region
        or country. <code>time</code> refers to descriptive statistics calculated across units
        of time (e.g., hours).
      exportDefaultCSV: true

    - name: observations
      type: array[number|null]
      minimum: 1
      doc: >
        The number of observations the descriptive statistics are calculated over, if different
        from the [numberOfCycles](/schema/Cycle#numberOfCycles) specified in [Cycle](/schema/Cycle).
      searchable: true
      arraySameSize:
        - value
      exportDefaultCSV: true

    - name: dates
      type: array[string]
      pattern: ^([0-9]{4}|-)(-[0-9]{2})?(-[0-9]{2})?$
      datePattern: true
      doc: >
        A corresponding array to [value](#value), representing the dates of the Practice
        in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).
      arraySameSize:
        - value
      exportDefaultCSV: true

    - name: startDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      datePattern: true
      doc: >
        The start date of the Practice if different from the start date
        of the [Cycle](/schema/Cycle) in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD,
        YYYY-MM, or YYYY). The start date is the first date the practice is carried out. The
        practice will continue without stopping until the end date is reached. Alternatively, use
        the `dates` term to specify practices which occur on a number of different dates.
      exportDefaultCSV: true

    - name: endDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      datePattern: true
      doc: >
        The end date of the Practice if different from the end date
        of the [Cycle](/schema/Cycle) in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD,
        YYYY-MM, or YYYY). The end date is the last date the practice is carried out. The practice
        will continue without stopping until the end date is reached. Alternatively, use
        the `dates` term to specify practices which occur on a number of different dates.
      exportDefaultCSV: true

    - name: methodClassification
      type: string
      enum:
        - physical measurement
        - verified survey data
        - non-verified survey data
        - modelled
        - estimated with assumptions
        - consistent external sources
        - inconsistent external sources
        - expert opinion
        - unsourced assumption
      doc: >
        A classification of the method used to acquire or estimate the
        <code>[term](#term)</code> and <code>[value](#value)</code>. Overrides the
        <code>[defaultMethodClassification](/schema/Cycle#defaultMethodClassification)</code>
        specified in the [Cycle](/schema/Cycle). <code>methodClassification</code> should be specified
        separately for <code>[properties](#properties)</code>
        (see <code>[Property](/schema/Property#methodClassification)</code>).
        <ul class="is-pl-3 is-list-style-disc">
          <li>
            <code>physical measurement</code> means the amount is quantified using weighing,
            volume measurement, metering, chemical methods, or other physical approaches.
          </li>
          <li>
            <code>verified survey data</code> means the data are initially collected through
            surveys; all or a subset of the data are verified using physical methods; and
            erroneous survey data are discarded or corrected.
          </li>
          <li>
            <code>non-verified survey data</code> means the data are collected through
            surveys that have not been subjected to verification.
          </li>
          <li>
            <code>modelled</code> means a previously calibrated model is used to estimate
            this data point from other data points describing this Cycle.
          </li>
          <li>
            <code>estimated with assumptions</code> means a documented assumption is used
            to estimate this data point from other data points describing this Cycle.
          </li>
          <li>
            <code>consistent external sources</code> means the data are taken from external
            datasets referring to different producers/enterprises:
            <ul class="is-pl-4 is-list-style-disc">
              <li>
                Using the same technology (defined as the same
                [System](/glossary?termType=system) or the same key [Practices](/schema/Practice)
                as those specified in the Cycle);
              </li>
              <li>
                At the same date (defined as occurring within the
                [startDate](/schema/Cycle#startDate) and [endDate](/schema/Cycle#endDate) of the Cycle);
                and
              </li>
              <li>
                In the same [region](/schema/Site#region) or [country](/schema/Site#country).
              </li>
            </ul>
            Modelling or assumptions may have also been used to transform these data.
          </li>
          <li>
            <code>inconsistent external sources</code> means the data are taken from external
            datasets referring to different producers/enterprises:
            <ul class="is-pl-4 is-list-style-disc">
              <li>
                Using a different technology (defined as a different
                [System](/glossary?termType=system) or using different key
                [Practices](/schema/Practice) to those specified in the Cycle);
              </li>
              <li>
                At a different date (defined as occurring within the
                [startDate](/schema/Cycle#startDate) and [endDate](/schema/Cycle#endDate) of the Cycle);
                or
              </li>
              <li>
                In a different [region](/schema/Site#region) or [country](/schema/Site#country).
              </li>
            </ul>
            Modelling or assumptions may have also been used to transform these data.
          </li>
          <li>
            <code>expert opinion</code> means the data have been estimated by experts in
            the field.
          </li>
          <li>
            <code>unsourced assumption</code> means the data do not have a clear source
            and/or are based on assumptions only.
          </li>
        </ul>
      searchable: true
      exportDefaultCSV: true

    - name: methodClassificationDescription
      type: string
      doc: >
        A justification of the <code>[methodClassification](#methodClassification)</code>
        used. If the data were <code>estimated with assumptions</code> this field should
        also describe the assumptions. This is a required field if
        <code>[methodClassification](#methodClassification)</code> is specified.
      exportDefaultCSV: true

    - name: model
      type: Ref[Term]
      doc: >
        A reference to the [Term](/schema/Term) describing the [model](/glossary?termType=model) used to
        estimate these data.
      searchable: true
      exportDefaultCSV: true

    - name: modelDescription
      type: string
      doc: A free text field, describing the model used to estimate these data.
      exportDefaultCSV: true

    - name: areaPercent
      type: number
      minimum: 0
      doc: >
        The area of the [Site](/schema/Site) that Practice occurred on, specified as a percentage
        of Site [area](/schema/Site#area). If the units of the [term](#term) are already in
        <code>% area</code>, do not use this field and use [value](#value) instead to
        record these data.
      exportDefaultCSV: true

    - name: price
      type: number
      minimum: 0
      doc: >
        The price paid for this Practice. The price should be expressed per the units defined
        in the [term](#term). The [currency](#currency) must be specified. The price of
        the [inputs](/schema/Cycle#inputs) associated with this practice should be included in the
        [inputs](/schema/Cycle#inputs) rather than here.
      exportDefaultCSV: true

    - name: priceSd
      type: number
      minimum: 0
      doc: The standard deviation of <code>[price](#price)</code>.
      exportDefaultCSV: true

    - name: priceMin
      type: number
      minimum: 0
      doc: The minimum of <code>[price](#price)</code>.
      exportDefaultCSV: true

    - name: priceMax
      type: number
      minimum: 0
      doc: The maximum of <code>[price](#price)</code>.
      exportDefaultCSV: true

    - name: priceStatsDefinition
      type: string
      enum:
        - cycles
        - time
        - cycles and time
      doc: >
        What the descriptive statistics for <code>[price](#price)</code> are calculated
        across.
      exportDefaultCSV: true

    - name: cost
      type: number
      minimum: 0
      doc: >
        The total cost of this Practice ([price](#price) <code>x</code> [quantity](#value)),
        expressed as a positive value. The [currency](#currency) must be specified. The cost of
        the [inputs](/schema/Cycle#inputs) associated with this practice should be included in the
        [inputs](/schema/Cycle#inputs) rather than here.
      exportDefaultCSV: true

    - name: costSd
      type: number
      minimum: 0
      doc: The standard deviation of <code>[cost](#cost)</code>.
      exportDefaultCSV: true

    - name: costMin
      type: number
      minimum: 0
      doc: The minimum of <code>[cost](#cost)</code>.
      exportDefaultCSV: true

    - name: costMax
      type: number
      minimum: 0
      doc: The maximum of <code>[cost](#cost)</code>.
      exportDefaultCSV: true

    - name: costStatsDefinition
      type: string
      enum:
        - cycles
        - time
        - cycles and time
      doc: >
        What the descriptive statistics for <code>[cost](#cost)</code> are calculated
        across.
      exportDefaultCSV: true

    - name: currency
      type: string
      pattern: ^[A-Z]{3}$
      doc: >
        The three letter currency code in [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217)
        format.
      exportDefaultCSV: true

    - name: ownershipStatus
      type: string
      enum:
        - owned
        - rented
        - borrowed
      doc: For operations, the ownership status of the equipment used to perform the operation.
      exportDefaultCSV: true

    - name: source
      type: Ref[Source]
      doc: >
        A reference to the [Source](/schema/Source) of these data, if different from the
        defaultSource of the [Cycle](/schema/Cycle) or [Site](/schema/Site).
      exportDefaultCSV: true

    - name: properties
      type: List[Property]
      doc: >
        A list of [Properties](/schema/Property) of the Practice, which would override any
        default properties specified in the <code>[term](#term)</code>.
      exportDefaultCSV: true
      uniqueArrayItem:
        - term.@id
        - key.@id
        - date
        - startDate
        - endDate

    - name: schemaVersion
      type: string
      doc: The version of the schema when these data were created.
      internal: true

    - name: added
      type: array[string]
      doc: A list of fields that have been added to the original dataset.
      internal: true

    - name: addedVersion
      type: array[string]
      doc: A list of versions of the model used to add these fields.
      internal: true

    - name: updated
      type: array[string]
      doc: A list of fields that have been updated on the original dataset.
      internal: true

    - name: updatedVersion
      type: array[string]
      doc: A list of versions of the model used to update these fields.
      internal: true

    - name: aggregated
      type: array[string]
      doc: A list of fields that have been 'aggregated' using data from multiple [Cycles](/schema/Cycle).
      internal: true

    - name: aggregatedVersion
      type: array[string]
      doc: A list of versions of the aggregation engine corresponding to each aggregated field.
      internal: true

  validation:
    allOf:
      - if:
          required:
            - term
        then:
          properties:
            term:
              properties:
                termType:
                  enum:
                    - animalBreed
                    - animalManagement
                    - aquacultureManagement
                    - cropEstablishment
                    - cropResidueManagement
                    - excretaManagement
                    - landCover
                    - landUseManagement
                    - standardsLabels
                    - system
                    - tillage
                    - waterRegime
                    - wasteManagement
                    - operation
      - if:
          required:
            - model
        then:
          properties:
            model:
              properties:
                termType:
                  enum:
                    - model
      - if:
          required:
            - term
          properties:
            term:
              required:
              - units
              properties:
                units:
                  const: "% area"
        then:
          not:
            required:
              - areaPercent
      - anyOf:
        - required:
          - term
        - required:
          - description
      - if:
          anyOf:
            - required:
              - min
            - required:
              - max
            - required:
              - sd
        then:
          required:
            - statsDefinition
      - if:
          required:
            - statsDefinition
        then:
          anyOf:
            - required:
              - value
            - required:
              - min
            - required:
              - max
            - required:
              - sd
      - if:
          required:
            - ownershipStatus
        then:
          properties:
            term:
              properties:
                termType:
                  enum:
                    - operation
      - if:
          anyOf:
            - required:
              - cost
            - required:
              - price
        then:
          required:
            - currency
      - if:
          required:
            - methodClassification
        then:
          required:
            - methodClassificationDescription
      - if:
          anyOf:
            - required:
              - priceMin
            - required:
              - priceMax
            - required:
              - priceSd
        then:
          required:
            - priceStatsDefinition
      - if:
          anyOf:
            - required:
              - costMin
            - required:
              - costMax
            - required:
              - costSd
        then:
          required:
            - costStatsDefinition
